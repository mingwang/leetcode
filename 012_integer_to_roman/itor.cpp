#include <iostream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    void split (string input, char c, vector<int>& out) {

        for (int i = 1; i<input.size();) {
           int new_i = input.find (c, i);
           if (new_i==string::npos)
               new_i = input.size();
           string s = input.substr(i,new_i-i+1);
           int ele = atoi(s.c_str());
           out.push_back(ele);
           i=new_i+1;
        }
    }

    string intToRoman(int num) {
        // I - 1
        // V - 5
        // X - 10
        // L - 50
        // C - 100
        // D - 500
        // M - 1000

        num = num % 4000;

        string roman;

        int digit_0001 = num % 10;
        int digit_0010 = (num/10) % 10;
        int digit_0100 = (num/100) % 10;
        int digit_1000 = (num/1000) % 10;

        while(digit_1000>0) {
            roman.append("M"); 
            digit_1000 --;
        }

        if (digit_0100 == 9) {
            roman.append("CM");
        } else if (digit_0100 >= 5) {
            roman.append("D");
            while (digit_0100 > 5) {
                roman.append("C");    
                digit_0100--;
            }
        } else if (digit_0100 == 4) {
            roman.append("CD");    
        } else {
            while (digit_0100>0) {
                roman.append("C");    
                digit_0100--;
            }    
        }

        if (digit_0010 == 9) {
            roman.append("XC");
        } else if (digit_0010 >= 5) {
            roman.append("L");
            while (digit_0010 > 5) {
                roman.append("X");    
                digit_0010--;
            }
        } else if (digit_0010 == 4) {
            roman.append("XL");    
        } else {
            while (digit_0010>0) {
                roman.append("X");    
                digit_0010--;
            }    
        }

        if (digit_0001 == 9) {
            roman.append("IX");
        } else if (digit_0001 >= 5) {
            roman.append("V");
            while (digit_0001 > 5) {
                roman.append("I");    
                digit_0001--;
            }
        } else if (digit_0001 == 4) {
            roman.append("IV");    
        } else {
            while (digit_0001>0) {
                roman.append("I");    
                digit_0001--;
            }    
        }

        return roman;
    }
};

int main(int argc, char *argv[]) {
    
    //int input = 2147483647;
    int input = atoi(argv[1]);
    string expected(argv[2]) ;


    Solution sol;

    string output = sol.intToRoman(input);

    cout << "input = '" << input <<"'" <<endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output.compare(expected)==0) {
        printf("match\n\n");
        return 0;
    } else {
        printf("mismatch\n\n");
        return 1;   
    }
}
