#include <iostream>
#include <math.h>


class Solution {
public:
    int reverse(int x) {

        long result=0;
        
        bool sign = (x>=0)?true:false;

        if (sign==false)
            x = abs(x);

        int len = log(x)/log(10)+1;


        //printf("len = %d\n", len);

        for(int i=0,j=len-1;i<len;i++,j--){
            long old_unit = pow(10,i);
            long mold = pow(10,i+1);
            long new_unit = pow(10,j);
            long digit = (x % mold)/old_unit;
            
            result += digit*new_unit;
            x -= digit*old_unit;

            if (result>INT_MAX) {
                printf("overflow\n");
                return 0;
            }

            bool result_sign = (result>=0)?true:false;

            //printf("digit = %ld\n", digit);
            //printf("old_unit=%ld, new_unit=%ld\n", old_unit, new_unit);
            //printf("x=%d, result=%ld\n", x, result);
        }

        return (sign)?result:-result;
    }
};

int main() {
    
    //int input = 2147483647;
    int input = 1534236469;
    int expected = 0;

    Solution sol;

    int output = sol.reverse(input);

    printf("input    = %d\n", input);
    printf("output   = %d\n", output);
    printf("expected = %d\n", expected);
    printf("MAX_INT = %d\n", INT_MAX);
    printf("MIN_INT = %d\n", INT_MIN);

    if (output==expected)
        printf("match\n");
    else
        printf("mismatch\n");
    
    return 0;
}
