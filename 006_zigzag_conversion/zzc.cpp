#include <stdio.h>
#include <string>
#include <iostream>
#include <stdbool.h>

using namespace std;

class Solution {
public:
    string convert(string s, int numRows) {
        string *rows = new string[numRows];
        int len = s.size();
        int row = 0;
        bool increase_row=true;

        for (int i=0; i<len; i++) {
            //printf("i=%d,row=%d\n", i, row);
            rows[row].push_back(s[i]);

            if (numRows-1 == 0)
                continue;
            else if (row==numRows-1)
                increase_row=false;
            else if (row==0)
                increase_row=true;
            
            if (increase_row==true)
                row++;
            else
                row--;
        }

        string result;

        for(int i=0;i<numRows;i++)
            result.append(rows[i]);

        return result;
    }
};


int main() {

    string input("abcdefghijklmnopq");
    string result("agmbfhlnceikoqdjp");
    int numRows = 4;
    //string input("ab");
    //string result("ab");
    //int numRows = 1;

    Solution sol;

    string output = sol.convert(input, numRows);

    printf("output = %s\n", output.c_str());
    printf("result = %s\n", result.c_str());

    if (result.compare(output)!=0) {
        cout << "mismatch\n";    
    } else {
        cout << "match\n";    
    }
    
    return 0;    
}
