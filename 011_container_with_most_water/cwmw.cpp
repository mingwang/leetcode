#include <iostream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    void split (string input, char c, vector<int>& out) {

        for (int i = 1; i<input.size();) {
           int new_i = input.find (c, i);
           if (new_i==string::npos)
               new_i = input.size();
           string s = input.substr(i,new_i-i+1);
           int ele = atoi(s.c_str());
           out.push_back(ele);
           i=new_i+1;
        }
    }

    int maxArea_brutforce(vector<int>& height) {
        int max_i=0,max_j=0,max_a=-1;
        for (int i=0; i<height.size()-1;i++) {
            for (int j=i+1; j<height.size();j++) {
                int area = min(height[i],height[j])*(j-i);
                //printf(" height[%d]=%d - height[%d]=%d, - area=%d\n", i,height[i],j,height[j], area);
                if (area>max_a) {
                    max_a=area;
                    max_i=i;
                    max_j=j;
                }

            }    
        }
        printf(" height[%d]=%d - height[%d]=%d\n", max_i,height[max_i],max_j,height[max_j]);
        return max_a;
    }

    int maxArea(vector<int>& height) {

        int water = 0;
        int i=0,j=height.size()-1;

        while (i<j) {
            int h = min(height[i],height[j]);
            water = max (water, h * (j-i));

            while (height[i]<=h && i<j) i++;
            while (height[j]<=h && i<j) j--;
        }


        return water;
    }
};

int main(int argc, char *argv[]) {
    
    //int input = 2147483647;
    string input(argv[1]);
    int expected = atoi(argv[2]) ;

    vector<int> in;

    Solution sol;

    sol.split(input, ',', in);

    int output = sol.maxArea(in);

    cout << "input = '" << input <<"'" <<endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output==expected) {
        printf("match\n\n");
        return 0;
    } else {
        printf("mismatch\n\n");
        return 1;   
    }
}
