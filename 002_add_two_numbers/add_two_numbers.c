#include <stdio.h>
#include <stdlib.h>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* createListNode (int head_value) {
    struct ListNode* head = (struct ListNode*) malloc(sizeof(struct ListNode));
    head->val = head_value;
    head->next = NULL;

    return head;
}

void appendListNode(struct ListNode* ptr, int append_value) {
    while (ptr->next!=NULL) {
        ptr = ptr->next;
    }
    ptr->next = createListNode(append_value);
}

void freeListNode(struct ListNode* head) {
    while (head!=NULL) {
        struct ListNode* deleteNode = head;
        head = head->next;
        free(deleteNode);
    }
        
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    
    struct ListNode* result = NULL;
    int carry = 0;

    while (l1!=NULL && l2!=NULL) {
        int value = (l1->val+l2->val+carry)%10;
        if (result == NULL) {
            result = createListNode(value);    
        } else {
            appendListNode(result, value);
        }
        carry = (l1->val+l2->val+carry)/10;

        if (l1->next == NULL && l2->next!=NULL) {
            appendListNode(l1,0);
        }
        if (l2->next == NULL && l1->next!=NULL) {
            appendListNode(l2,0);
        }
        l1 = l1->next;
        l2 = l2->next;
    }

    if (carry!=0) {
        appendListNode(result, carry);    
    }

    return result;

}

void printListNode (struct ListNode* head, char* comment) {

    printf("%s\n", comment);

    if (head==NULL) {
        return;
    }

    for (struct ListNode* ptr = head; ptr!=NULL; ptr=ptr->next) {
        printf("%d", ptr->val); 
        if (ptr->next != NULL) {
            printf(" -> ");
        }
    }
    printf("\n");
}

int main(){

    struct ListNode* l1 = createListNode(5);

    struct ListNode* l2 = createListNode(5);

    printListNode(l1, "l1:");
    printListNode(l2, "l2:");

    struct ListNode* result = addTwoNumbers(l1, l2);


    printListNode(result, "OUTPUT:");

    freeListNode(l1);
    freeListNode(l2);
    freeListNode(result);
    
    return 0;
}
