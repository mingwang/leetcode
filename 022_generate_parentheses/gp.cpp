#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>

using namespace std;


int getMilliCount(){
	timeb tb;
	ftime(&tb);
	int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}

int getMilliSpan(int nTimeStart){
	int nSpan = getMilliCount() - nTimeStart;
	if(nSpan < 0)
		nSpan += 0x100000 * 1000;
	return nSpan;
}

class Solution {
public:
    void split (string input, char c, vector<int>& out) {

        for (int i = 1; i<input.size();) {
           int new_i = input.find (c, i);
           if (new_i==string::npos)
               new_i = input.size();
           string s = input.substr(i,new_i-i+1);
           int ele = atoi(s.c_str());
           out.push_back(ele);
           i=new_i+1;
        }
    }

    void splitStr (string input, char c, vector<string>& out) {

        for (int i = 1; i<input.size();) {
           int new_i = input.find (c, i);
           if (new_i==string::npos)
               new_i = input.size()-1;
           string s = input.substr(i,new_i-i);
           out.push_back(s);
           i=new_i+1;
        }
    }


    void splitDoubleArray (string input, char c, vector< vector<int> >& out) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector<int> out_ele;

        for (int i = 1; i<input.size();) {
           int new_i = input.find (c, i);
           if (new_i==string::npos)
               new_i = input.size()-1;
           string s = input.substr(i,new_i-i);
           if (s[0]=='[')
               s.erase(0,1);
           if (s[s.length()-1]==']') {
               s.erase(s.length()-1,1);
               out_ele.push_back(atoi(s.c_str()));
               out.push_back(out_ele);
               out_ele.clear();
           } else {
               out_ele.push_back(atoi(s.c_str()));
           }
           //printf("substr = %s\n", s.c_str());
           i=new_i+1;
        }
    }

    string printDoubleArray (vector<vector<int> >& in) {
        string out;
        out.append("[");
        for(int i=0;i<in.size();i++) {
            out.append("[");
            for(int j=0; j<in[i].size();j++) {
                out.append(to_string(in[i][j]));
                if (j!=in[i].size()-1)
                    out.append(",");
            }
            out.append("]");
            if (i!=in.size()-1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }


    string intToRoman(int num) {
        // I - 1
        // V - 5
        // X - 10
        // L - 50
        // C - 100
        // D - 500
        // M - 1000

        num = num % 4000;

        string roman;

        int digit_0001 = num % 10;
        int digit_0010 = (num/10) % 10;
        int digit_0100 = (num/100) % 10;
        int digit_1000 = (num/1000) % 10;

        while(digit_1000>0) {
            roman.append("M"); 
            digit_1000 --;
        }

        if (digit_0100 == 9) {
            roman.append("CM");
        } else if (digit_0100 >= 5) {
            roman.append("D");
            while (digit_0100 > 5) {
                roman.append("C");    
                digit_0100--;
            }
        } else if (digit_0100 == 4) {
            roman.append("CD");    
        } else {
            while (digit_0100>0) {
                roman.append("C");    
                digit_0100--;
            }    
        }

        if (digit_0010 == 9) {
            roman.append("XC");
        } else if (digit_0010 >= 5) {
            roman.append("L");
            while (digit_0010 > 5) {
                roman.append("X");    
                digit_0010--;
            }
        } else if (digit_0010 == 4) {
            roman.append("XL");    
        } else {
            while (digit_0010>0) {
                roman.append("X");    
                digit_0010--;
            }    
        }

        if (digit_0001 == 9) {
            roman.append("IX");
        } else if (digit_0001 >= 5) {
            roman.append("V");
            while (digit_0001 > 5) {
                roman.append("I");    
                digit_0001--;
            }
        } else if (digit_0001 == 4) {
            roman.append("IV");    
        } else {
            while (digit_0001>0) {
                roman.append("I");    
                digit_0001--;
            }    
        }

        return roman;
    }

    int romanToInt(string s) {
        // I - 1
        // V - 5
        // X - 10
        // L - 50
        // C - 100
        // D - 500
        // M - 1000

        int result = 0;

        for (int i=0; i<s.length();i++) {
            int ni = (i+1>=s.length())?-1:i+1;
            switch (s[i]) {
            
                case 'M': result += 1000; break;

                case 'C': if (ni>0 && (s[ni] == 'M' || s[ni] == 'D')) result -= 100; 
                          else result += 100;
                          break;

                case 'D': result += 500; break;

                case 'L': result += 50; break;

                case 'X': if (ni>0 && (s[ni] == 'C' || s[ni] == 'L')) result -= 10;
                          else result += 10;
                          break;

                case 'V': result += 5; break;

                case 'I': if (ni>0 && (s[ni] == 'X' || s[ni] == 'V')) result -= 1;
                          else result += 1;
                          break;
            }
        }
        return result;
    }

    string longestCommonPrefix(vector<string>& strs) {

        if (strs.size()==0)
            return "";
    
        int i = 0;

        for (i=0; i<strs[0].length(); i++) {
            for (int s=1; s<strs.size(); s++) {
                if (strs[0][i]!=strs[s][i]) {
                    return strs[0].substr(0,i);
                }
            }
        }
        return strs[0].substr(0,i);
    }

    vector<vector<int> > threeSum(vector<int>& nums) {
        vector<vector <int> > result;

        std::sort(nums.begin(), nums.end());
        

        for (int i = 0; i< nums.size(); ) {
            int si = i+1;
            int ti = nums.size()-1;

            int first = nums[i];

            while (si<ti) {
                int second = nums[si];
                int third = nums[ti];

                if (first+second+third==0) {
                    vector<int> entry(3,0);
                    entry[0] = first;
                    entry[1] = second;
                    entry[2] = third;
                    result.push_back(entry);
                
                    while (second==nums[si]) si++;
                    while (third==nums[ti]) ti--;

                } else if (first+second+third>0) {
                    ti--;
                } else {
                    si++;
                }

            }

            while (first==nums[i]) i++;
        }
        
        return result;
    }

    int threeSumClosest(vector<int>& nums, int target) {
        int min_delta = INT_MAX;
        int result=0;
        std::sort(nums.begin(), nums.end());
        

        for (int i = 0; i< nums.size(); ) {
            int si = i+1;
            int ti = nums.size()-1;

            int first = nums[i];

            while (si<ti) {
                int second = nums[si];
                int third = nums[ti];

                int diff = first+second+third-target;

                if (abs(diff)<min_delta) {
                    min_delta = abs(diff);
                    result = first+second+third;
                }

                if (diff == 0) {

                    return target;

                } else if (diff>0) {
                    ti--;
                } else {
                    si++;
                }
            }
            while (first==nums[i]) i++;
        }
        
        return result;
    }

    vector<string> letterCombinations(string digits) {
        vector<string> result;

        string map[10] = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

        if (digits.size()==0) {
            return result;  
        } else if (digits.size()==1) {
            int digit = atoi(digits.substr(0,1).c_str());
            for(int i=0;i<map[digit].size();i++)
                result.push_back(map[digit].substr(i,1));
        } else {
            vector<string> comb = letterCombinations(digits.substr(1,digits.size()-1));
            
            int digit = atoi(digits.substr(0,1).c_str());
            for(int i=0;i<map[digit].size();i++) {
                for (int j=0;j<comb.size();j++) {
                    result.push_back(map[digit].substr(i,1).append(comb[j]));   
                }
            }
        }
        
        return result;
    }

    void addParenthesis(vector <string> &v, string str, int lp, int rp) {
        if (lp==0 && rp==0) {
            v.push_back(str);
        }
        if (lp>0) addParenthesis(v, str+'(', lp-1, rp+1);
        if (rp>0) addParenthesis(v, str+')', lp, rp-1);
    }

    vector<string> generateParenthesis(int n) {
        vector <string> result;
        if (n!=0)
            addParenthesis(result, "", n, 0);
        return result;
    }

};

int main(int argc, char *argv[]) {
    
    //int input = 2147483647;
    int input = atoi(argv[1]);
    string expect(argv[2]);

    vector<string > expect_out;
    Solution sol;

    sol.splitStr(expect,',',expect_out);

    int start_time = getMilliCount();
    vector<string> output = sol.generateParenthesis(input);
    printf("Runtime = %u ms\n",getMilliSpan(start_time));

    cout << "input = '" << input <<"'" <<endl;
    //cout << "output = " << output << endl;
    cout << "expected = " << expect << endl;

    bool mismatch = false;



    if (output.size()!=expect_out.size()) {
        printf("size: output=%lu, expect=%lu\n", output.size(),expect_out.size());
        mismatch=true;
    }   
    
    for (int i=0;!mismatch&&i<output.size();i++) {
        //printf("output[%d]=%s, expect_out[%d]=%s\n", i, output[i].c_str(), i, expect_out[i].c_str());

        if (output[i].compare(expect_out[i])) {
        printf("output[%d]=%s, expect_out[%d]=%s\n", i, output[i].c_str(), i, expect_out[i].c_str());
            mismatch = true;
            break;
        }
    }

    if (mismatch==false) {
        printf(" match\n\n");
        return 0;
    } else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;   
    }
}
