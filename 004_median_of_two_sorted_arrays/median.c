/*
There are two sorted arrays nums1and nums2 of size mand n respectively.

Find the median of the two sorted arrays.The overall run time complexity should be O(log(m + n)).

Example 1:
nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2 :
    nums1 = [1, 2]
    nums2 = [3, 4]

    The median is(2 + 3) / 2 = 2.5
    */


#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    int *A, *B;
    int sizeA, sizeB;


    // make sure size A is always smaller than size B
    if (nums1Size<nums2Size) {
        sizeA=nums1Size;
        sizeB=nums2Size;
        A=nums1;
        B=nums2;
    } else {
        sizeA=nums2Size;
        sizeB=nums1Size;
        A=nums2;
        B=nums1;
    }

    int max_of_left, min_of_right,i,j;
    int imin=0,imax=sizeA, half_len=(sizeA+sizeB+1)/2;

    while (imin<=imax) {
        i = (imin+imax)/2;
        j = half_len-i;

        if (i<sizeA && B[j-1]>A[i]) {
            imin = i + 1;    
        } else if (i>0 && A[i-1]>B[j]) {
            imax = i - 1;    
        } else {
            if (i==0)
                max_of_left = B[j-1];
            else if (j==0)
                max_of_left = A[i-1];
            else
                max_of_left = (A[i-1]>B[j-1])?A[i-1]:B[j-1];

            if ((sizeA+sizeB)%2==1)
                return max_of_left;

            if (i==sizeA)
                min_of_right=B[j];
            else if (j==sizeB)
                min_of_right=A[i];
            else
                min_of_right=A[i]<B[j]?A[i]:B[j];

            printf ("max_of_left=%d, min_of_right=%d\n", max_of_left,min_of_right);

            return (max_of_left+min_of_right)/2.0;

        }    
    }

    return -1.0;
}


int main(){

    int nums1[1] = {1};
    int nums1Size = 1;

    int nums2[1] = {5};
    int nums2Size = 1;

    double median = findMedianSortedArrays(nums1, nums1Size, nums2, nums2Size);

    printf("median is %lf\n", median);

    return 0;
}
