// 127_word_ladder.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//A transformation sequence from word beginWord to word endWord using a dictionary wordList is a sequence of words beginWord->s1->s2 -> ...->sk such that :
//
//Every adjacent pair of words differs by a single letter.
//Every si for 1 <= i <= k is in wordList.Note that beginWord does not need to be in wordList.
//sk == endWord
//Given two words, beginWordand endWord, and a dictionary wordList, return the number of words in the shortest transformation sequence from beginWord to endWord, or 0 if no such sequence exists.
//
//
//
//Example 1:
//
//Input: beginWord = "hit", endWord = "cog", wordList = ["hot", "dot", "dog", "lot", "log", "cog"]
//Output : 5
//Explanation : One shortest transformation sequence is "hit" -> "hot" -> "dot" -> "dog"->cog", which is 5 words long.
//Example 2 :
//
//    Input : beginWord = "hit", endWord = "cog", wordList = ["hot", "dot", "dog", "lot", "log"]
//    Output : 0
//    Explanation : The endWord "cog" is not in wordList, therefore there is no valid transformation sequence.
//
//
//    Constraints :
//
//    1 <= beginWord.length <= 10
//    endWord.length == beginWord.length
//    1 <= wordList.length <= 5000
//    wordList[i].length == beginWord.length
//    beginWord, endWord, and wordList[i] consist of lowercase English letters.
//    beginWord != endWord
//    All the words in wordList are unique.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>
#include <set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    bool isSingleTransform(string& nxt, string& cur) {
        int cnt = 0, len = cur.length();
        for (int i = 0; i < len; i++) {
            if (nxt[i] != cur[i]) {
                ++cnt;
                if (cnt > 1)
                    break;
            }
        }

        return cnt == 1;
    }

    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
        // 80ms, 87.66%
        // 13.9MB, 77.89%
         
        unordered_set<string> dict = { wordList.begin(),wordList.end() };
        if (dict.find(endWord) == dict.end())
            return 0;

        int step = 0;
        queue<string> q = queue<string>{ {beginWord} };
        dict.erase(beginWord);
        bool found = false;
        int wordLen = beginWord.length();

        while (q.empty() == false && found == false) {
            step++;
            int size = q.size();
            for (int i = 0; i < size; i++) {
                string currWord = q.front(); q.pop();
                for (int j = 0; j < wordLen; ++j) {
                    string nextWord = currWord;
                    for (char c = 'a'; c <= 'z'; ++c) {
                        nextWord[j] = c;
                        if (dict.find(nextWord) != dict.end()) {
                            q.push(nextWord);
                            dict.erase(nextWord);

                            if (nextWord == endWord) {
                                step++;
                                found = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return (found == true) ? step : 0;
    }

    int ladderLength2(string beginWord, string endWord, vector<string>& wordList) {
        // 20ms, 99.96%
        // 13.8MB, 85.27%

        unordered_set<string> list;
        for (int i = 0; i != wordList.size(); ++i) //put everything in a set for quick search
            list.insert(wordList[i]);

        if (list.find(endWord) == list.end()) //This condition is recently aded to the LC test
            return 0;

        int sol = 1;

        unordered_set<string> set1{ beginWord }; //The word can be reached in sol steps
        unordered_set<string> set2{ endWord };   //The target set

        while (set1.size()) {
            sol++;
            unordered_set<string> set_tmp;
            for (auto word : set1)
                list.erase(word);

            for (auto word : set1) {
                for (size_t i = 0; i < word.size(); ++i) {
                    string next = word;
                    for (char c = 'a'; c <= 'z'; ++c) {  // do not worry about next[i]=c already (already added in set1)
                        next[i] = c;
                        if (list.find(next) == list.end()) //if not in wordlist do nothing
                            continue;
                        if (set2.find(next) != set2.end()) //if in wordlist and in the target set
                            return sol;
                        set_tmp.insert(next); //if in wordlist but not in the target set
                    }
                }
            }

            if (set_tmp.size() > set2.size())
                swap(set_tmp, set2);
            set1 = set_tmp;
        }

        return 0;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string beginWord = string(argv[1]);
    string endWord = string(argv[2]);
    vector<string> wordList = sol.split<string>(argv[3]);

    int expected = atoi(argv[4]);

    int start_time = getMilliCount();
    int output = sol.ladderLength(beginWord, endWord, wordList);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "beginWord   = '" << beginWord << "'" << endl;
    cout << "endWOrd   = '" << endWord << "'" << endl;
    cout << "wordList   = '" << sol.print<string>(wordList) << "'" << endl;
    cout << "output = '" << output << "'" << endl;
    cout << "expected = '" << expected << "'" << endl;

    if (sol.check<int>(output, expected) == true)
        return 0;
    else
        return 1;
}

