echo off

echo %1
set "status=0"

set arr[0]="%1" "[1,2,3,4]" "[1,4,2,3]"
set arr[1]="%1" "[1,2,3,4,5]" "[1,5,2,4,3]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 