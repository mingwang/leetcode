// 010_regular_expression_matching.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an input string sand a pattern p, implement regular expression matching with support for '.' and '*' where:
//
//'.' Matches any single character.????
//'*' Matches zero or more of the preceding element.
//The matching should cover the entire input string(not partial).
//
//
//
//Example 1:
//
//Input: s = "aa", p = "a"
//Output : false
//Explanation : "a" does not match the entire string "aa".
//Example 2 :
//
//    Input : s = "aa", p = "a*"
//    Output : true
//    Explanation : '*' means zero or more of the preceding element, 'a'.Therefore, by repeating 'a' once, it becomes "aa".
//    Example 3 :
//
//    Input : s = "ab", p = ".*"
//    Output : true
//    Explanation : ".*" means "zero or more (*) of any character (.)".
//    Example 4 :
//
//    Input : s = "aab", p = "c*a*b"
//    Output : true
//    Explanation : c can be repeated 0 times, a can be repeated 1 time.Therefore, it matches "aab".
//    Example 5 :
//
//    Input : s = "mississippi", p = "mis*is*p*."
//    Output : false
//
//
//    Constraints :
//
//    1 <= s.length <= 20
//    1 <= p.length <= 30
//    s contains only lowercase English letters.
//    p contains only lowercase English letters, '.', and '*'.
//    It is guaranteed for each appearance of the character '*', there will be a previous valid character to match.

#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    #define lli int
    bool isMatch2(string s, string p) {
        //lli n = p.length();
        //lli m = s.length();
        const int n = 5;
        const int m = 3;
        lli dp[n + 1][m + 1];
        memset(dp, 0, sizeof(dp));
        dp[0][0] = 1;
        for (lli i = 1; i <= n; i++) {
            if (p[i - 1] == '*') {
                dp[i][0] = dp[i - 2][0];
            }
        }
        for (lli i = 1; i <= n; i++) {
            for (lli j = 1; j <= m; j++) {
                if (s[j - 1] == p[i - 1] || p[i - 1] == '.') {
                    dp[i][j] = dp[i - 1][j - 1];
                }
                else if (p[i - 1] == '*') {
                    if (s[j - 1] == p[i - 2] || p[i - 2] == '.') {
                        dp[i][j] = dp[i - 2][j] || dp[i][j - 1];
                    }
                    else {
                        dp[i][j] = dp[i - 2][j];
                    }
                }
                else {
                    dp[i][j] = 0;
                }
            }
        }
        return dp[n][m];
    }
    bool isMatch(string s, string p) {
        int n = p.length();
        int m = s.length();
        vector<vector<int>> dp;
        dp.resize(n + 1, vector<int>(m + 1, 0));
        dp[0][0] = 1;
        for (int i = 1; i <= n; i++) {
            if (p[i - 1] == '*') {
                dp[i][0] = dp[i - 2][0];
            }
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (s[j - 1] == p[i - 1] || p[i - 1] == '.') {
                    dp[i][j] = dp[i - 1][j - 1];
                }
                else if (p[i - 1] == '*') {
                    if (s[j - 1] == p[i - 2] || p[i - 2] == '.') {
                        dp[i][j] = dp[i - 2][j] || dp[i][j - 1];
                    }
                    else {
                        dp[i][j] = dp[i - 2][j];
                    }
                }
                else {
                    dp[i][j] = 0;
                }
            }
        }
        return dp[n][m];
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    string pattern(argv[2]);
    bool expected = atoi(argv[3]);

    Solution sol;

    bool output = sol.isMatch(input, pattern);

    cout << "input = '" << input << "'" << endl;
    cout << "pattern = '" << pattern << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output == expected) {
        printf("match\n\n");
        return 0;
    }
    else {
        printf("mismatch\n\n");
        return 1;
    }
}
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
