echo off

echo %1

set status = 0

"%1" "aa" "a" "0"
set /a status= %status% + %errorlevel%

"%1" "aa" "a*" "1"
set /a status= %status% + %errorlevel%

"%1" "ab" ".*" "1"
set /a status= %status% + %errorlevel%

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")