echo off

echo %1
set status = 0

set arr[0]="%1" "[1,2,3,4,5]" "[3,4,5,1,2]" "3"
set arr[1]="%1" "[2,3,4]" "[3,4,3]" "-1"


set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 