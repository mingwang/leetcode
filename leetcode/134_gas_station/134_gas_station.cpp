//134. Gas Station
//Medium
//
//6587
//
//650
//
//Add to List
//
//Share
//There are n gas stations along a circular route, where the amount of gas at the ith station is gas[i].
//
//You have a car with an unlimited gas tankand it costs cost[i] of gas to travel from the ith station to its next(i + 1)th station.You begin the journey with an empty tank at one of the gas stations.
//
//Given two integer arrays gasand cost, return the starting gas station's index if you can travel around the circuit once in the clockwise direction, otherwise return -1. If there exists a solution, it is guaranteed to be unique
//
//
//
//Example 1:
//
//Input: gas = [1, 2, 3, 4, 5], cost = [3, 4, 5, 1, 2]
//Output : 3
//Explanation :
//    Start at station 3 (index 3) and fill up with 4 unit of gas.Your tank = 0 + 4 = 4
//    Travel to station 4. Your tank = 4 - 1 + 5 = 8
//    Travel to station 0. Your tank = 8 - 2 + 1 = 7
//    Travel to station 1. Your tank = 7 - 3 + 2 = 6
//    Travel to station 2. Your tank = 6 - 4 + 3 = 5
//    Travel to station 3. The cost is 5. Your gas is just enough to travel back to station 3.
//    Therefore, return 3 as the starting index.
//    Example 2:
//
//Input: gas = [2, 3, 4], cost = [3, 4, 3]
//Output : -1
//Explanation :
//    You can't start at station 0 or 1, as there is not enough gas to travel to the next station.
//    Let's start at station 2 and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
//    Travel to station 0. Your tank = 4 - 3 + 2 = 3
//    Travel to station 1. Your tank = 3 - 3 + 3 = 3
//    You cannot travel back to station 2, as it requires 4 unit of gas but you only have 3.
//    Therefore, you can't travel around the circuit once no matter where you start.
//
//
//    Constraints:
//
//n == gas.length == cost.length
//1 <= n <= 105
//0 <= gas[i], cost[i] <= 104

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>
#include <set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return string(1, item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }

    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

#define SOL 4

#if SOL == 1
    // Time Limit Exceeded
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int n = gas.size();
        vector<int> path = vector<int>(n, 0);
        for (int i = 0; i < n; i++) {
            path[i] = gas[i] - cost[i];
        }
        for (int i = 0; i < n; i++) {
            int curr_cost = path[i];
            int s;
            for (s = 1; s < n && curr_cost > 0; s++) {
                curr_cost += path[(i + s) % n];
            }
            if (curr_cost >= 0 && s==n) {
                return i;
            }
        }

        return -1;
    }
#endif

#if SOL == 2
//Runtime: 172 ms, faster than 14.49 % of C++ online submissions for Gas Station.
//Memory Usage : 69.4 MB, less than 89.02 % of C++ online submissions for Gas Station.

    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int n = gas.size();
        int i = 0;
        while (i < n) {
            int sumOfGas = 0, sumOfCost = 0;
            int cnt = 0;
            while (cnt < n) {
                int j = (i + cnt) % n;
                sumOfGas += gas[j];
                sumOfCost += cost[j];
                if (sumOfCost > sumOfGas)
                    break;
                cnt++;
            }
            if (cnt == n) {
                return i;
            }
            else {
                i = i + cnt + 1;
            }
        }
        return -1;
    }
#endif


#if SOL == 3
//Runtime: 100 ms, faster than 81.59 % of C++ online submissions for Gas Station.
//Memory Usage : 69.5 MB, less than 13.82 % of C++ online submissions for Gas Station.
        int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int start = 0, total = 0, tank = 0;

        //if car fails at 'start', record the next station
        for (int i = 0; i < gas.size(); i++) {
            tank += gas[i] - cost[i];
            if (tank < 0) {
                start = i + 1; //move starting position forward
                total += tank; //add the negative tank value to total
                tank = 0; //reset tank
            }
        }
        //negative total + positive tank should be 0 or more, if so we can do a round trip and return start
        return (total + tank < 0) ? -1 : start;
    }
#endif

#if SOL == 4
        // O(N) 91ms, 97.81%
        // O(1) 130.57MB, 10.11%
        int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
            int remain_gain = 0, start = 0, gain = 0;
            int n = gas.size();
            for (int i = 0; i < n; ++i) {
                gain += (gas[i] - cost[i]);
                if (gain < 0) {
                    remain_gain += gain;
                    gain = 0;
                    start = i + 1;
                }
            }

            return (gain + remain_gain >= 0) ? start : -1;
        }
#endif

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> gas = sol.split<int>(argv[1]);
    vector<int> cost = sol.split<int>(argv[2]);
    int expected = atoi(argv[3]);

    int start_time = getMilliCount();
    int res = sol.canCompleteCircuit(gas, cost);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "gas   = '" << argv[1] << "'" << endl;
    cout << "cost   = '" << argv[2] << "'" << endl;
    cout << "expected = '" << expected << "'" << endl;
    cout << "res = '" << res << "'" << endl;

    if (sol.check<int>(res, expected) == true)
        return 0;
    else
        return 1;
}