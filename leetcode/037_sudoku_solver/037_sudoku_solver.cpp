// 037_sudoku_solver.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    vector<string> splitStr(string input, char c) {
        vector<string> out;
        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
        return out;
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    vector<vector<char>> splitDoubleCharArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<char> > out;
        vector<char> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(s[0]);
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(s[0]);
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleCharArray(vector<vector<char> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out += in[i][j];
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append("\n,");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    bool checkInt(int& out, int& exp) {
        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    bool checkBool(bool& out, bool& exp) {
        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    bool checkDoubleCharArray(vector<vector<char>>& out, vector<vector<char>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return 1;
        }

        if (out[0].size() != exp[0].size()) {
            printf("width mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[0].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d out=%c, exp=%c\n\n", i, j, out[i][j], exp[i][j]);
                    return 1;
                }
            }
        }

        printf("match\n\n");
        return 0;
    }

    bool isValidSudoku(vector<vector<char>>& board) {
        // 20ms, 77.56%
        // 21mb, 13.27%
        vector<vector<int>> col(9, vector<int>(10, 0));
        vector<vector<int>> row(9, vector<int>(10, 0));
        vector<vector<int>> blk(9, vector<int>(10, 0));

        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                if (board[i][j] != '.') {
                    int d = board[i][j] - '0';
                    if (col[j][d] == 0 &&
                        row[i][d] == 0 &&
                        blk[i / 3 * 3 + j / 3][d] == 0) {
                        col[j][d] ++;
                        row[i][d] ++;
                        blk[i / 3 * 3 + j / 3][d] ++;
                    }
                    else {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    bool isValidSudoku2(vector<vector<char>>& board) {
        // 20ms, 77.56%
        // 21mb, 13.27%
        unordered_map<char, bool> col[9], row[9], blk[9];

        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                if (board[i][j] != '.') {
                    if (col[j].find(board[i][j]) == col[j].end() &&
                        row[i].find(board[i][j]) == row[i].end() &&
                        blk[i / 3 * 3 + j / 3].find(board[i][j]) == blk[i / 3 * 3 + j / 3].end()) {
                        col[j][board[i][j]] = true;
                        row[i][board[i][j]] = true;
                        blk[i / 3 * 3 + j / 3][board[i][j]] = true;
                    }
                    else {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    bool row[9][10];
    bool col[9][10];
    bool blk[3][3][10];
    vector<pair<int, int>> pos;
    bool valid = false;

    void dfs(vector<vector<char>>& board, int step) {
        if (pos.size() == step) {
            valid = true;
            return;
        }
        int r=pos[step].first, c=pos[step].second;
        for (int d = 0; d < 9 && !valid; d++) {
            if (!row[r][d] && !col[c][d] && !blk[r / 3][c / 3][d]) {
                row[r][d] = col[c][d] = blk[r / 3][c / 3][d] = true;
                board[r][c] = d + '1';
                dfs(board, step + 1);
                row[r][d] = col[c][d] = blk[r / 3][c / 3][d] = false;
            }
        }
    }

    void solveSudoku(vector<vector<char>>& board) {
        // 4ms, 98.89%
        // 6.5MB, 25.90%

        memset(row, false, sizeof(row));
        memset(col, false, sizeof(col));
        memset(blk, false, sizeof(blk));
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int d = board[i][j] - '1';
                if (board[i][j] == '.')
                    pos.push_back({i,j});            
                else 
                    row[i][d] = col[j][d] = blk[i / 3][j / 3][d] = true;                
            }
        }
        dfs(board, 0);
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<vector<char>> board = sol.splitDoubleCharArray(argv[1], ',');
    vector<vector<char>> expected = sol.splitDoubleCharArray(argv[2], ',');

    int start_time = getMilliCount();
    sol.solveSudoku(board);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    string t = sol.printDoubleCharArray(board);

    cout << "input = '" << argv[1] << "'" << endl;
    cout << "t = '\n" << t << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    return sol.checkDoubleCharArray(board, expected);
}