echo off

echo %1
set status = 0

set arr[0]="%1" "[3,3,5,0,0,3,1,4]" "6"
set arr[1]="%1" "[7,6,4,3,1]" "0"
set arr[2]="%1" "[1,2,3,4,5]" "4"
set arr[3]="%1" "[1]" "0"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 