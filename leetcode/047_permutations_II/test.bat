echo off

echo %1
set status = 0

set arr[0]="%1" "[1,1,2]"			"[[1,1,2],[1,2,1],[2,1,1]]"		
set arr[1]="%1" "[1,2,3]"			"[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]"
  
set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")