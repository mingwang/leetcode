echo off

echo %1
set "status=0"

set arr[0]="%1" "1" "2" "0.5"
set arr[1]="%1" "2" "1" "2"
set arr[2]="%1" "4" "333" "0.(012)"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 