// 007_reverse_integer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a signed 32 - bit integer x, return x with its digits reversed.If reversing x causes the value to go outside the signed 32 - bit integer range[-231, 231 - 1], then return 0.
//
//Assume the environment does not allow you to store 64 - bit integers(signed or unsigned).
//
//
//
//Example 1:
//
//Input: x = 123
//Output : 321
//Example 2 :
//
//    Input : x = -123
//    Output : -321
//    Example 3 :
//
//    Input : x = 120
//    Output : 21
//    Example 4 :
//
//    Input : x = 0
//    Output : 0
//
//
//    Constraints :
//
//    -231 <= x <= 231 - 1

#include <iostream>
#include <math.h>


class Solution {
public:
    int reverse(int x) {

        int rev = 0;
        while (x != 0) {
            int digit = x % 10;
            x /= 10;

            if (rev > INT_MAX / 10 || (rev >= INT_MAX / 10 && digit > 7)) return 0;
            if (rev < INT_MIN / 10 || (rev <= INT_MIN / 10 && digit < -8)) return 0;

            rev = rev * 10 + digit;
        }

        return rev;

    }
};

int main() {

    //int input = 2147483647;
    //int input = 1534236469;
    //int expected = 0;

    int input = 321;
    int expected = 123;

    Solution sol;

    int output = sol.reverse(input);

    printf("input    = %d\n", input);
    printf("output   = %d\n", output);
    printf("expected = %d\n", expected);
    printf("MAX_INT = %d\n", INT_MAX);
    printf("MIN_INT = %d\n", INT_MIN);

    if (output == expected)
        printf("match\n");
    else
        printf("mismatch\n");

    return 0;
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
