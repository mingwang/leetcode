// 076_minimun_window_substring.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given two strings sand t of lengths mand n respectively, return the minimum window substring of s such that every character in t(including duplicates) is included in the window.If there is no such substring, return the empty string "".
//
//The testcases will be generated such that the answer is unique.
//
//A substring is a contiguous sequence of characters within the string.
//
//
//
//Example 1:
//
//Input: s = "ADOBECODEBANC", t = "ABC"
//Output : "BANC"
//Explanation : The minimum window substring "BANC" includes 'A', 'B', and 'C' from string t.
//Example 2 :
//
//    Input : s = "a", t = "a"
//    Output : "a"
//    Explanation : The entire string s is the minimum window.
//    Example 3 :
//
//    Input : s = "a", t = "aa"
//    Output : ""
//    Explanation : Both 'a's from t must be included in the window.
//    Since the largest window of s only has one 'a', return empty string.
//
//
//    Constraints :
//
//    m == s.length
//    n == t.length
//    1 <= m, n <= 105
//    s and t consist of uppercase and lowercase English letters.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }

    string minWindow(string s, string t) {
        // 4ms
        // 7.5MB
        int n = s.length();
        int p = t.length();
        if (p > n)
            return "";
        int m[256] = { 0 };
        int count = 0;
        for (int i = 0; i < t.length(); i++)
        {
            if (m[t[i]] == 0)
                count++;
            m[t[i]]++;
        }
        int i = 0;
        int j = 0;
        int ans = INT_MAX, start = 0;
        while (j < s.length())
        {
            m[s[j]]--;
            if (m[s[j]] == 0)
                count--;
            if (count == 0)
            {
                while (count == 0)
                {
                    if (ans > j - i + 1)
                    {
                        ans = j - i + 1;
                        start = i;
                    }
                    m[s[i]]++;
                    if (m[s[i]] > 0)
                        count++;
                    i++;
                }
            }
            j++;
        }
        if (ans != INT_MAX)
            return s.substr(start, ans);
        return "";
    }

    unordered_map<char, int> ori, cnt;

    bool check() {
        for (const auto& p : ori) {
            if (cnt[p.first] < p.second)
                return false;
        }
        return true;
    }

    string minWindow3(string s, string t) {
        // 136ms
        for (const auto& c : t) {
            ++ori[c];
        }

        int l = 0, r = -1;
        int len = INT_MAX, ansL = -1, ansR = -1;

        while (r < int(s.size())) {
            if (ori.find(s[++r]) != ori.end()) {
                ++cnt[s[r]];
            }
            while (check() && l <= r) {
                if (r - l + 1 < len) {
                    len = r - l + 1;
                    ansL = l;
                }
                if (ori.find(s[l]) != ori.end()) {
                    --cnt[s[l]];
                }
                ++l;
            }
        }
        return ansL == -1 ? string() : s.substr(ansL, len);
    }

    bool checkValid(unordered_map<char, int>& ref, unordered_map<char, int>& tally) {
        if (ref.size() == 0)
            return false;

        for (auto i : ref) {
            //cout << "ref[" << i.first << "]=" << i.second << std::flush;
            //if (tally.find(i.first) != tally.end())
            //    cout << " tally = " << tally[i.first] << std::flush;
            //else
            //    cout << " not exist in tally\n" << std::flush;
            //cout << "\n" << std::flush;
            if (tally.find(i.first) == tally.end() || tally[i.first] < i.second) {
                return false;
            }
            
        }

        return true;
    }

    string minWindow2(string s, string t) {
        unordered_map<char, int> ref;
        unordered_map<char, int> tally;
        int min_si = 0, min_len = s.length()+1;
        int si = 0, ei = 0;

        for (int i = 0; i < t.length(); ++i)
            ++ref[t[i]];
         
        while (si <= ei) {
            if (checkValid(ref, tally) == false) {
                if (ei < s.length()) {
                    ++tally[s[ei]];
                    ++ei;
                }
                else {
                    break;
                }
            }
            else {
                //cout << "si: " << si << ", ei: " << ei << "\n" << std::flush;
                if (min_len > ei - si) {
                    min_si = si;
                    min_len = ei - si;
                }
                --tally[s[si]];
                ++si;
            }
        }

        if (min_len <= s.length()) {
            //cout << "si: " << min_si << ", len=" << min_len << "\n";
            return s.substr(min_si, min_len);
        }
        else {
            return string("");
        }
        
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s = string(argv[1]);
    string t = string(argv[2]);
    string expected = string(argv[3]);

    int start_time = getMilliCount();
    string output = sol.minWindow(s,t);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "s   = '" << argv[1] << "'" << endl;
    cout << "t   = '" << argv[2] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << argv[3] << "'" << endl;

    if (sol.check<string>(output, expected) == true)
        return 0;
    else
        return 1;
}





