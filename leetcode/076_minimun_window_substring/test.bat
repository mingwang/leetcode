echo off

echo %1
set status = 0

set arr[0]="%1" "ADOBECODEBANC"					"ABC"			"BANC"
set arr[1]="%1" "a"								"a"				"a"
set arr[2]="%1" "a"								"aa"			""
set arr[3]="%1" "aa"							"aa"			"aa"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 