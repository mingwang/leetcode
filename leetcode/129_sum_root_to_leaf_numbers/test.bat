echo off

echo %1
set status = 0

set arr[0]="%1" "[1,2,3]" "25"
set arr[1]="%1" "[4,9,0,5,1]" "1026"
set arr[2]="%1" "[1,0]" "10"


set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 