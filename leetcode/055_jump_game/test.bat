echo off

echo %1
set status = 0

set arr[0]="%1" "[2,3,1,1,4]"		"1"	
set arr[1]="%1" "[3,2,1,0,4]"		"0"	
set arr[2]="%1" "[2,3,1,1,4]"		"1"		
set arr[3]="%1" "[2,3,0,1,4]"		"1"
set arr[4]="%1" "[1,2]"				"1"
set arr[5]="%1" "[3,2,1]"			"1"
set arr[6]="%1" "[0]"				"1"
set arr[7]="%1" "[5,9,3,2,1,0,2,3,3,1,0,0]" "1"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")