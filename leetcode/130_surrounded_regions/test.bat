echo off

echo %1
set status = 0

set arr[0]="%1" "[[X,X,X,X],[X,O,O,X],[X,X,O,X],[X,O,X,X]]" "[[X,X,X,X],[X,X,X,X],[X,X,X,X],[X,O,X,X]]"
set arr[1]="%1" "[[X]]" "[[X]]"
set arr[2]="%1" "[[O,X,X,O,X],[X,O,O,X,O],[X,O,X,O,X],[O,X,O,O,O],[X,X,O,X,O]]" "[[O,X,X,O,X],[X,X,X,X,O],[X,X,X,O,X],[O,X,O,O,O],[X,X,O,X,O]]]"
set arr[3]="%1" "[[O,X,O,O,X,X],[O,X,X,X,O,X],[X,O,O,X,O,O],[X,O,X,X,X,X],[O,O,X,O,X,X],[X,X,O,O,O,O]]" "[[O,X,O,O,X,X],[O,X,X,X,O,X],[X,O,O,X,O,O],[X,O,X,X,X,X],[O,O,X,O,X,X],[X,X,O,O,O,O]]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 