echo off

echo %1
set status = 0

"%1" "II"     "2" 
set /a status= %status% + %errorlevel%
"%1" "XXII"   "22" 
set /a status= %status% + %errorlevel%
"%1" "CCVII"  "207" 
set /a status= %status% + %errorlevel%
"%1" "MLXVI"  "1066"
set /a status= %status% + %errorlevel%
"%1" "MMMI"   "3001" 
set /a status= %status% + %errorlevel%
"%1" "MCMLIV" "1954" 
set /a status= %status% + %errorlevel%
"%1" "MMXIV"  "2014" 
set /a status= %status% + %errorlevel%
"%1" "IV"     "4" 
set /a status= %status% + %errorlevel%
"%1" "4"      "IV"
set /a status= %status% + %errorlevel%
"%1" "9"      "IX"
set /a status= %status% + %errorlevel%
"%1" "IX"     "9"
set /a status= %status% + %errorlevel%
"%1" "XC"     "90"
set /a status= %status% + %errorlevel%
"%1" "CD"     "400"
set /a status= %status% + %errorlevel%
"%1" "CM"     "900" 
set /a status= %status% + %errorlevel%


if %status% NEQ 0 (echo "FAIL") else (echo "PASS")
