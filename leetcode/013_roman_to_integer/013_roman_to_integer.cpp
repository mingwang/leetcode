// 013_roman_to_integer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Roman numerals are represented by seven different symbols : I, V, X, L, C, Dand M.
//
//Symbol       Value
//I             1
//V             5
//X             10
//L             50
//C             100
//D             500
//M             1000
//For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
//
//Roman numerals are usually written largest to smallest from left to right.However, the numeral for four is not IIII.Instead, the number four is written as IV.Because the one is before the five we subtract it making four.The same principle applies to the number nine, which is written as IX.There are six instances where subtraction is used :
//
//I can be placed before V(5) and X(10) to make 4 and 9.
//X can be placed before L(50) and C(100) to make 40 and 90.
//C can be placed before D(500) and M(1000) to make 400 and 900.
//Given a roman numeral, convert it to an integer.
//
//
//
//Example 1:
//
//Input: s = "III"
//Output : 3
//Example 2 :
//
//    Input : s = "IV"
//    Output : 4
//    Example 3 :
//
//    Input : s = "IX"
//    Output : 9
//    Example 4 :
//
//    Input : s = "LVIII"
//    Output : 58
//    Explanation : L = 50, V = 5, III = 3.
//    Example 5 :
//
//    Input : s = "MCMXCIV"
//    Output : 1994
//    Explanation : M = 1000, CM = 900, XC = 90 and IV = 4.
//
//
//    Constraints :
//
//    1 <= s.length <= 15
//    s contains only the characters('I', 'V', 'X', 'L', 'C', 'D', 'M').
//    It is guaranteed that s is a valid roman numeral in the range[1, 3999].

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    int romanToInt(string s) {
        unordered_map<char, int> map = { {'I', 1}, {'V', 5},  {'X', 10}, {'L', 50}, {'C', 100},
        {'D', 500}, {'M', 1000} };

        int res = 0, a, b, i = 0, n = s.length();
        if (n == 1) return map[s[i]];
        while (i < n) {
            a = map[s[i]];
            b = map[s[i + 1]];
            if (a < b) {
                res += b - a;
                i += 2;
            }
            else {
                res += a;
                i++;
            }
        }
        return res;
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    int expected = atoi(argv[2]);


    Solution sol;

    int output = sol.romanToInt(input);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output == expected) {
        printf("match\n\n");
        return 0;
    }
    else {
        printf("mismatch\n\n");
        return 1;
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
