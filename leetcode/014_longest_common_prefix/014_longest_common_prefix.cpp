// 014_longest_common_prefix.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Write a function to find the longest common prefix string amongst an array of strings.
//
//If there is no common prefix, return an empty string "".
//
//
//
//Example 1:
//
//Input: strs = ["flower", "flow", "flight"]
//Output : "fl"
//Example 2 :
//
//    Input : strs = ["dog", "racecar", "car"]
//    Output : ""
//    Explanation : There is no common prefix among the input strings.
//
//
//    Constraints :
//
//    1 <= strs.length <= 200
//    0 <= strs[i].length <= 200
//    strs[i] consists of only lower - case English letters.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }

    string longestCommonPrefix(vector<string>& strs) {

        if (strs.size() == 0)
            return "";

        int i = 0;

        for (i = 0; i < strs[0].length(); i++) {
            for (int s = 1; s < strs.size(); s++) {
                if (strs[0][i] != strs[s][i]) {
                    return strs[0].substr(0, i);
                }
            }
        }
        return strs[0].substr(0, i);
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    string expected(argv[2]);

    vector<string> in;
    Solution sol;

    sol.splitStr(input, ',', in);

    for (int i = 0; i < in.size(); i++) {
        printf("string%d = '%s'\n", i, in[i].c_str());
    }


    string output = sol.longestCommonPrefix(in);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output.compare(expected) == 0) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }
}