echo off

echo %1
set status = 0

set arr[0]="%1" "[a,abc,bc]"			"" 
set arr[1]="%1" "[abc,abcd,abce]"		"abc" 
set arr[2]="%1" "[abc]"					"abc"
set arr[3]="%1" ""						""
set arr[4]="%1" "[a,b]"					""
set arr[5]="%1" "[a,a]"					"a"

set "x=0"
:loop
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")

