echo off

echo %1
set status = 0

set arr[0]="%1" "aa"		"a"		"0" 
set arr[1]="%1" "aa"		"*"		"1"		
set arr[2]="%1" "cb"		"?a"	"0" 
set arr[3]="%1" "adceb"		"*a*b"	"1" 
set arr[4]="%1" "acdcb"		"a*c?b"	"0" 
  
set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")