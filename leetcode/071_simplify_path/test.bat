echo off

echo %1
set status = 0

set arr[0]="%1" "/home/"		"/home"					
set arr[1]="%1" "/../"			"/"		
set arr[2]="%1" "/home//foo/"	"/home/foo"		
set arr[3]="%1" "/a/./b/../../c/"	"/c"	
set arr[4]="%1" "/home//foo///"	"/home/foo"	
set arr[5]="%1" "/../"	"/"	
set arr[6]="%1" "/a//b////c/d//././/.."    "/a/b/c"
set arr[7]="%1" "/home"    "/home"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 