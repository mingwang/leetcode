// 042_trapping_rain_water.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given n non - negative integers representing an elevation map where the width of each bar is 1, compute how much water it can trap after raining.
//
//
//
//Example 1:
//
//
//Input: height = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
//Output : 6
//Explanation : The above elevation map(black section) is represented by array[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1].In this case, 6 units of rain water(blue section) are being trapped.
//Example 2:
//
//Input: height = [4, 2, 0, 3, 2, 5]
//Output : 9
//
//
//Constraints :
//
//    n == height.length
//    1 <= n <= 2 * 104
//    0 <= height[i] <= 105


#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    vector<string> splitStr(string input, char c) {
        vector<string> out;
        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
        return out;
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    vector<vector<char>> splitDoubleCharArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<char> > out;
        vector<char> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(s[0]);
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(s[0]);
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleCharArray(vector<vector<char> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out += in[i][j];
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append("\n,");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    template  <class T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return 1;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return 1;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return 1;
                }
            }
        }

        printf("match\n\n");
        return 0;
    }

    int trap(vector<int>& height) {
        // double pointer
        // Time - O(n) - 12ms - 17.11%
        // Space - O(1) - 15.7MB - 5.13%
        int ans = 0; 
        int l = 0, r = height.size() - 1, lMax = 0, rMax = 0;
        while (l < r) {
            lMax = max(lMax, height[l]);
            rMax = max(rMax, height[r]);

            if (height[l] < height[r]) {
                ans += lMax - height[l];
                l++;
            }

            if (height[l] >= height[r]) {
                ans += rMax - height[r];
                r--;
            }
        }

        return ans;
    }

    int trap_stack(vector<int>& height) {
        // stack
        // time - O(n) - 12ms - 17.11%
        // space - O(n) - 16.1MB - 5.13%

        stack<int> cur;
        int ans = 0;
        for (int i = 0; i < height.size(); i++) {
            while (cur.size() > 0 && height[cur.top()] < height[i]) {
                int top = cur.top();
                cur.pop();
                if (!cur.size())
                    break;
                int left = cur.top();
                ans += (i - left - 1) * (min(height[left], height[i]) - height[top]); 
            }
            
            cur.push(i);
        }
        return ans;
    }

    int trap_dp(vector<int>& height) {
        // dp
        // time - O(n) - 4 ms - 88.22% 
        // space - O(n) - 16.1MB - 5.13%
        int n = height.size();
        vector<int> leftMax(n, 0);
        vector<int> rightMax(n, 0);
       
        leftMax[0] = height[0];
        for (int i = 1; i < height.size(); i++) {
            leftMax[i] = max(leftMax[i - 1], height[i]);
        }

        rightMax[n - 1] = height[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            rightMax[i] = max(rightMax[i + 1], height[i]);
        } 

        int ans = 0;
        for (int i = 0; i < n; i++) {
            ans += min(leftMax[i], rightMax[i]) - height[i];

        }

        return ans;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> height = sol.split(argv[1], ',');
    int expected = atoi(argv[2]);

    int start_time = getMilliCount();
    int output = sol.trap(height);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    cout << "height = '" << argv[1] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    return sol.check<int>(output, expected);
}