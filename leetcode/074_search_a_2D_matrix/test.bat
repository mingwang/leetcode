echo off

echo %1
set status = 0

set arr[0]="%1" "[[1,3,5,7],[10,11,16,20],[23,30,34,60]]"					"3"					"1"
set arr[1]="%1" "[[1,3,5,7],[10,11,16,20],[23,30,34,60]]"					"13"				"0"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 