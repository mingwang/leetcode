echo off

echo %1
set status = 0

set arr[0]="%1" "[-10,-3,0,5,9]" "[0,-3,9,-10,null,5]" 
set arr[1]="%1" "[]" "[]"
set arr[2]="%1" "[1,3]" "[3,1]"	
set arr[3]="%1" "[3,5,8]" "[5,3,8]"	
set arr[4]="%1" "[0,1,2,3,4,5]" "[3,1,5,0,2,4]"	

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 