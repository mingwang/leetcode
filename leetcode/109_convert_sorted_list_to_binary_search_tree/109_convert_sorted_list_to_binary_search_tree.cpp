// 109_convert_sorted_list_to_binary_search_tree.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

// 106_construct_binary_tree_from_inorder_and_postorder_traversal.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//106. Construct Binary Tree from Inorder and Postorder Traversal
//Medium
//
//3356
//
//56
//
//Add to List
//
//Share
//Given two integer arrays inorder and postorder where inorder is the inorder traversal of a binary tree and postorder is the postorder traversal of the same tree, constructand return the binary tree.
//
//
//
//Example 1:
//
//
//Input: inorder = [9, 3, 15, 20, 7], postorder = [9, 15, 7, 20, 3]
//Output : [3, 9, 20, null, null, 15, 7]
//Example 2 :
//
//    Input : inorder = [-1], postorder = [-1]
//    Output : [-1]
//
//
//    Constraints :
//
//    1 <= inorder.length <= 3000
//    postorder.length == inorder.length
//    - 3000 <= inorder[i], postorder[i] <= 3000
//    inorder and postorder consist of unique values.
//    Each value of postorder also appears in inorder.
//    inorder is guaranteed to be the inorder traversal of the tree.
//    postorder is guaranteed to be the postorder traversal of the tree.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        int n = nodes.size();
        //if (index == 0) index_offset = 0;
        if (index < n && nodes[index].compare("null") != 0) {
            int val = atoi(nodes[index].c_str());
            int left_index = 2 * index + 1 - index_offset;
            int right_index = left_index + 1;
            //cout << "node: " << val << ", ind: " << index << ", l_ind:" << left_index << ", r_ind:" << right_index << "\n" << std::flush;
            left = createTree(nodes, left_index, index_offset);
            if (left == nullptr)
                index_offset = (index_offset + 1) << 1;
            right = createTree(nodes, right_index, index_offset);
            root = new TreeNode(val, left, right);
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        s[s.length() - 1] = ']';

        return s;

    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    TreeNode* foldTree(TreeNode* root, int length) {
        int right_size = (length - 1) / 2;
        int left_size = length - right_size - 1;
        TreeNode* prev = root, *right_root = root;

        if (right_size > 0) {
            for (int i = right_size; i > 0; --i) {
                prev = root;
                root = root->left;
            }
            prev->left = nullptr;
            root->left = foldTree(root->left, left_size);
            root->right = foldTree(right_root, right_size);
        }

        return root;
    }

    TreeNode* sortedListToBST(ListNode* head) {
        // 32ms, 66.35%
        // 28.7MB, 99.61%
        int count = 0;
        TreeNode* root = nullptr;
        ListNode* curr = head;
        while (curr != nullptr) {
            root = new TreeNode(curr->val, root, nullptr);
            curr = curr->next;
            ++count;
        }

        return foldTree(root, count);
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> list = sol.split<int>(argv[1]);
    ListNode* head = sol.createList(list);
    vector<string> tree_nodes = sol.split<string>(argv[2]);

    TreeNode* expected_root = sol.createTree(tree_nodes);
    string expected = sol.printTree(expected_root);

    int start_time = getMilliCount();
    TreeNode* output_root = sol.sortedListToBST(head);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));
    string output = sol.printTree(output_root);


    cout << "list   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << expected << "'" << endl;

    if (sol.check<string>(output, expected) == true)
        return 0;
    else
        return 1;
}






