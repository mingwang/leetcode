// 099_recover_binary_search_tree.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//You are given the root of a binary search tree(BST), where the values of exactly two nodes of the tree were swapped by mistake.Recover the tree without changing its structure.
//
//
//
//Example 1:
//
//
//Input: root = [1, 3, null, null, 2]
//Output : [3, 1, null, null, 2]
//Explanation : 3 cannot be a left child of 1 because 3 > 1. Swapping 1 and 3 makes the BST valid.
//Example 2:
//
//
//Input: root = [3, 1, 4, null, null, 2]
//Output : [2, 1, 4, null, null, 3]
//Explanation : 2 cannot be in the right subtree of 3 because 2 < 3. Swapping 2 and 3 makes the BST valid.
//
//
//    Constraints :
//
//    The number of nodes in the tree is in the range[2, 1000].
//    - 231 <= Node.val <= 231 - 1
//
//
//    Follow up : A solution using O(n) space is pretty straight - forward.Could you devise a constant O(1) space solution ?

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode* root) { return printTree(root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        int n = nodes.size();
        //if (index == 0) index_offset = 0;
        if (index < n && nodes[index].compare("null") != 0) {
            int val = atoi(nodes[index].c_str());
            int left_index = 2 * index + 1 - index_offset;
            int right_index = left_index + 1;
            //cout << "node: " << val << ", ind: " << index << ", l_ind:" << left_index << ", r_ind:" << right_index << "\n" << std::flush;
            left = createTree(nodes, left_index, index_offset);
            if (left == nullptr)
                index_offset = (index_offset + 1) << 1;
            right = createTree(nodes, right_index, index_offset);
            root = new TreeNode(val, left, right);
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        s[s.length() - 1] = ']';

        return s;

    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    bool isValidBST2(TreeNode* root) {
        // 16ms, 40%
        // 21.7MB, 69.84%
        vector<tuple<TreeNode*, long long int, long long int>> stack;
        if (root != nullptr) {
            stack.push_back({ root, LLONG_MIN, LLONG_MAX });
        }
        TreeNode* curr;
        long long int minimum, maximum;
        while (stack.size() > 0) {
            curr = get<0>(stack.back());
            minimum = get<1>(stack.back());
            maximum = get<2>(stack.back());
            stack.pop_back();
            long long int cur_val = curr->val;
            if (curr->left != nullptr) {
                long long int left_val = curr->left->val;
                if (left_val >= cur_val || left_val <= minimum)
                    return false;
                else
                    stack.push_back({ curr->left, minimum, min(cur_val,maximum) });
            }
            if (curr->right != nullptr) {
                long long int right_val = curr->right->val;
                if (right_val <= cur_val || right_val >= maximum)
                    return false;
                else
                    stack.push_back({ curr->right, max(cur_val, minimum), maximum });
            }

        }

        return true;
    }

    void recoverTree(TreeNode* root) {
        // O(n), 40ms, 39.84%
        // O(H),   59.2MB, 11.30%
        stack<TreeNode*> stk;
        TreeNode* x = nullptr, * y = nullptr, * pred = nullptr;
        while (stk.empty() == false || root != nullptr) {
            while (root != nullptr) {
                stk.push(root);
                root = root -> left;
            }
            root = stk.top();
            stk.pop();
            if (pred != nullptr && root->val < pred->val) {
                y = root;
                if (x == nullptr) { x = pred; }
                else { break; }
            }
            pred = root;
            root = root->right;
        }
        if (x != nullptr && y != nullptr) {
            int tmp = x->val;
            x->val = y->val;
            y->val = tmp;
        }
    }

    void recoverTree2(TreeNode* root) {
        // O(n), 32ms, 63.83%
        // O(1), 57.8MB, 88.06%

        TreeNode* x = nullptr, * y = nullptr, * pred = nullptr, * predecessor = nullptr;

        while (root != nullptr) {
            if (root->left != nullptr) {
                predecessor = root->left;
                while (predecessor->right != nullptr && predecessor->right != root) {
                    predecessor = predecessor->right;
                }
                if (predecessor->right == nullptr) {
                    predecessor->right = root;
                    root = root->left;
                }
                else {
                    if (pred != nullptr && root->val < pred->val) {
                        y = root;
                        if (x == nullptr) {
                            x = pred;
                        }
                    }
                    pred = root;
                    root = root->right;
                    predecessor->right = nullptr;
                }
            }
            else {
                if (pred != nullptr && root->val < pred->val) {
                    y = root;
                    if (x == nullptr) {
                        x = pred;
                    }
                }
                pred = root;
                root = root->right;
            }
        }

        if (x != nullptr && y != nullptr) {
            int tmp = x->val;
            x->val = y->val;
            y->val = tmp;
        }
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<string> tree_nodes = sol.split<string>(argv[1]);
    TreeNode* root = sol.createTree(tree_nodes);
    vector<string> expected_tree_nodes = sol.split<string>(argv[2]);
    TreeNode* expected = sol.createTree(expected_tree_nodes);

    int start_time = getMilliCount();
    sol.recoverTree(root);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "root   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << sol.printTree(root) << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;
    cout << "expected_str = '" << sol.printTree(expected) << "'" << endl;

    string s1 = sol.printTree(root);
    string s2 = sol.printTree(expected);

    if (sol.check<string>(s1,s2) == true)
        return 0;
    else
        return 1;
}


