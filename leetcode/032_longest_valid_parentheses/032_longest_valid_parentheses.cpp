// 032_longest_valid_parentheses.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a string containing just the characters '(' and ')', find the length of the longest valid(well - formed) parentheses substring.
//
//
//
//Example 1:
//
//Input: s = "(()"
//Output : 2
//Explanation : The longest valid parentheses substring is "()".
//Example 2 :
//
//    Input : s = ")()())"
//    Output : 4
//    Explanation : The longest valid parentheses substring is "()()".
//    Example 3 :
//
//    Input : s = ""
//    Output : 0
//
//
//    Constraints :
//
//    0 <= s.length <= 3 * 104
//    s[i] is '(', or ')'.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    vector<string> splitStr(string input, char c) {
        vector<string> out;
        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
        return out;
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    bool checkInt(string& input, int& out, int& exp) {
        cout << "input = '" << input << "'" << endl;
        cout << "output = '" << out << "'" << endl;
        cout << "expected = '" << exp << "'" << endl;

        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    int longestValidParentheses(string s) {
        // double pointer
        // time - O(n) - 4ms - 80.65%%
        // space - O(1) - 6.9MB - 86.54%
        int left = 0, right = 0, ans=0;
        
        for (int i = 0; i < s.length(); i++) {
            if (s[i] == '(')
                left++;
            else
                right++;

            if (left == right) {
                ans = max(ans, right * 2);
            }
            else if (right > left) {
                left = right = 0;
            }
        }

        left = right = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (s[i] == '(')
                left++;
            else
                right++;

            if (left == right) {
                ans = max(ans, left * 2);
            }
            else if (left > right) {
                left = right = 0;
            }
        }

        return ans;
    }

    int longestValidParentheses_stack(string s) {
        // stack
        // time - O(n) - 0ms - 100%
        // space - O(n) - 7.2MB - 63.95%
        stack<int> stk;
        int res = 0;
        
        stk.push(-1);

        for (int i = 0; i < s.length(); i++) {
            if (s[i] == ')') {
                stk.pop();
                if (stk.size()) {
                    res = max(res, i - stk.top());
                }
                else {
                    stk.push(i);
                }
            }
            else {
                stk.push(i); 
            }
        }

        return res;
    }

    int longestValidParentheses_dp(string s) {
        // dp
        // time - O(n) - 4ms - 80.65%
        // space - O(n) - 7.4MB - 17.45%
        vector<int> dp(s.length(), 0);
        int res = 0;

        for (int i = 1; i < s.length(); i++) {
            if (s[i] == ')') {
                if (s[i - 1] == '(') {
                    dp[i] = ((i - 2) > 0) ? dp[i - 2] + 2 : 2;
                }
                else if (i-dp[i-1]>0 && s[i-dp[i-1]-1]=='(') {
                    dp[i] = dp[i - 1] + 2 + ((i - dp[i - 1] - 2) > 0 ? dp[i - dp[i - 1] - 2] : 0);
                }
            }
            res = max(res, dp[i]);
        }

        return res;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s(argv[1]);
    int expected = atoi(argv[2]);

    int start_time = getMilliCount();
    int output = sol.longestValidParentheses(s);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    return sol.checkInt(s, output, expected);
}