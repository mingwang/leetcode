// 017_letter_combinations_of_a_phone_number.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a digit string, return all possible letter combinations that the number could represent.
//
//A mapping of digit to letters(just like on the telephone buttons) is given below.
//
//Input:Digit string "23"
//Output : ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"] .
//Note :
//    Although the above answer is in lexicographical order, your answer could be in any order you want.


#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    void splitDoubleArray(string input, char c, vector< vector<int> >& out) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector<int> out_ele;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    vector<string> letterCombinations(string digits) {
        vector<string> result;

        unordered_map<char,string> map = { {'0', ""},{'1',""},{'2',"abc"},{'3',"def"},
            {'4',"ghi"},{'5',"jkl"},{'6',"mno"},
            {'7',"pqrs"},{'8',"tuv"},{'9',"wxyz"} };


        for (int i = 0; i < digits.size(); i++) {
            int prev_length = result.size();

            if (!prev_length) {
                for (int j = 0; j < map[digits[i]].size(); j++) {
                    string s;
                    result.push_back(s + map[digits[i]][j]);
                }
            }
            else {
                for (int k = 0; k < prev_length; k++) {
                    for (int j = 0; j < map[digits[i]].size(); j++) {
                        result.push_back(result[k] + map[digits[i]][j]);
                    }
                }
            }
            
            result = { result.begin() + prev_length, result.end() };
        }

        return result;
    }

    vector<string> letterCombinations2(string digits) {
        vector<string> result;

        string map[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };

        if (digits.size() == 0) {
            return result;
        }
        else if (digits.size() == 1) {
            int digit = atoi(digits.substr(0, 1).c_str());
            for (int i = 0; i < map[digit].size(); i++)
                result.push_back(map[digit].substr(i, 1));
        }
        else {
            vector<string> comb = letterCombinations(digits.substr(1, digits.size() - 1));

            int digit = atoi(digits.substr(0, 1).c_str());
            for (int i = 0; i < map[digit].size(); i++) {
                for (int j = 0; j < comb.size(); j++) {
                    result.push_back(map[digit].substr(i, 1).append(comb[j]));
                }
            }
        }

        return result;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    string expect(argv[2]);

    vector<string > expect_out;
    Solution sol;

    sol.splitStr(expect, ',', expect_out);

    int start_time = getMilliCount();
    vector<string> output = sol.letterCombinations(input);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    cout << "input = '" << input << "'" << endl;
    //cout << "output = " << output << endl;
    cout << "expected = " << expect << endl;

    bool mismatch = false;

    for (int i = 0; i < output.size(); i++) {
        //printf("output[%d]=%s, expect_out[%d]=%s\n", i, output[i].c_str(), i, expect_out[i].c_str());

        if (output[i].compare(expect_out[i])) {
            printf("output[%d]=%s, expect_out[%d]=%s\n", i, output[i].c_str(), i, expect_out[i].c_str());
            mismatch = true;
            break;
        }
    }

    if (output.size() != expect_out.size()) {
        printf("size: output=%d, expect=%d\n", output.size(), expect_out.size());
        mismatch = true;
    }

    if (mismatch == false) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }
}