echo off

echo %1
set status = 0

set arr[0]="%1"  "23"     "[ad,ae,af,bd,be,bf,cd,ce,cf]"
set arr[1]="%1"  "2"     "[a,b,c]"
set arr[2]="%1"  "213"     ""
set arr[3]="%1"  "234"     "[adg,adh,adi,aeg,aeh,aei,afg,afh,afi,bdg,bdh,bdi,beg,beh,bei,bfg,bfh,bfi,cdg,cdh,cdi,ceg,ceh,cei,cfg,cfh,cfi]"

set "x=0"
:loop
if defined arr[%x%] (
	call echo Running test %x%: %%arr[%x%]%%
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")