// 008_string_to_integer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Implement the myAtoi(string s) function, which converts a string to a 32 - bit signed integer(similar to C / C++'s atoi function).
//
//    The algorithm for myAtoi(string s) is as follows :
//
//Read inand ignore any leading whitespace.
//Check if the next character(if not already at the end of the string) is '-' or '+'.Read this character in if it is either.This determines if the final result is negative or positive respectively.Assume the result is positive if neither is present.
//Read in next the characters until the next non - digit charcter or the end of the input is reached.The rest of the string is ignored.
//Convert these digits into an integer(i.e. "123" -> 123, "0032" -> 32).If no digits were read, then the integer is 0. Change the sign as necessary(from step 2).
//If the integer is out of the 32 - bit signed integer range[-231, 231 - 1], then clamp the integer so that it remains in the range.Specifically, integers less than - 231 should be clamped to - 231, and integers greater than 231 - 1 should be clamped to 231 - 1.
//Return the integer as the final result.
//Note:
//
//Only the space character ' ' is considered a whitespace character.
//Do not ignore any characters other than the leading whitespace or the rest of the string after the digits.
//
//
//Example 1:
//
//Input: s = "42"
//Output : 42
//Explanation : The underlined characters are what is read in, the caret is the current reader position.
//Step 1 : "42" (no characters read because there is no leading whitespace)
//^
//Step 2 : "42" (no characters read because there is neither a '-' nor '+')
//^
//Step 3 : "42" ("42" is read in)
//^
//The parsed integer is 42.
//Since 42 is in the range[-231, 231 - 1], the final result is 42.
//Example 2 :
//
//    Input : s = "   -42"
//    Output : -42
//    Explanation :
//    Step 1 : "   -42" (leading whitespace is read and ignored)
//    ^
//    Step 2 : "   -42" ('-' is read, so the result should be negative)
//    ^
//    Step 3 : "   -42" ("42" is read in)
//    ^
//    The parsed integer is - 42.
//    Since - 42 is in the range[-231, 231 - 1], the final result is - 42.
//    Example 3 :
//
//    Input : s = "4193 with words"
//    Output : 4193
//    Explanation :
//    Step 1 : "4193 with words" (no characters read because there is no leading whitespace)
//    ^
//    Step 2 : "4193 with words" (no characters read because there is neither a '-' nor '+')
//    ^
//    Step 3 : "4193 with words" ("4193" is read in; reading stops because the next character is a non - digit)
//    ^
//    The parsed integer is 4193.
//    Since 4193 is in the range[-231, 231 - 1], the final result is 4193.
//    Example 4 :
//
//    Input : s = "words and 987"
//    Output : 0
//    Explanation :
//    Step 1 : "words and 987" (no characters read because there is no leading whitespace)
//    ^
//    Step 2 : "words and 987" (no characters read because there is neither a '-' nor '+')
//    ^
//    Step 3 : "words and 987" (reading stops immediately because there is a non - digit 'w')
//    ^
//    The parsed integer is 0 because no digits were read.
//    Since 0 is in the range[-231, 231 - 1], the final result is 0.
//    Example 5 :
//
//    Input : s = "-91283472332"
//    Output : -2147483648
//    Explanation :
//    Step 1 : "-91283472332" (no characters read because there is no leading whitespace)
//    ^
//    Step 2 : "-91283472332" ('-' is read, so the result should be negative)
//    ^
//    Step 3 : "-91283472332" ("91283472332" is read in)
//    ^
//    The parsed integer is - 91283472332.
//    Since - 91283472332 is less than the lower bound of the range[-231, 231 - 1], the final result is clamped to - 231 = -2147483648.
//
//
//    Constraints :
//
//    0 <= s.length <= 200
//    s consists of English letters(lower - caseand upper - case), digits(0 - 9), ' ', '+', '-', and '.'.

#include <iostream>
#include <math.h>
#include <string>

using namespace std;


class Solution {
public:
    int myAtoi(string s) {
        int i = 0;
        //skip white space
        while (s[i] == ' ')
            i++;

        bool positive = false;

        if (s[i] == '+') {
            positive = true;
            i++;
        }
        else if (s[i] >= '0' && s[i] <= '9') {
            positive = true;
        }
        else if (s[i] == '-') {
            i++;
        }
        else {
            // not start with +, -, or 0-9
            return 0;
        }

        int result = 0;
        while (s[i] >= '0' && s[i] <= '9') {
            int digit = (positive)?s[i] - '0':'0'- s[i];
            if (positive) {
                if (result > INT_MAX / 10 || (result >= INT_MAX / 10 && digit > 7)) {
                    return INT_MAX;
                }
            }
            else {
                if (result < INT_MIN / 10 || (result <= INT_MIN / 10 && digit < -8)) {
                    return INT_MIN;
                }
            }
            result = result * 10 + digit;
            i++;
        }

        return result;
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    int expected = atoi(argv[2]);

    Solution sol;

    int output = sol.myAtoi(input);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output == expected) {
        printf("match\n\n");
        return 0;
    }
    else {
        printf("mismatch\n\n");
        return 1;
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
