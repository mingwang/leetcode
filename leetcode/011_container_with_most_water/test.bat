echo off

echo %1

set status = 0

"%1" "[1,2,3,4,5,6,7,8,9]" "20"
set /a status= %status% + %errorlevel%

"%1" "[1,2]" "1"
set /a status= %status% + %errorlevel%

"%1" "[5,5,5,5,5,6]" "25"
set /a status= %status% + %errorlevel%


if %status% NEQ 0 (echo "FAIL") else (echo "PASS")