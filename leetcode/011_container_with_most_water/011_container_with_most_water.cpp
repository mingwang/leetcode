// 011_container_with_most_water.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given n non - negative integers a1, a2, ..., an, where each represents a point at coordinate(i, ai).n vertical lines are drawn such that the two endpoints of the line i is at(i, ai) and (i, 0).Find two lines, which, together with the x - axis forms a container, such that the container contains the most water.
//
//Notice that you may not slant the container.
//
//
//
//Example 1:
//
//
//Input: height = [1, 8, 6, 2, 5, 4, 8, 3, 7]
//Output : 49
//Explanation : The above vertical lines are represented by array[1, 8, 6, 2, 5, 4, 8, 3, 7].In this case, the max area of water(blue section) the container can contain is 49.
//Example 2:
//
//Input: height = [1, 1]
//Output : 1
//Example 3 :
//
//    Input : height = [4, 3, 2, 1, 4]
//    Output : 16
//    Example 4 :
//
//    Input : height = [1, 2, 1]
//    Output : 2
//
//
//    Constraints :
//
//    n == height.length
//    2 <= n <= 105
//    0 <= height[i] <= 104

#include <iostream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    int maxArea_brutforce(vector<int>& height) {
        int max_i = 0, max_j = 0, max_a = -1;
        for (int i = 0; i < height.size() - 1; i++) {
            for (int j = i + 1; j < height.size(); j++) {
                int area = min(height[i], height[j]) * (j - i);
                //printf(" height[%d]=%d - height[%d]=%d, - area=%d\n", i,height[i],j,height[j], area);
                if (area > max_a) {
                    max_a = area;
                    max_i = i;
                    max_j = j;
                }

            }
        }
        printf(" height[%d]=%d - height[%d]=%d\n", max_i, height[max_i], max_j, height[max_j]);
        return max_a;
    }

    int maxArea(vector<int>& height) {

        int water = 0;
        int i = 0, j = height.size() - 1;

        while (i < j) {
            int h = min(height[i], height[j]);
            water = max(water, h * (j - i));

            while (height[i] <= h && i < j) i++;
            while (height[j] <= h && i < j) j--;
        }


        return water;
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    int expected = atoi(argv[2]);

    vector<int> in;

    Solution sol;

    sol.split(input, ',', in);

    int output = sol.maxArea(in);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output == expected) {
        printf("match\n\n");
        return 0;
    }
    else {
        printf("mismatch\n\n");
        return 1;
    }
}
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
