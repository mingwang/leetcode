// 004_median_of_two_sorted_arrays.cpp : This file contains the 'main' function. Program execution begins and ends there.
/*
There are two sorted arrays nums1and nums2 of size mand n respectively.

Find the median of the two sorted arrays.The overall run time complexity should be O(log(m + n)).

Example 1:
nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2 :
    nums1 = [1, 2]
    nums2 = [3, 4]

    The median is(2 + 3) / 2 = 2.5
    */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

using namespace std;

double findMedianSortedArrays3(vector<int>& nums1, vector<int>& nums2) {
    const int n1 = nums1.size();
    const int n2 = nums2.size();

    if (n1 > n2) {
        return findMedianSortedArrays3(nums2, nums1);
    }

    int l = 0;
    int r = n1;

    while (l <= r) {
        const int part1 = l + (r - l) / 2;
        const int part2 = (n1 + n2 + 1) / 2 - part1;
        const int maxLeft1 = part1 == 0 ? INT_MIN : nums1[part1 - 1];
        const int maxLeft2 = part2 == 0 ? INT_MIN : nums2[part2 - 1];
        const int minRight1 = part1 == n1 ? INT_MAX : nums1[part1];
        const int minRight2 = part2 == n2 ? INT_MAX : nums2[part2];

        if (maxLeft1 <= minRight2 && maxLeft2 <= minRight1) {
            return (n1 + n2) % 2 == 0 ? (max(maxLeft1, maxLeft2) + min(minRight1, minRight2)) * 0.5 : max(maxLeft1, maxLeft2);
        }
        else if (maxLeft1 > minRight2) {
            r = part1 - 1;
        }
        else {
            l = part1 + 1;
        }
    }

    throw;
}

double findMedianSortedArrays2(vector<int>& nums1, vector<int>& nums2) {
    // O(m+n)
    int len = nums1.size() + nums2.size();
    int first = 0;
    int second = 0;
    int ret = -1;
    int prev = -1;

    for (int i = 0; i <= len / 2; i++) {
        if (second >= nums2.size()) {
            prev = ret;
            ret = nums1[first];
            first++;
        }
        else if (first >= nums1.size()) {
            prev = ret;
            ret = nums2[second];
            second++;
        }
        else if (nums1[first] < nums2[second]) {
            prev = ret;
            ret = nums1[first];
            first++;
        }
        else {
            prev = ret;
            ret = nums2[second];
            second++;
        }
    }
    if (len % 2 == 0)
        return (ret + prev) / 2.0;
    
    return ret;
}

double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    int* A, * B;
    int sizeA, sizeB;


    // make sure size A is always smaller than size B
    if (nums1Size < nums2Size) {
        sizeA = nums1Size;
        sizeB = nums2Size;
        A = nums1;
        B = nums2;
    }
    else {
        sizeA = nums2Size;
        sizeB = nums1Size;
        A = nums2;
        B = nums1;
    }

    int max_of_left, min_of_right, i, j;
    int imin = 0, imax = sizeA, half_len = (sizeA + sizeB + 1) / 2;

    while (imin <= imax) {
        i = (imin + imax) / 2;
        j = half_len - i;

        if (i<sizeA && B[j - 1]>A[i]) {
            imin = i + 1;
        }
        else if (i > 0 && A[i - 1] > B[j]) {
            imax = i - 1;
        }
        else {
            if (i == 0)
                max_of_left = B[j - 1];
            else if (j == 0)
                max_of_left = A[i - 1];
            else
                max_of_left = (A[i - 1] > B[j - 1]) ? A[i - 1] : B[j - 1];

            if ((sizeA + sizeB) % 2 == 1)
                return max_of_left;

            if (i == sizeA)
                min_of_right = B[j];
            else if (j == sizeB)
                min_of_right = A[i];
            else
                min_of_right = A[i] < B[j] ? A[i] : B[j];

            printf("max_of_left=%d, min_of_right=%d\n", max_of_left, min_of_right);

            return (max_of_left + min_of_right) / 2.0;

        }
    }

    return -1.0;
}


int main() {

    int nums1[1] = { 1 }; 
    int nums1Size = 1;

    int nums2[1] = { 5 };
    int nums2Size = 1;

    double median;
    //median = findMedianSortedArrays(nums1, nums1Size, nums2, nums2Size);

    //vector<int> A = { 1, 2, 6, 7 };
    //vector<int> B = { 4, 5 };
    vector<int> A = { 2 };
    vector<int> B = {  };

    median = findMedianSortedArrays3(A, B);

    printf("median is %lf\n", median);

    return 0;
}
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
