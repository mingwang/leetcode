echo off

echo %1
set "status=0"

set arr[0]="%1" "[3,4,5,1,2]" "1"
set arr[1]="%1" "[4,5,6,7,0,1,2]" "0"
set arr[2]="%1" "[11,13,15,17]" "11"
set arr[3]="%1" "[2,2,2,0,1]" "0"
set arr[4]="%1" "[1,1]" "1"
set arr[5]="%1" "[3,1,1]" "1"
set arr[6]="%1" "[3,1,3]" "1"
set arr[7]="%1" "[3,3,1,3]" "1"
set arr[8]="%1" "[3,3,1,3,3,3,3]" "1"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 