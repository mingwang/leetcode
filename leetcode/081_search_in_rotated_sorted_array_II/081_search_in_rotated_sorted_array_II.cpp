// 081_search_in_rotated_sorted_array_II.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//There is an integer array nums sorted in non - decreasing order(not necessarily with distinct values).
//
//Before being passed to your function, nums is rotated at an unknown pivot index k(0 <= k < nums.length) such that the resulting array is[nums[k], nums[k + 1], ..., nums[n - 1], nums[0], nums[1], ..., nums[k - 1]](0 - indexed).For example, [0, 1, 2, 4, 4, 4, 5, 6, 6, 7] might be rotated at pivot index 5 and become[4, 5, 6, 6, 7, 0, 1, 2, 4, 4].
//
//    Given the array nums after the rotation and an integer target, return true if target is in nums, or false if it is not in nums.
//
//    You must decrease the overall operation steps as much as possible.
//
//
//
//    Example 1:
//
//Input: nums = [2, 5, 6, 0, 0, 1, 2], target = 0
//Output : true
//Example 2 :
//
//    Input : nums = [2, 5, 6, 0, 0, 1, 2], target = 3
//    Output : false
//
//
//    Constraints :
//
//    1 <= nums.length <= 5000
//    - 104 <= nums[i] <= 104
//    nums is guaranteed to be rotated at some pivot.
//    - 104 <= target <= 104
//
//
//    Follow up : This problem is similar to Search in Rotated Sorted Array, but nums may contain duplicates.Would this affect the runtime complexity ? How and why ?

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }


    bool search(vector<int>& nums, int target) {
        // O(logn): 0ms, 100%, worst case O(n);
        // O(1): 13.8MB, 90.7%

        int l = 0, r = nums.size() - 1, mid;

        while (l <= r) {

            mid = (l + r) / 2;

            if (nums[mid] == target || nums[l] == target || nums[r] == target)
                return true;
            
            if (nums[l] == nums[r]) {
                ++l;
                --r;
            }
            else if (nums[l] <= nums[mid]) {
                if (nums[l] <= target && target <= nums[mid]) {
                    r = mid - 1;
                }
                else {
                    l = mid + 1;
                }
            }
            else if (nums[mid]<=nums[r]) {
                if (nums[mid] <= target && target <= nums[r]) {
                    l = mid + 1;
                }
                else {
                    r = mid - 1;
                }
            }
        }

        return false;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> nums = sol.split<int>(argv[1]);
    int target = atoi(argv[2]);
    bool expected = (atoi(argv[3]) == 1);


    int start_time = getMilliCount();
    bool output = sol.search(nums, target);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "nums   = '" << argv[1] << "'" << endl;
    cout << "target   = '" << target << "'" << endl;
    cout << "output = '" << output << "'" << endl;
    cout << "expected = '" << argv[3] << "'" << endl;

    if (sol.check<bool>(expected, output) == true)
        return 0;
    else
        return 1;
}









