// 033_search_in_rotated_sorted_array.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//
//There is an integer array nums sorted in ascending order(with distinct values).
//
//Prior to being passed to your function, nums is rotated at an unknown pivot index k(0 <= k < nums.length) such that the resulting array is[nums[k], nums[k + 1], ..., nums[n - 1], nums[0], nums[1], ..., nums[k - 1]](0 - indexed).For example, [0, 1, 2, 4, 5, 6, 7] might be rotated at pivot index 3 and become[4, 5, 6, 7, 0, 1, 2].
//
//    Given the array nums after the rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.
//
//    You must write an algorithm with O(log n) runtime complexity.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    vector<string> splitStr(string input, char c) {
        vector<string> out;
        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
        return out;
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    bool checkInt(string& input, int& out, int& exp) {
        cout << "input = '" << input << "'" << endl;
        cout << "output = '" << out << "'" << endl;
        cout << "expected = '" << exp << "'" << endl;

        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    int search(vector<int>& nums, int target) {
        // O(logn) = 4ms, 74.55%
        // 11MB - 73.80%
        int l = 0, r = nums.size() - 1, p;
        while (l <= r) {
            p = (l + r) / 2;
            if (nums[p] == target)
                return p;
            
            if (nums[p]>= nums[l]) {
                if (nums[l] <= target && target <= nums[p]) {
                    r = p - 1;
                }
                else {
                    l = p + 1;
                }
            }
            else if (nums[r] >= nums[p]) {
                if (nums[p] <= target && target <= nums[r]) {
                    l = p + 1;
                }
                else {
                    r = p - 1;
                }
            }
        }
        return -1;
    }

    int search2(vector<int>& nums, int target) {
        // O(logn) = 0ms, 100%
        // 11.2MB - 28.57%
        int l = 0, r = nums.size() - 1, p = (l + r) / 2;

        while (l < r) {
            if (nums[p] > nums[r]) {
                l = p + 1;
            }
            else {
                r = p;
            }
            p = (l + r) / 2;
        }
        if (nums[p] <= target && target <= nums[nums.size() - 1]) {
            l = p;
            r = nums.size() - 1;
        }
        else if (nums[0] <= target && p>0 && target <= nums[p-1]) {
            l = 0;
            r = p - 1;
        }
        else {
            return -1;  
        }

        if (l == r && target == nums[l])
            return l;

        p = (l + r) / 2;

        while (l < r) {   
            if (nums[p] == target)
                return p;
            else if (nums[p] < target)
                l = p+1;
            else
                r = p-1;
            p = (l + r) / 2;
        }

        if (nums[p] == target)
            return p;
        else
            return -1;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> nums = sol.split(argv[1], ',');
    int target = atoi(argv[2]);
    int expected = atoi(argv[3]);

    int start_time = getMilliCount();
    int output = sol.search(nums, target);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    string in_str = sol.print(nums);
    cout << "target = '" << target << "'" << endl;

    return sol.checkInt(in_str, output, expected);
}