echo off

echo %1
set status = 0

set arr[0]="%1" "[4,5,6,7,0,1,2]"			"0"			"4"
set arr[1]="%1" "[4,5,6,7,0,1,2]"			"3"			"-1"		
set arr[2]="%1" "[1]"						"0"			"-1"
set arr[3]="%1" "[1,2]"						"0"			"-1"
set arr[4]="%1" "[1,2]"						"1"			"0"
set arr[5]="%1" "[2,1]"						"1"			"1"
set arr[6]="%1" "[2,1]"						"0"			"-1"
set arr[7]="%1" "[1]"						"1"			"0"
set arr[8]="%1" "[1,3]"						"2"			"-1"
set arr[9]="%1" "[3,1]"						"3"			"0"
set arr[9]="%1" "[1,3,5]"					"3"			"1"

set "x=0"
:loop
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")