// 003_longest_Substring_without_repeating_characters.cpp : This file contains the 'main' function. Program execution begins and ends there.
/*
Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.

Subscribe to see which companies asked this question.
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unordered_map>

using namespace std;

void printSubStr(string s, int s_index, int e_index) {
    if (s_index<0 || e_index<0 || s_index>s.length() - 1 || e_index>s.length() - 1) {
        printf("input outbound\n");
        return;
    }

    printf("[%d,%d] => ", s_index, e_index);

    for (int i = s_index; i <= e_index; i++) {
        printf("%c", s[i]);
    }
    printf("\n");
}

int lengthOfLongestSubstring1(string s) {
    // 4 ms

    vector<int> map(128, -1);
    int step = 1;
    int max_len = s.length() ? 1 : 0;
    map[s[0]] = 0;

    for (int i = 1; i < s.length(); i++) {
        step = min(step + 1, i - map[s[i]]);
        map[s[i]] = i;
        max_len = max(max_len, step);
    }

    return max_len;
}

int lengthOfLongestSubstring2(string s) {
    // 16 ms

    int len = s.length();
    int s_index = 0;
    int e_index = 0;
    int max_len = 0;
    unordered_map<char, int> map;


    for (int i = 0; i < len; i++) {
        //printf("%d char %c %d\n", i, s[i], s[i]);
        //printf("map[u] = %d\n", map['u']);
        auto search = map.find(s[i]);
        if (search != map.end()) {
            if (s_index <= map[s[i]])
                e_index = i - 1;
            else
                e_index = i;
            if (e_index - s_index + 1 > max_len) {
                max_len = e_index - s_index + 1;
            }
            if (map[s[i]] + 1 > s_index)
                s_index = map[s[i]] + 1;
            map[s[i]] = i;
        }
        else {
            e_index = i;
            if (e_index - s_index + 1 > max_len) {
                max_len = e_index - s_index + 1;
            }
            map[s[i]] = i;
        }
        printSubStr(s, s_index, e_index);
    }

    return max_len;
}


int main()
{

    //char* input = "abcabcdbxyzjklc";
    //char* input = "tmmzuxt";
    //char* input = "ggububgvfk";
    //char* input = "xbcabcd";
    //char* input = "abcadef";
    string input = "aabbccabc";

    //printSubStr(input,0,0);
    //printSubStr(input,0,1);
    //printSubStr(input,0,7);
    
    printSubStr(input,0, input.length());

    int longest_length = lengthOfLongestSubstring1(input);

    printf("longest length = %d\n", longest_length);

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
