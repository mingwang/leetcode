echo off

echo %1
set status = 0

set arr[0]="%1" "[10,1,2,7,6,1,5]"	"8" "[[1,1,6],[1,2,5],[1,7],[2,6]]"
set arr[1]="%1" "[2,5,2,1,2]"		"5"	"[[1,2,2],[5]]"
set arr[2]="%1" "[2]"		"1"	"[]"
set arr[3]="%1" "[1]"		"1"	"[[1]]"
set arr[4]="%1" "[1]"		"2"	"[]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")