// 091_decode_ways.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//A message containing letters from A - Z can be encoded into numbers using the following mapping :
//
//'A' -> "1"
//'B' -> "2"
//...
//'Z' -> "26"
//To decode an encoded message, all the digits must be grouped then mapped back into letters using the reverse of the mapping above(there may be multiple ways).For example, "11106" can be mapped into :
//
//"AAJF" with the grouping(1 1 10 6)
//"KJF" with the grouping(11 10 6)
//Note that the grouping(1 11 06) is invalid because "06" cannot be mapped into 'F' since "6" is different from "06".
//
//Given a string s containing only digits, return the number of ways to decode it.
//
//The answer is guaranteed to fit in a 32 - bit integer.
//
//
//
//Example 1:
//
//Input: s = "12"
//Output : 2
//Explanation : "12" could be decoded as "AB" (1 2) or "L" (12).
//Example 2 :
//
//    Input : s = "226"
//    Output : 3
//    Explanation : "226" could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF" (2 2 6).
//    Example 3 :
//
//    Input : s = "0"
//    Output : 0
//    Explanation : There is no character that is mapped to a number starting with 0.
//    The only valid mappings with 0 are 'J' -> "10" and 'T' -> "20", neither of which start with 0.
//    Hence, there are no valid ways to decode this since all digits need to be mapped.
//    Example 4 :
//
//    Input : s = "06"
//    Output : 0
//    Explanation : "06" cannot be mapped to "F" because of the leading zero("6" is different from "06").
//
//
//    Constraints :
//
//    1 <= s.length <= 100
//    s contains only digits and may contain leading zero(s).

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    unordered_map<string, char> map = { {"1", 'A'}, {"2", 'B'}, {"3", 'C'}, {"4", 'D'}, {"5", 'E'}, {"6", 'F'},
        {"7", 'G'}, {"8",'H'}, {"9", 'I'}, {"10", 'J'}, {"11", 'K'},{"12", 'L'}, {"13", 'M'},{"14",'N'},
        {"15",'O'}, {"16", 'P'},{"17",'Q'},{"18",'R'},{"19",'S'},{"20",'T'},{"21", 'U'},{"22", 'V'},
        {"23", 'W'},{"24",'X'},{"25",'Y'},{"26",'Z'}
    };

    int numDecodings_recursion(string& s, int start) {
        if (start == s.length())
            return 1;
        else if (start > s.length())
            return 0;
        else {
            //cout << "one:" << s.substr(start, 1) << ", two: " << s.substr(start, 2) << "\n" << std::flush;
            bool decodeOne = map.find(s.substr(start, 1)) != map.end();
            bool decodeTwo = map.find(s.substr(start, 2)) != map.end();
            if (decodeOne == true && decodeTwo == true)
                return numDecodings_recursion(s, start + 1) + numDecodings_recursion(s, start + 2);
            else if (decodeOne == true)
                return numDecodings_recursion(s, start + 1);
            else if (decodeTwo == true)
                return numDecodings_recursion(s, start + 2);
            else
                return 0;
        }
    }

    int numDecodings_recursion(string s) {
        return numDecodings_recursion(s,0);
    }

    int numDecodings2(string s) {
        // 0ms, 100%
        // 6.3MB, 21.72%
        int len = s.length() + 2;
        vector<int> dp(len, 0);
        dp[1] = 1;
        if (s.length() > 0 && s[0] != '0')
            dp[len - s.length()] = 1;
        else 
            dp[len - s.length()] = 0;
        
        for (int i = 3; i < len; ++i) {
            string one = s.substr(i - 2, 1), two = s.substr(i - 3, 2);
            int one_int = atoi(one.c_str()), two_int = atoi(two.c_str());
            if (one_int != 0)
                dp[i] += dp[i - 1];
            if (two_int >= 10 && two_int <= 26)
                dp[i] += dp[i - 2];
        }

        return dp[len-1];
    }

    int numDecodings(string s) {
        // O(n), 0ms, 100%
        // O(1), 6MB, 98.74%
        int len = s.length() + 2;
        int one_int, two_int, n_2, n_1, i=2;
        vector<int> dp(2, 0);
        dp[1] = 1;
        if (s.length() > 0 && s[0] != '0')
            dp[0] = 1;
        else
            dp[0] = 0;

        for (i = 3; i < len; ++i) {
            string one = s.substr(i - 2, 1), two = s.substr(i - 3, 2);
            one_int = atoi(one.c_str()), two_int = atoi(two.c_str());
            n_2 = dp[(i - 2) % 2], n_1 = dp[(i - 1) % 2];
            dp[i % 2] = 0;
            if (one_int != 0)
                dp[i%2] += n_1;
            if (two_int >= 10 && two_int <= 26)
                dp[i%2] += n_2;
        }

        return dp[(i-1)%2];
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s = string(argv[1]);
    int expected = atoi(argv[2]);

    int start_time = getMilliCount();
    int output = sol.numDecodings(s);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "s   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    if (sol.check<int>(expected, output) == true)
        return 0;
    else
        return 1;
}


