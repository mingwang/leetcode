// 149_max_points_on_a_line.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given an array of points where points[i] = [xi, yi] represents a point on the X - Y plane, return the maximum number of points that lie on the same straight line.
//
//
//
//Example 1:
//
//
//Input: points = [[1, 1], [2, 2], [3, 3]]
//Output : 3
//Example 2 :
//
//
//    Input : points = [[1, 1], [3, 2], [5, 3], [4, 1], [2, 3], [1, 4]]
//    Output : 4
//
//
//    Constraints :
//
//    1 <= points.length <= 300
//    points[i].length == 2
//    - 104 <= xi, yi <= 104
//    All the points are unique.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>
#include <set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return string(1, item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }

    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

   /* Euclid's Algorithm for Greatest Common Divisor
        As per Euclid's algorithm for the greatest common divisor, the GCD of two positive integers (a, b) can be calculated as:

        If a = 0, then GCD(a, b) = b as GCD(0, b) = b.
        If b = 0, then GCD(a, b) = a as GCD(a, 0) = a.
        If both a��0 and b��0, we write 'a' in quotient remainder form(a = b��q + r) where q is the quotient and r is the remainder, and a > b.
        Find the GCD(b, r) as GCD(b, r) = GCD(a, b)
        We repeat this process until we get the remainder as 0.
        Example: Find the GCD of 12 and 10 using Euclid's Algorithm.
        Solution : The GCD of 12 and 10 can be found using the below steps :
    a = 12 and b = 10
        a��0 and b��0
        In quotient remainder form we can write 12 = 10 �� 1 + 2
        Thus, GCD(10, 2) is to be found, as GCD(12, 10) = GCD(10, 2)

        Now, a = 10 and b = 2
        a��0 and b��0
        In quotient remainder form we can write 10 = 2 �� 5 + 0
        Thus, GCD(2, 0) is to be found, as GCD(10, 2) = GCD(2, 0)

        Now, a = 2 and b = 0
        a��0 and b = 0
        Thus, GCD(2, 0) = 2

        GCD(12, 10) = GCD(10, 2) = GCD(2, 0) = 2

        Thus, GCD of 12 and 10 is 2.

        Euclid's algorithm is very useful to find GCD of larger numbers, as in this we can easily break down numbers into smaller numbers to find the greatest common divisor.*/

//O(n^2xlogm): Runtime: 61 ms, faster than 58.14 % of C++ online submissions for Max Points on a Line.
//     m is the max point of x coordinate. 
//     logm is the complexity of calculating gcd
//O(n): Memory Usage : 12.2 MB, less than 73.60 % of C++ online submissions for Max Points on a Line.

    int gcd(int a, int b) {
        return b ? gcd(b, a % b) : a;
    }

    int maxPoints(vector<vector<int>>& points) {
        int n = points.size();
        if (n <= 2) {
            return n;
        }
        int ret = 0;
        for (int i = 0; i < n; i++) {
            if (ret >= n - 1 || ret > n / 2) {
                break;
            }
            unordered_map<int, int> mp;
            for (int j = i + 1; j < n; j++) {
                int dx = points[i][0] - points[j][0];
                int dy = points[i][1] - points[j][1];
                if (dx == 0) {
                    dy = 1;
                }
                else if (dy == 0) {
                    dx = 1;
                }
                else {
                    if (dy < 0) {
                        dx = -dx;
                        dy = -dy;
                    }
                    int gcdXY = gcd(abs(dx), abs(dy));
                    dx /= gcdXY, dy /= gcdXY;
                }
                mp[dy + dx * 20001]++;
            }
            int maxn = 0;
            for (auto& num : mp) {
                maxn = max(maxn, num.second + 1);
            }
            ret = max(maxn, ret);
        }
        return ret;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<vector<int>> points = sol.splitDoubleArray<int>(argv[1]);
    int expected = atoi(argv[2]);

    int start_time = getMilliCount();
    int result = sol.maxPoints(points);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "points   = '" << argv[1] << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;
    cout << "res = '" << result << "'" << endl;

    if (sol.check<int>(result, expected) == true)
        exit(0);
    else
        exit(1);
}


