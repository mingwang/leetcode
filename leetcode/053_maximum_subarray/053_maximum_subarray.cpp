// 053_maximum_subarray.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an integer array nums, find the contiguous subarray(containing at least one number) which has the largest sumand return its sum.
//
//A subarray is a contiguous part of an array.
//
//
//
//Example 1:
//
//Input: nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
//Output : 6
//Explanation : [4, -1, 2, 1] has the largest sum = 6.
//Example 2 :
//
//    Input : nums = [1]
//    Output : 1
//    Example 3 :
//
//    Input : nums = [5, 4, -1, 7, 8]
//    Output : 23
//
//
//    Constraints :
//
//    1 <= nums.length <= 3 * 104
//    - 105 <= nums[i] <= 105
//
//
//    Follow up : If you have figured out the O(n) solution, try coding another solution using the divideand conquer approach, which is more subtle

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    template<typename T> string printItem(T& item) {  }
    template<> string printItem(int& item) { return to_string(item); }
    template<> string printItem(double& item) { return to_string(item); }
    template<> string printItem(char& item) { return to_string(item); }
    template<> string printItem(string& item) { return item; }

    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(printItem(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return 1;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return 1;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return 1;
                }
            }
        }

        printf("match\n\n");
        return 0;
    }

    struct Status {
        /// <summary>
        /// for an given interval [l,r]
        /// lSum: maximus sub sum with interval starting with l
        /// rSum: maximus sub sum with interval end with r
        /// mSum: maximus sub sum in the entire interval
        /// iSum: sum of the internal
        /// </summary>
        int lSum, rSum, mSum, iSum;
    };

    Status pushUp(Status l, Status r) {
        int iSum = l.iSum + r.iSum;
        int lSum = max(l.lSum, l.iSum + r.lSum);
        int rSum = max(r.rSum, r.iSum + l.rSum);
        int mSum = max(max(l.mSum, r.mSum), l.rSum + r.lSum);
        return Status{ lSum,rSum,mSum,iSum };
    }

    Status get(vector<int>& nums, int l, int r) {
        if (l == r) {
            return Status{ nums[l],nums[l],nums[l],nums[l] };
        }
        int m = (l + r) / 2;
        Status lSub = get(nums, l, m);
        Status rSub = get(nums, m + 1, r);
        return pushUp(lSub, rSub);
    }

    int maxSubArray(vector<int>& nums) {
        // divide and conquer
        // O(n), 4ms, 91.31%
        //       13.1MB, 91.06%
        return get(nums,0,nums.size()-1).mSum;
    }

    int maxSubArray2(vector<int>& nums) {
        // O(n), 4ms, 91.31%
        // O(1), 13.1MB, 91.06%
        int cur = 0, Max = nums[0];

        for (int i = 0; i < nums.size(); i++) {
            cur = max(cur + nums[i], nums[i]);
            Max = max(cur, Max);
        }

        return Max;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> nums = sol.split<int>(argv[1], ',');
    int expected = atoi(argv[2]);

    int start_time = getMilliCount();
    int output = sol.maxSubArray(nums);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    cout << "nums   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    return sol.check<int>(output, expected);
}