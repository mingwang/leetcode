echo off

echo %1
set "status=0"

set arr[0]="%1" "[2,3,-2,4]" "6"
set arr[1]="%1" "[-2,0,-1]" "0"
set arr[2]="%1" "[2,3,0,-1,-7]" "7"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 