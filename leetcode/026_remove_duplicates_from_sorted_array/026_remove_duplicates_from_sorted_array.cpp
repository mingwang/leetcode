// 026_remove_duplicates_from_sorted_array.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an integer array nums sorted in non - decreasing order, remove the duplicates in - place such that each unique element appears only once.The relative order of the elements should be kept the same.
//
//Since it is impossible to change the length of the array in some languages, you must instead have the result be placed in the first part of the array nums.More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result.It does not matter what you leave beyond the first k elements.
//
//Return k after placing the final result in the first k slots of nums.
//
//Do not allocate extra space for another array.You must do this by modifying the input array in - place with O(1) extra memory.
//
//Custom Judge :
//
//The judge will test your solution with the following code :
//
//int[] nums = [...]; // Input array
//int[] expectedNums = [...]; // The expected answer with correct length
//
//int k = removeDuplicates(nums); // Calls your implementation
//
//assert k == expectedNums.length;
//for (int i = 0; i < k; i++) {
//    assert nums[i] == expectedNums[i];
//}
//If all assertions pass, then your solution will be accepted.
//
//
//
//Example 1:
//
//Input: nums = [1, 1, 2]
//Output : 2, nums = [1, 2, _]
//Explanation : Your function should return k = 2, with the first two elements of nums being 1 and 2 respectively.
//It does not matter what you leave beyond the returned k(hence they are underscores).
//Example 2 :
//
//    Input : nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]
//    Output : 5, nums = [0, 1, 2, 3, 4, _, _, _, _, _]
//    Explanation : Your function should return k = 5, with the first five elements of nums being 0, 1, 2, 3, and 4 respectively.
//    It does not matter what you leave beyond the returned k(hence they are underscores).
//
//
//    Constraints :
//
//    0 <= nums.length <= 3 * 104
//    - 100 <= nums[i] <= 100
//    nums is sorted in non - decreasing order.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    int removeDuplicates(vector<int>& nums) {
        int pos = 0, i = 1, n=nums.size();
        
        if (n == 0 || n == 1)
            return n;

        while (i < n) {
            if (nums[i] != nums[pos]) {
                nums[++pos] = nums[i];
            }
            i++;
        }

        return pos+1;
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> v = sol.split(argv[1], ',');
    int         k = atoi(argv[2]);
    vector<int> vo = sol.split(argv[3], ',');


    int start_time = getMilliCount();
    int output = sol.removeDuplicates(v);
    //ListNode* output = sol.reverseList(l);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string out_str = sol.print(v);
    string expected = sol.print(vo);

    cout << "input = '" << argv[1] << "'" << endl;
    cout << "k = '" << argv[2] << "'" << endl;
    cout << "output k = " << output << endl;
    cout << "output v = " << out_str << endl;
    cout << "expected = " << expected << endl;

    if (output != k) {
        printf ("mismatch vector length output=%d, k = %d\n", output, k);
        return 1;
    }

    for (int i = 0; i < k; i++) {
        if (v[i] == vo[i]) {

        }
        else {
            printf("\033[91m mismatch\n\n\033[0m");
            return 1;
        }
    }

    printf(" match\n\n");
    return 0;



}

