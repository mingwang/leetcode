// 087_scramble_string.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//We can scramble a string s to get a string t using the following algorithm :
//
//If the length of the string is 1, stop.
//If the length of the string is > 1, do the following :
//Split the string into two non - empty substrings at a random index, i.e., if the string is s, divide it to xand y where s = x + y.
//Randomly decide to swap the two substrings or to keep them in the same order.i.e., after this step, s may become s = x + y or s = y + x.
//Apply step 1 recursively on each of the two substrings x and y.
//Given two strings s1 and s2 of the same length, return true if s2 is a scrambled string of s1, otherwise, return false.
//
//
//
//Example 1:
//
//Input: s1 = "great", s2 = "rgeat"
//Output : true
//Explanation : One possible scenario applied on s1 is :
//"great" -- > "gr/eat" // divide at random index.
//"gr/eat" -- > "gr/eat" // random decision is not to swap the two substrings and keep them in order.
//"gr/eat" -- > "g/r / e/at" // apply the same algorithm recursively on both substrings. divide at ranom index each of them.
//"g/r / e/at" -- > "r/g / e/at" // random decision was to swap the first substring and to keep the second substring in the same order.
//"r/g / e/at" -- > "r/g / e/ a/t" // again apply the algorithm recursively, divide "at" to "a/t".
//"r/g / e/ a/t" -- > "r/g / e/ a/t" // random decision is to keep both substrings in the same order.
//The algorithm stops now and the result string is "rgeat" which is s2.
//As there is one possible scenario that led s1 to be scrambled to s2, we return true.
//Example 2:
//
//Input: s1 = "abcde", s2 = "caebd"
//Output : false
//Example 3 :
//
//    Input : s1 = "a", s2 = "a"
//    Output : true
//
//
//    Constraints :
//
//    s1.length == s2.length
//    1 <= s1.length <= 30
//    s1 and s2 consist of lower - case English letters.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    int memo[30][30][31]; // -1: false, 1: true, 0: init
    string s1, s2;

    bool quickCheck(int i1, int i2, int length) {
        unordered_map<int, int> tally;
        for (int i = i1; i < i1 + length; ++i) {
            ++tally[s1[i]];
        }
        for (int i = i2; i < i2 + length; ++i) {
            --tally[s2[i]];
        }
        for (auto &i : tally) {
            if (i.second != 0) {
                return false;
            }
        }
        return true;
    }

    bool dfs(int i1, int i2, int length) {
        if (memo[i1][i2][length] != 0)
            return memo[i1][i2][length] == 1;
        if (s1.substr(i1, length) == s2.substr(i2, length)) {
            memo[i1][i2][length] = 1;
            return true;
        }
        if (!quickCheck(i1, i2, length)) {
            memo[i1][i2][length] = -1;
            return false;
        }

        for (int i = 1; i < length; ++i) {
            // no swap
            if (dfs(i1, i2, i) && dfs(i1 + i, i2 + i, length - i)) {
                memo[i1][i2][length] = 1;
                return true;
            }

            // swap
            if (dfs(i1, i2 + length - i, i) && dfs(i1 + i, i2, length - i)) {
                memo[i1][i2][length] = 1;
                return true;
            }
        }

        memo[i1][i2][length] = -1;
        return false;
    }

    bool isScramble(string s1, string s2) {
        // O(n^4): 28ms, 69.74%
        // 17.5MB, 67.99%

        memset(memo, 0, sizeof(memo));
        this->s1 = s1;
        this->s2 = s2;
        return dfs(0,0,s1.size());
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s1 = string(argv[1]);
    string s2 = string(argv[2]);
    bool expected = (atoi(argv[3])==1);

    int start_time = getMilliCount();
    bool output = sol.isScramble(s1, s2);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "s1   = '" << argv[1] << "'" << endl;
    cout << "s2   = '" << argv[2] << "'" << endl;
    cout << "output = '" << output << "'" << endl;
    cout << "expected = '" << argv[3] << "'" << endl;

    if (sol.check<bool>(expected, output) == true)
        return 0;
    else
        return 1;
}














