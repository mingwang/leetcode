echo off

echo %1
set status = 0

set arr[0]="%1" "[1,null,2,3]"			"[1,3,2]"					
set arr[1]="%1" "[]"					"[]"			
set arr[2]="%1" "[1]"					"[1]"	
set arr[3]="%1" "[1,2]"					"[2,1]"
set arr[4]="%1" "[1,null,2]"			"[1,2]"	

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 