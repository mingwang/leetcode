echo off

echo %1
set status = 0

set arr[0]="%1" "[100,4,200,1,3,2]" "4"
set arr[1]="%1" "[0,3,7,2,5,8,4,6,0,1]" "9"
set arr[2]="%1" "[0,2,4,8]" "1"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 