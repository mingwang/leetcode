// 023_merge_k_sorted_lists.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//You are given an array of k linked - lists lists, each linked - list is sorted in ascending order.
//
//Merge all the linked - lists into one sorted linked - list and return it.
//
//
//
//Example 1:
//
//Input: lists = [[1, 4, 5], [1, 3, 4], [2, 6]]
//Output : [1, 1, 2, 3, 4, 4, 5, 6]
//Explanation : The linked - lists are :
//[
//    1->4->5,
//    1->3->4,
//    2->6
//]
//merging them into one sorted list :
//1->1->2->3->4->4->5->6
//Example 2 :
//
//    Input : lists = []
//    Output : []
//    Example 3 :
//
//    Input : lists = [[]]
//    Output : []
//
//
//    Constraints :
//
//    k == lists.length
//    0 <= k <= 10 ^ 4
//    0 <= lists[i].length <= 500
//    - 10 ^ 4 <= lists[i][j] <= 10 ^ 4
//    lists[i] is sorted in ascending order.
//    The sum of lists[i].length won't exceed 10^4.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out += '[';
        for (int i = 0; i < in.size(); i++) {
            out += in[i];
            if (i != in.size() - 1)
                out += ',';
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode* head;
        if (!l2) return l1;
        if (!l1) return l2;
        if (l1->val > l2->val) {
            head = l2;
            l2 = l2->next;
        }
        else {
            head = l1;
            l1 = l1->next;
        }

        ListNode* cur = head;
        ListNode* tmp;

        while (l1 && l2) {
            if (l1->val > l2->val) {
                cur->next = l2;
                l2 = l2->next;
            }
            else {
                cur->next = l1;
                l1 = l1->next;
            }
            cur = cur->next;
        }
        if (l1) cur->next = l1;
        if (l2) cur->next = l2;

        return head;
    }

    ListNode* mergeKLists2(vector<ListNode*>& lists) {
        // 540ms
        ListNode* head = nullptr;
        bool finish = true;
        int min = INT_MAX, min_index = 0;
        for (int i = 0; i < lists.size(); i++) {
            if (lists[i])
                finish = false;
            if (lists[i] && lists[i]->val < min) {
                min_index = i;
                min = lists[i]->val;
            }
        }
        if (!finish) {
            head = lists[min_index];
            lists[min_index] = lists[min_index]->next;
        }

        ListNode* cur = head;

        while (finish == false) {
            finish = true;
            min = INT_MAX, min_index = 0;

            for (int i = 0; i < lists.size(); i++) {
                if (lists[i]) 
                    finish = false;
                else {
                    lists.erase(lists.begin() + i);
                    i--;
                    continue;
                }
                if (lists[i] && lists[i]->val < min) {
                    min_index = i;
                    min = lists[i]->val;
                }
            }
            if (!finish) {
                cur->next = lists[min_index];
                lists[min_index] = lists[min_index]->next;
                cur = cur->next;
            }
            if (lists.size() == 1) 
                break;
            
        }
        
        return head;
    }

    ListNode* mergeKLists(vector<ListNode*>& lists) {
        // 24ms
        vector<int> v_r;
        for (int i = 0; i < lists.size(); i++) {
            ListNode* temp = lists[i];
            while ( temp != nullptr) {
                v_r.push_back(temp->val);
                temp = temp->next;
            }
        }
        sort(v_r.begin(), v_r.end());
        ListNode* head = (v_r.size() > 0) ? new ListNode(v_r[0]) : nullptr;
        ListNode* curr = head;
        for (int i = 1; i < v_r.size(); i++) {
            curr->next = new ListNode(v_r[i]);
            curr = curr->next;
        }
        return head;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<vector<int>> v = sol.splitDoubleArray(argv[1], ',');
    vector<int> vo = sol.split(argv[2], ',');
    vector<ListNode*> v_l = sol.createList(v);
    ListNode* expect = sol.createList(vo);

    int start_time = getMilliCount();
    ListNode* output = sol.mergeKLists(v_l);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string out_str = sol.printList(output);
    string expected = sol.printList(expect);

    cout << "input = '" << argv[1] << "'" << endl;
    cout << "output = " << out_str << endl;
    cout << "expected = " << expected << endl;
                     
    if (out_str.compare(argv[2]) == 0) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }



}