echo off

echo %1
set status = 0

set arr[0]="%1" "[[1,4,5],[1,3,4],[2,6]]"	"[1,1,2,3,4,4,5,6]"
set arr[1]="%1" "[]"		"[]"
set arr[2]="%1" "[[]]"		"[]"	
set arr[3]="%1" "[[1,4,5,7,8,9],[1],[2,6]]"	"[1,1,2,4,5,6,7,8,9]"

set "x=0"
:loop
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")