// 001_two_sum_vs.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {

        vector<int> result;
        unordered_map<int, int> map;

        for (int i = 0; i < nums.size(); i++) {
            cout << "i:" << i << endl;
            auto search = map.find(nums[i]);
            if (search != map.end()) {
                result.push_back(search->second);
                result.push_back(i);
            }
            else {
                if (target - nums[i] > 0)
                    map[target - nums[i]] = i;
            }
        }

        return result;
    }
};


int main() {

    Solution sol;
    vector<int> input = { 3,2,4 };
    int target = 7;

    vector<int> v = sol.twoSum(input, target);

    printf("input[%d] + input[%d]\n", v[0], v[1]);
    printf("= %d + %d\n", input[v[0]], input[v[1]]);
    printf("= %d\n", target);

    return 0;

}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
