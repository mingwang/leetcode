echo off

echo %1
set "status=0"

set arr[0]="%1" "[1,2,3,1]" "2"
set arr[1]="%1" "[1,2,1,3,5,6,4]" "5"
set arr[2]="%1" "[1,2,3,4]" "3"
set arr[3]="%1" "[4,3,2,1]" "0"
set arr[3]="%1" "[1,2,3,1]" "2"


set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 