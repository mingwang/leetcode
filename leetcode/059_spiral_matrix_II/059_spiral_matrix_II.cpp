// 059_spiral_matrix_II.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a positive integer n, generate an n x n matrix filled with elements from 1 to n2 in spiral order.
//
//
//
//Example 1:
//
//
//Input: n = 3
//Output : [[1, 2, 3], [8, 9, 4], [7, 6, 5]]
//Example 2 :
//
//    Input : n = 1
//    Output : [[1]]
//
//
//    Constraints :
//
//    1 <= n <= 20

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    template<typename T> string printItem(T& item) {  }
    template<> string printItem(int& item) { return to_string(item); }
    template<> string printItem(double& item) { return to_string(item); }
    template<> string printItem(char& item) { return to_string(item); }
    template<> string printItem(string& item) { return item; }

    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(printItem(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    vector<vector<int>> generateMatrix(int n) {
        // 0ms, 100%
        // 6.6MB, 17.81%
        vector<vector<int>> matrix(n, vector<int>(n, 0));
        int maxNum = n * n;
        int curNum = 1;
        vector<vector<int>> dir = { {0,1},{1,0},{0,-1},{-1,0} };
        int dir_ind = 0;
        int row = 0, col = 0;
        while (curNum <= maxNum) {
            matrix[row][col] = curNum;
            curNum++;
            int nextRow = row + dir[dir_ind][0], nextCol = col + dir[dir_ind][1];
            if (nextRow < 0 || nextRow >= n || nextCol < 0 || nextCol >= n || matrix[nextRow][nextCol] != 0) {
                dir_ind = (dir_ind + 1) % 4;
            }
            row += dir[dir_ind][0];
            col += dir[dir_ind][1];
        }

        return matrix;
    }

    vector<vector<int>> generateMatrix2(int n) {
        // 3ms, 6.93%
        // 6.6MB, 17.81%
        vector<vector<int>> matrix(n, vector<int>(n, 0));
        
        int c = n * n;
        int min_x = 0, max_x = n - 1, min_y = 1, max_y = n - 1;
        bool inc_x = 1, dec_x = 0, inc_y = 0, dec_y = 0;

        for (int i = 1, x = 0, y = 0; i <= c; ++i) {
            if (inc_x && x == max_x) { inc_x = 0; inc_y = 1; --max_x; }
            if (inc_y && y == max_y) { inc_y = 0; dec_x = 1; --max_y; }
            if (dec_x && x == min_x) { dec_x = 0; dec_y = 1; ++min_x; }
            if (dec_y && y == min_y) { dec_y = 0; inc_x = 1; ++min_y; }

            matrix[y][x] = i;

            if (inc_x) ++x;
            if (dec_x) --x;
            if (inc_y) ++y;
            if (dec_y) --y;
        }


        return matrix;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    int n = atoi(argv[1]);
    vector<vector<int>> expected = sol.splitDoubleArray<int>(argv[2], ',');

    int start_time = getMilliCount();
    vector<vector<int>> output = sol.generateMatrix(n);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "s   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << sol.printDoubleArray<int>(output) << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    if (sol.checkDoubleArray<int>(output, expected) == true)
        return 0;
    else
        return 1;
}
