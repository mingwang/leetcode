// 084_largest_rectangle_in_histogram.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given an array of integers heights representing the histogram's bar height where the width of each bar is 1, return the area of the largest rectangle in the histogram.
//
//
//
//Example 1:
//
//
//Input: heights = [2, 1, 5, 6, 2, 3]
//Output : 10
//Explanation : The above is a histogram where width of each bar is 1.
//The largest rectangle is shown in the red area, which has an area = 10 units.
//Example 2 :
//
//
//    Input : heights = [2, 4]
//    Output : 4

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }

    int largestRectangleArea(vector<int>& heights) {
        // O(n): 100ms, 96.42%
        // O(n): 64.9MB, 47.68 %
        int n = heights.size();
        if (n == 1) {
            return heights[0];
        }
        int max_area = 0;
        vector<int> stk;
        for (int i = 0; i < n; ++i) {
            while (!stk.empty() && heights[stk.back()] > heights[i]) {
                int height = heights[stk.back()];
                stk.pop_back();
                int width = (stk.empty() == true) ? i : i - stk.back() - 1;
                max_area = max(max_area, width * height);
            }
            stk.push_back(i);
        }
        while (stk.empty() == false) {
            int height = heights[stk.back()];
            stk.pop_back();
            int width = (stk.empty() == true) ? n : n - stk.back() - 1;
            max_area = max(max_area, width * height);
        }
        return max_area;
    }

    int largestRectangleArea3(vector<int>& heights) {
        // 177ms, 24.95%
        // 63.1MB, 71.94%
        int n = heights.size();
        if (n == 1) {
            return heights[0];
        }
        int max_area = 0;
        stack<int> stk;
        for (int i = 0; i < n; ++i) {
            while (!stk.empty() && heights[stk.top()] > heights[i]) {
                int height = heights[stk.top()];
                stk.pop();
                int width = (stk.empty() == true) ? i : i - stk.top() - 1;
                max_area = max(max_area, width * height);
            }
            stk.push(i);
        }
        while (stk.empty() == false) {
            int height = heights[stk.top()];
            stk.pop();
            int width = (stk.empty() == true) ? n : n - stk.top() - 1;
            max_area = max(max_area, width * height);
        }
        return max_area;
    }


    int largestRectangleArea2(vector<int>& heights) {
        // overtime;

        int max_area = 0;

        for (int i = 0; i < heights.size(); ++i) {
            int l = i - 1, r = i + 1;
            while (l >= 0 && heights[l] >= heights[i]) { l--; }
            while (r < heights.size() && heights[r] >= heights[i]) { r++; }

            ++l;
            --r;

            int area = (r - l + 1) * heights[i];
            max_area = max(max_area, area);
            //cout << "i: " << i << ", l:" << l << ", r:" << r << ", area: "<< area << "\n" << std::flush;
        }

        return max_area;
    }


};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> heights = sol.split<int>(argv[1]);
    int expected = atoi(argv[2]);

    int start_time = getMilliCount();
    int output = sol.largestRectangleArea(heights);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "heights   = '" << argv[1] << "'" << endl;
    cout << "output = '" << output << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    if (sol.check<int>(expected, output) == true)
        return 0;
    else
        return 1;
}











