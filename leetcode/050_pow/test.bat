echo off

echo %1
set status = 0

set arr[0]="%1" "2.0"		"10"			"1024.0"		
set arr[1]="%1" "2.1"		"3"				"9.261"
set arr[2]="%1" "2"			"-1"			"0.5"
set arr[3]="%1" "2"			"-2"			"0.25"
set arr[4]="%1" "1"			"2147483647"	"1"
set arr[5]="%1" "2"			"-2147483648"	"0.0"
set arr[6]="%1" "0.44894"	"-5"			"54.83508"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")