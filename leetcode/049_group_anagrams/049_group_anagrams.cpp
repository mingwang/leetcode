// 049_group_anagrams.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an array of strings strs, group the anagrams together.You can return the answer in any order.
//
//An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
//
//
//
//Example 1:
//
//Input: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
//Output : [["bat"], ["nat", "tan"], ["ate", "eat", "tea"]]
//Example 2 :
//
//    Input : strs = [""]
//    Output : [[""]]
//    Example 3 :
//
//    Input : strs = ["a"]
//    Output : [["a"]]
//
//
//    Constraints :
//
//    1 <= strs.length <= 104
//    0 <= strs[i].length <= 100
//    strs[i] consists of lower - case English letters.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    template<typename T> string printItem(T& item) {  }
    template<> string printItem(int& item) { return to_string(item); }
    template<> string printItem(double& item) { return to_string(item); }
    template<> string printItem(char& item) { return to_string(item); }
    template<> string printItem(string& item) { return item; }

    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(printItem(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    template  <class T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return 1;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return 1;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return 1;
                }
            }
        }

        printf("match\n\n");
        return 0;
    }

    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        // 32ms. 79.89% - O(nklogk), n = strs.size(), k = max(strs[i].length())
        //                O(klogk) for sorting string.
        // 19.6MB. 69.69%, - O(nk)
        vector<vector<string>> res;
        unordered_map<string, vector<string>> classify;

        for (int i = strs.size() - 1 ; i >= 0; --i) {
            string key = strs[i];
            sort(key.begin(), key.end());
            classify[key].push_back(strs[i]);
        }

        for (auto& it : classify) {
            sort(it.second.begin(), it.second.end());
            res.push_back(it.second);
        }

        if (!res.size()) {
            vector<string> ele;
            res.push_back(ele);
        }

        return res;
    }

    vector<vector<string>> groupAnagrams2(vector<string>& strs) {
        // runtime too long
        vector<vector<string>> res;

        while (strs.size()) {
            string s = strs.back();
            strs.pop_back();
            vector<string> anagramsOfs;
            anagramsOfs.push_back(s);
            int e = 0;
            unordered_map<char,int> ref;
            for (int i = 0; i < s.size(); i++) {
                if (ref.find(s[i]) == ref.end()) {
                    ref[s[i]] = 1;
                }
                else
                    ref[s[i]]++;
            }

            while (e < strs.size()) {
                bool match = s.length()==strs[e].length();
                unordered_map<char, int> cur;
                for (int i = 0; i < s.length() && match; i++) {
                    if (cur.find(strs[e][i]) == cur.end())
                        cur[strs[e][i]] = 1;
                    else
                        cur[strs[e][i]]++;

                    if (strs[e].find(s[i]) == string::npos || cur[strs[e][i]] > ref[strs[e][i]])
                        match = false;

                }
                if (match) {
                    anagramsOfs.push_back(strs[e]);
                    strs.erase(strs.begin()+e);
                }
                else {
                    ++e;
                }  
            }
            res.push_back(anagramsOfs);
        }

        if (!res.size()) {
            vector<string> ele;
            res.push_back(ele);
        }

        return res;
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<string> strs = sol.split<string>(argv[1], ',');
    vector<vector<string>> expected = sol.splitDoubleArray<string>(argv[2], ',');

    int start_time = getMilliCount();
    vector<vector<string>> output = sol.groupAnagrams(strs);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string out_str = sol.printDoubleArray<string>(output);

    cout << "matrix = '" << argv[1] << "'" << endl;
    cout << "output   = '" << out_str << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    return sol.checkDoubleArray<string>(output, expected);
}

