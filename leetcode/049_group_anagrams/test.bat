echo off

echo %1
set status = 0

set arr[0]="%1" "[eat,tea,tan,ate,nat,bat]"		"[[bat],[nat,tan],[ate,eat,tea]]"		
set arr[1]="%1" "[]"							"[[]]"
set arr[2]="%1" "[a]"							"[[a]]"
set arr[3]="%1" "[ddddddddddg,dgggggggggg]"		"[[dgggggggggg],[ddddddddddg]]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")