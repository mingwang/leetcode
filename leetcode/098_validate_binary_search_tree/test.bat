echo off

echo %1
set status = 0

set arr[0]="%1" "[2,1,3]" "1" 			
set arr[1]="%1" "[5,1,4,null,null,3,6]" "0"
set arr[2]="%1" "[5,4,6,null,null,3,7]" "0"
set arr[3]="%1" "[-2147483648,null,2147483647]"		"1"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 