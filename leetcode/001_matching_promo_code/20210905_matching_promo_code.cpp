// 001_matching_promo_code.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }

    vector<string> splitCode(string s, char c = ' ') {
        vector<string> out;
        int i = 0;
        while (i < s.length()) {
            int new_i = s.find(c, i);
            if (new_i == string::npos)
                new_i = s.length();
            out.push_back(s.substr(i, new_i - i));
            i = new_i + 1;
        }
        return out;
    }

    void printCode(vector<string>& code, int ind) {
        cout << "codes i = " << ind << "\n";
        for (int i = 0; i < code.size(); ++i) {
            cout << "'" << code[i] << "'";
            if (i < code.size() - 1)
                cout << ",";
        }
        cout << "\n";
    }

    int foo(vector<string> codeList, vector<string> shoppingCart) {
        int cart_size = shoppingCart.size();
        int cart_index = 0;
        int codelist_index = 0;

        for (int i = 0; i < codeList.size(); ++i) {
            vector<string> code = splitCode(codeList[i]);
            //printCode(code, i);
            bool code_match = false;
            int cnt = code.size();


            cout << "cart_index = " << cart_index << " cnt = " << cnt << " cart size = " << shoppingCart.size() << "\n" << std::flush;

            //if (cnt>shoppingCart.size())
            //    return 0;

            for (int j = cart_index; j <= cart_size - cnt; ++j) {
                //cout << "cart_index = " << cart_index << " cnt = " << cnt << " cart size = " << shoppingCart.size() << "\n" << std::flush;
                bool curr = true;
                for (int k = 0; k < cnt; ++k) {
                    if (code[k].compare("anything") != 0 && code[k].compare(shoppingCart[j + k]) != 0) {
                        //cout << "k=" << k << " " << code[k] << " j=" << j << " " << shoppingCart[j + k]
                        //    << " cond1: " << code[k].compare("anything")
                        //    << " cond2: " << code[k].compare(shoppingCart[j + k]) << "\n" << std::flush;
                        curr = false;
                        break;
                    }
                }
                if (curr == true) {
                    cart_index = j + cnt;
                    code_match = true;
                    break;
                }
            }
            if (!code_match) {
                return 0;
            }
        }

        return 1;

    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<string> codeList = sol.split<string>(argv[1], ',');
    vector<string> shoppingCart = sol.split<string>(argv[2], ',');
    int expected = atoi(argv[3]);

    int start_time = getMilliCount();
    int output = sol.foo(codeList, shoppingCart);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "codeList   = '" << argv[1] << "'" << endl;
    cout << "shoppingCart   = '" << argv[2] << "'" << endl;
    cout << "output = '" << output << "'" << endl;
    cout << "expected = '" << argv[3] << "'" << endl;

    if (sol.check<int>(output, expected) == true)
        return 0;
    else
        return 1;
}
