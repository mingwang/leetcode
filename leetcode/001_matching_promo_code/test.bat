echo off

echo %1
set status = 0

set arr[0]="%1" "[apple apple,banana anything banana]" "[orange,apple,apple,banana,orange,banana]"		"1"					
set arr[1]="%1" "[apple apple,banana anything banana]" "[banana,orange,banana,apple,apple]"		"0"	
set arr[2]="%1" "[apple apple,banana anything banana]" "[apple,banana,apple,banana,orange,banana]"		"0"	
set arr[3]="%1" "[apple apple,banana anything banana]" "[apple,apple,apple,banana]"		"0"	
set arr[4]="%1" "[apple apple,banana anything banana]" "[apple]"		"0"	

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 