echo off

echo %1
set status = 0

set arr[0]="%1" "[10,5,15,3,7,null,18]" "7" "15" "32" 			
set arr[1]="%1" "[10,5,15,3,7,13,18,1,null,6]" "6" "10" "23"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 