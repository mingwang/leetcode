// 093_restore_ip_address.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given a string s containing only digits, return all possible valid IP addresses that can be obtained from s.You can return them in any order.
//
//A valid IP address consists of exactly four integers, each integer is between 0 and 255, separated by single dotsand cannot have leading zeros.For example, "0.1.2.201" and "192.168.1.1" are valid IP addresses and "0.011.255.245", "192.168.1.312" and "192.168@1.1" are invalid IP addresses.
//
//
//
//Example 1:
//
//Input: s = "25525511135"
//Output : ["255.255.11.135", "255.255.111.35"]
//Example 2 :
//
//    Input : s = "0000"
//    Output : ["0.0.0.0"]
//    Example 3 :
//
//    Input : s = "1111"
//    Output : ["1.1.1.1"]
//    Example 4 :
//
//    Input : s = "010010"
//    Output : ["0.10.0.10", "0.100.1.0"]
//    Example 5 :
//
//    Input : s = "101023"
//    Output : ["1.0.10.23", "1.0.102.3", "10.1.0.23", "10.10.2.3", "101.0.2.3"]
//
//
//    Constraints :
//
//    0 <= s.length <= 3000
//    s consists of digits only.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    bool validateInteger(string s) { // no leading zero and between 0 and 255;
        int len = s.length();
        int val = atoi(s.c_str());

        if (val < 10 && len > 1)
            return false;
        else if (val < 100 && len > 2)
            return false;
        else if (len > 3 || val > 255)
            return false;
        else
            return true;
    }

    void findIP(vector<string>& res, vector<int>& curr, string& s, int start, int index) {
        if (index == 0) {
            string tmp;
            for (int i = 0; i < 4; i++) {
                tmp += to_string(curr[i]);
                tmp += '.';
            }
            tmp.pop_back();
            res.push_back(tmp);
        }
        else {
            for (int step = 1; step < 4; ++step) {
                int new_len = s.length() - start - step;
                int new_index = index - 1;
                if (new_len >= new_index && new_len <= new_index * 3) {
                    string tmp = s.substr(start, step);
                    if (validateInteger(tmp) == true) {
                        cout << "start: " << start << ",step:" << step <<", new_len:"<< new_len<< ", index:" << index << ",tmp:" << tmp << "\n" << std::flush;
                        curr.push_back(atoi(tmp.c_str()));
                        findIP(res, curr, s, start + step, new_index);
                        curr.pop_back();
                    }
                }
            }
        }

    }

    vector<string> restoreIpAddresses(string s) {
        // 4ms, 61.84%
        // 6.6MB, 68.34%
        vector<string> res;
        vector<int> curr;

        findIP(res, curr, s, 0, 4);

        return res;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s = string(argv[1]);
    vector<string> expected = sol.split<string>(argv[2]);

    int start_time = getMilliCount();
    vector<string> output = sol.restoreIpAddresses(s);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "s   = '" << argv[1] << "'" << endl;  
    cout << "output   = '" << sol.print<string>(output) << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    if (sol.checkArray<string>(expected, output) == true)
        return 0;
    else
        return 1;
}



