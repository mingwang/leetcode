echo off

echo %1
set status = 0

set arr[0]="%1" "25525511135"			"[255.255.11.135,255.255.111.35]"					
set arr[1]="%1" "0000"					"[0.0.0.0]"			
set arr[2]="%1" "1111"					"[1.1.1.1]"	
set arr[3]="%1" "010010"				"[0.10.0.10,0.100.1.0]"
set arr[4]="%1" "101023"				"[1.0.10.23,1.0.102.3,10.1.0.23,10.10.2.3,101.0.2.3]"	

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 