// 015_3sum.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an integer array nums, return all the triplets[nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.
//
//Notice that the solution set must not contain duplicate triplets.
//Example 1:
//
//Input: nums = [-1, 0, 1, 2, -1, -4]
//Output : [[-1, -1, 2], [-1, 0, 1]]
//Example 2 :
//
//    Input : nums = []
//    Output : []
//    Example 3 :
//
//    Input : nums = [0]
//    Output : []
//
//
//    Constraints :
//
//    0 <= nums.length <= 3000
//    - 105 <= nums[i] <= 105

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    void splitDoubleArray(string input, char c, vector< vector<int> >& out) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector<int> out_ele;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }


    vector<vector<int> > threeSum(vector<int>& nums) {
        vector<vector <int> > result;

        if (nums.size()<3) {
            return result;
        }

        sort(nums.begin(), nums.end());

        for (int i = 0; i < nums.size(); ) {
            int si = i + 1;
            int ti = nums.size() - 1;

            int first = nums[i];

            while (si < ti) {
                int second = nums[si];
                int third = nums[ti];

                if (first + second + third == 0) {
                    vector<int> entry(3, 0);
                    entry[0] = first;
                    entry[1] = second;
                    entry[2] = third;
                    result.push_back(entry);

                    while (si<nums.size() && second == nums[si]) si++;
                    while (ti>0 && third == nums[ti]) ti--;

                }
                else if (first + second + third > 0) {
                    ti--;
                }
                else {
                    si++;
                }

            }

             while (i<nums.size() && first == nums[i]) i++;
        }

        return result;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    string expected(argv[2]);

    vector<int> in;
    Solution sol;

    sol.split(input, ',', in);

    for (int i = 0; i < in.size(); i++) {
        printf("input = %d\n", in[i]);
    }

    vector<vector<int> > expect_array;
    sol.splitDoubleArray(expected, ',', expect_array);
    string expect_str = sol.printDoubleArray(expect_array);

    vector<vector<int> > output = sol.threeSum(in);
    string out_str = sol.printDoubleArray(output);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << out_str << endl;
    cout << "expected = " << expect_str << endl;

    if (out_str.compare(expect_str) == 0) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf(" mismatch\n\n");
        return 1;
    }
}