echo off

echo %1
set status = 0

set arr[0]=%1 "[2,3,1,6,7]" "4"
set arr[1]=%1 "[1,1,1,1,1]" "10"
set arr[2]=%1 "[1,3,5,7,9]" "3"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 