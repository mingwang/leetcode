echo off

echo %1
set "status=0"

set arr[0]="%1" "leetcode" "[leet,code]" "1"
set arr[1]="%1" "applepenapple" "[apple,pen]" "1"
set arr[2]="%1" "catsandog" "[cats,dog,sand,and,cat]" "0"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 