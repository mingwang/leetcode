echo off

echo %1
set status = 0

set arr[0]="%1" "hit" "cog" "[hot,dot,dog,lot,log,cog]" "[[hit,hot,dot,dog,cog],[hit,hot,lot,log,cog]]"
set arr[1]="%1" "hit" "cog" "[hot,dot,dog,lot,log]" "[]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 