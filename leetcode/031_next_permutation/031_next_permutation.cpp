// 031_next_permutation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.
// find the smallest number that are great than the current number.
// 
//If such an arrangement is not possible, it must rearrange it as the lowest possible order(i.e., sorted in ascending order).
//
//The replacement must be in placeand use only constant extra memory.
//
//
//
//Example 1:
//
//Input: nums = [1, 2, 3]
//Output : [1, 3, 2]
//Example 2 :
//
//    Input : nums = [3, 2, 1]
//    Output : [1, 2, 3]
//    Example 3 :
//
//    Input : nums = [1, 1, 5]
//    Output : [1, 5, 1]
//    Example 4 :
//
//    Input : nums = [1]
//    Output : [1]
//
//
//    Constraints :
//
//    1 <= nums.length <= 100
//    0 <= nums[i] <= 100

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    vector<string> splitStr(string input, char c) {
        vector<string> out;
        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
        return out;
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    void nextPermutation(vector<int>& nums) {
        // 4ms - 79.58%, 12.1MB - 34.83%
        int p= nums.size() - 2, i=nums.size(), diff=INT_MAX;
        if (p < 0)
            return;

        while (p >= 0 && nums[p] >= nums[p+1]) {
            p--;
        }


        if (p >=0) {
            for (int j = p + 1; j < nums.size(); j++) {
                int d = nums[j] - nums[p];
                if (d > 0 && d <= diff) {
                    diff = d;
                    i = j;
                }
            }
            if (diff != INT_MAX) {
                int temp = nums[p];
                nums[p] = nums[i];
                nums[i] = temp;
                p++;
            }
        }

        if (p < 0)
            p++;

        reverse(nums.begin()+p, nums.end());

        
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> input = sol.split(argv[1], ',');
    vector<int> expected = sol.split(argv[2], ',');

    int start_time = getMilliCount();
    sol.nextPermutation(input);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string output_str = sol.print(input);

    cout << "input = '" << argv[1] << "'" << endl;
    cout << "output = '" << output_str << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    return sol.checkIntArray(input, expected);

}