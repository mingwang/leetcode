// 006_zigzag_conversion.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)
//
//P   A   H   N
//A P L S I I G
//Y   I   R
//And then read line by line : "PAHNAPLSIIGYIR"
//Write the code that will take a string and make this conversion given a number of rows :
//
//string convert(string text, int nRows);
//convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".

#include <stdio.h>
#include <string>
#include <iostream>
#include <stdbool.h>

using namespace std;

class Solution {
public:
    string convert(string s, int numRows) {
        string* rows = new string[numRows];
        int len = s.size();
        int row = 0;
        bool increase_row = true;

        for (int i = 0; i < len; i++) {
            //printf("i=%d,row=%d\n", i, row);
            rows[row].push_back(s[i]);

            if (numRows - 1 == 0)
                continue;
            else if (row == numRows - 1)
                increase_row = false;
            else if (row == 0)
                increase_row = true;

            if (increase_row == true)
                row++;
            else
                row--;
        }

        string result;

        for (int i = 0; i < numRows; i++)
            result.append(rows[i]);

        return result;
    }

    string convert2(string s, int numRows) {
        // 8ms
        string result;
        int step = (numRows==1)?1:2 * (numRows - 1);
        for (int i = 0; i < numRows; i++) {
            for (int j = i; j < s.length(); j += step) {
                result.push_back(s[j]);
                if (i > 0 && i < numRows - 1 && (j + step - i - i) < s.length()) {
                    result.push_back(s[j + step - i - i]);
                }
            }
        }

        return result;
    }
};


int main() {

    //string input("abcdefghijklmnopq");
    //string result("agmbfhlnceikoqdjp");
    //int numRows = 4;
    string input("ab");
    string result("ab");
    int numRows = 1;

    Solution sol;

    string output = sol.convert2(input, numRows);

    printf("output = %s\n", output.c_str());
    printf("result = %s\n", result.c_str());

    if (result.compare(output) != 0) {
        cout << "mismatch\n";
    }
    else {
        cout << "match\n";
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
