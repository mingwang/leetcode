// 1509_min_difference.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//you are given an integer array nums.
//
//In one move, you can choose one element of numsand change it to any value.
//
//Return the minimum difference between the largestand smallest value of nums after performing at most three moves.
//
//
//
//Example 1:
//
//Input: nums = [5, 3, 2, 4]
//Output : 0
//Explanation : We can make at most 3 moves.
//In the first move, change 2 to 3. nums becomes[5, 3, 3, 4].
//In the second move, change 4 to 3. nums becomes[5, 3, 3, 3].
//In the third move, change 5 to 3. nums becomes[3, 3, 3, 3].
//After performing 3 moves, the difference between the minimumand maximum is 3 - 3 = 0.
//Example 2 :
//
//    Input : nums = [1, 5, 0, 10, 14]
//    Output : 1
//    Explanation : We can make at most 3 moves.
//    In the first move, change 5 to 0. nums becomes[1, 0, 0, 10, 14].
//    In the second move, change 10 to 0. nums becomes[1, 0, 0, 0, 14].
//    In the third move, change 14 to 1. nums becomes[1, 0, 0, 0, 1].
//    After performing 3 moves, the difference between the minimumand maximum is 1 - 0 = 1.
//    It can be shown that there is no way to make the difference 0 in 3 moves.
//    Example 3:
//
//Input: nums = [3, 100, 20]
//Output : 0
//Explanation : We can make at most 3 moves.
//In the first move, change 100 to 7. nums becomes[3, 7, 20].
//In the second move, change 20 to 7. nums becomes[3, 7, 7].
//In the third move, change 3 to 7. nums becomes[7, 7, 7].
//After performing 3 moves, the difference between the minimumand maximum is 7 - 7 = 0.
//
//
//Constraints :
//
//    1 <= nums.length <= 105
//    - 109 <= nums[i] <= 109

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>
#include <set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

#define PASS 0
#define FAIL 1

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return string(1, item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }

    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return false;
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

#define GET_STATE(state) \
    ((state == RISING) ?  "rise" : \
     (state == FALLING) ? "fall" :\
    (state == RESET) ?    "rest" : "")

#define SOL 1

#if SOL == 1
    // O(nlogC) : 144ms. 5.96%
    // O(1) : 80.15MB. 5.15%

    void print_multiset(string str, multiset<int>& set) {
        printf("%s : ", str.c_str());
        for (auto& i : set) {
            printf("%d ", i);
        }
        printf("\n");
    }

    int minDifference(vector<int>& nums) {
        multiset<int> can_min;
        multiset<int> can_max;

        int n = nums.size();
        const int max_move = 3;
        const int max_depth = max_move + 1;

        for (int i = 0; i < n; i++) {
            can_min.insert(nums[i]);
            can_max.insert(nums[i]);

            if (can_min.size() > max_depth) {
                //can_min.erase(can_min.find(*can_min.rbegin())); // use find to not remove duplicates
                //can_max.erase(can_max.find(*can_max.begin()));
                auto last_min = can_min.end();
                can_min.erase(--last_min);
                auto first_max = can_max.begin();
                can_max.erase(first_max);
            }
        }

        print_multiset("min set", can_min);
        print_multiset("max set", can_max);

        int i = 0;
        auto it_min = can_min.begin();
        auto it_max = can_max.begin();
        int res = INT_MAX;

        while (i < max_depth && it_min != can_min.end()) {
            res = min(res, *it_max - *it_min);
            //printf("i=%d, min=%d, max=%d\n", i, *it_min, *it_max);
            ++it_min;
            ++it_max;
            i++;
        }

        return res;
    }
#endif

#if SOL == 2
    int minDifference(vector<int>& nums) {

        return 0;
    }

#endif

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> list = sol.split<int>(argv[1]);
    int expected = (atoi(argv[2]));

    int start_time = getMilliCount();
    int result = sol.minDifference(list);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "list   = '" << argv[1] << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;
    cout << "res = '" << result << "'" << endl;

    if (sol.check<int>(result, expected) == true) {
        exit(PASS);
    }
    else {
        exit(FAIL);
    }
}