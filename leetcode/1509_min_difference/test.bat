echo off

echo %1
set status = 0

set arr[0]=%1 "[5,3,2,4]" "0"
set arr[1]=%1 "[1,5,0,10,14]" "1"
set arr[2]=%1 "[3,100,20]" "0"
set arr[3]=%1 "[6,6,0,1,1,4,6]" "2"
set arr[4]=%1 "[866,985,750,911,793,408,370,896,979,629,693,664,518,210,654,995,496,280,279,68,610,922,677,46,14,516,922,281,583,740]" "785"


set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 