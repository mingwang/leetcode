echo off

echo %1
set status = 0

set arr[0]="%1" "2"			"3"		"6" 
set arr[1]="%1" "123"		"456"	"56088"		
set arr[2]="%1" "0"			"3"		"0" 
set arr[3]="%1" "2"			"0"		"0" 
set arr[4]="%1" "9"			"99"	"891" 

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")