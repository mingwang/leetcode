// 043_multiply_strings.cpp : This file contains the 'main' function. Program execution begins and ends there.
//Given two non - negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.
//
//Note: You must not use any built - in BigInteger library or convert the inputs to integer directly.
//
//
//
//Example 1 :
//
//    Input : num1 = "2", num2 = "3"
//    Output : "6"
//    Example 2 :
//
//    Input : num1 = "123", num2 = "456"
//    Output : "56088"
//
//
//    Constraints :
//
//    1 <= num1.length, num2.length <= 200
//    num1 and num2 consist of digits only.
//    Both num1 and num2 do not contain any leading zero, except the number 0 itself.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    vector<string> splitStr(string input, char c) {
        vector<string> out;
        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
        return out;
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    vector<vector<char>> splitDoubleCharArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<char> > out;
        vector<char> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(s[0]);
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(s[0]);
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleCharArray(vector<vector<char> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out += in[i][j];
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append("\n,");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    template  <class T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return 1;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return 1;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return 1;
                }
            }
        }

        printf("match\n\n");
        return 0;
    }

    string reverse_add(string num1, string num2) {
        if (num1.size() < num2.size())
            return reverse_add(num2, num1);

        if (num1 == "0") {
            return num2;
        }
        else if (num2 == "0") {
            return num1;
        }

        string ans;
        int c = 0;
        for (int i = 0; i < num1.size(); i++) {
            int a = num1[i] - '0', b = (i < num2.size()) ? num2[i] - '0' : 0;
            int d = a + b + c;
            c = d / 10;
            d %= 10;
            ans += to_string(d);
        }
        if (c)
            ans += to_string(c);

        return ans;
    }

    string multiply2(string num1, string num2) {
        // 128ms - 9.64%
        // 13MB - 15.93%
        string ans("0");

        if (num1 == "0" || num2 == "0")
            return ans;

        int l1 = num1.size(), l2 = num2.size();
        for (int i = 0; i < l2; i++) {
            string tmp(l2 - 1 - i, '0');
            int c = 0;
            for (int j = l1-1; j >=0; j--) {
                int d =(num2[i]-'0')*(num1[j]-'0') + c;
                c = d / 10;
                d %= 10;
                
                tmp += to_string(d);
            }
            if (c)
                tmp += to_string(c);

            ans = reverse_add(ans, tmp);
        }
        
        string ans1;
        for (int i = ans.size() - 1; i >= 0; i--) {
            ans1 += ans[i];
        }
        
        return ans1;

    }

    string multiply(string nums1, string nums2) {

        // 4ms,  89.72%
        // 6.6MB, 64.73%

        string res(nums1.length() + nums2.length(), '0');

        reverse(nums1.begin(), nums1.end());
        reverse(nums2.begin(), nums2.end());

        for (int i = 0; i < nums1.size(); ++i) {
            int carry = 0;
            for (int j = 0; j < nums2.size(); ++j) {
                int total = (nums1[i] - '0') * (nums2[j] - '0') + carry + (res[i + j] - '0');
                carry = total / 10;
                res[i + j] = total % 10 + '0';
            }
            if (carry) res[i + nums2.size()] = carry + '0';
        }

        reverse(res.begin(), res.end());

        auto pos = res.find_first_not_of("0");
        if (pos == string::npos)return "0";
        return res.substr(pos);
    }

};

int main(int argc, char* argv[]) { 

    //int input = 2147483647;
    Solution sol;
    string num1(argv[1]);
    string num2(argv[2]);
    string expected(argv[3]);

    int start_time = getMilliCount();
    string output = sol.multiply(num1, num2);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    cout << "num1 = '" << num1 << "'" << endl;
    cout << "num2 = '" << num2 << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << expected << "'" << endl;

    return sol.check<string>(output, expected);
}
