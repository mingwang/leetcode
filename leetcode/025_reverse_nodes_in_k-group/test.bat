echo off

echo %1
set status = 0

set arr[0]="%1" "[1,2,3,4,5]"		"3"		"[3,2,1,4,5]"
set arr[1]="%1" "[1,2,3,4,5,6,7]"	"3"		"[3,2,1,6,5,4,7]"
set arr[2]="%1" "[1,2,3,4,5,6,7]"	"2"		"[2,1,4,3,6,5,7]"
set arr[3]="%1" "[1,2,3,4,5]"		"1"		"[1,2,3,4,5]"
set arr[4]="%1" "[1]"				"1"		"[1]"	


set "x=0"
:loop
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")