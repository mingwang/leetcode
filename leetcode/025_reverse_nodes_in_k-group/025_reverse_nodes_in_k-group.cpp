// 025_reverse_nodes_in_k-group.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a linked list, reverse the nodes of a linked list k at a timeand return its modified list.
//
//k is a positive integer and is less than or equal to the length of the linked list.If the number of nodes is not a multiple of k then left - out nodes, in the end, should remain as it is.
//
//You may not alter the values in the list's nodes, only nodes themselves may be changed.
//
//
//
//Example 1:
//
//
//Input: head = [1, 2, 3, 4, 5], k = 2
//Output : [2, 1, 4, 3, 5]
//Example 2 :
//
//
//    Input : head = [1, 2, 3, 4, 5], k = 3
//    Output : [3, 2, 1, 4, 5]
//    Example 3 :
//
//    Input : head = [1, 2, 3, 4, 5], k = 1
//    Output : [1, 2, 3, 4, 5]
//    Example 4 :
//
//    Input : head = [1], k = 1
//    Output : [1]
//
//
//    Constraints :
//
//    The number of nodes in the list is in the range sz.
//    1 <= sz <= 5000
//    0 <= Node.val <= 1000
//    1 <= k <= sz

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out += '[';
        for (int i = 0; i < in.size(); i++) {
            out += in[i];
            if (i != in.size() - 1)
                out += ',';
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    ListNode* swapPairs2(ListNode* head) {
        // 3 ms
        if (head == NULL) {
            return NULL;
        }
        else if (head->next == NULL) {
            return head;
        }
        else {
            ListNode* newHead = head->next;
            head->next = newHead->next;
            newHead->next = head;
            head->next = swapPairs(head->next);
            return newHead;
        }
    }

    ListNode* swapPairs(ListNode* head) {
        // 0 ms
        ListNode* cur = head;
        ListNode* nxt = (head) ? head->next : nullptr;

        if (nxt) {
            head = nxt;
            cur->next = nxt->next;
            nxt->next = cur;
            nxt = cur->next;
        }

        while (nxt && nxt->next) {
            cur->next = nxt->next;
            nxt->next = cur->next->next;
            cur->next->next = nxt;
            cur = nxt;
            nxt = cur->next;
        }

        return head;
    }

    ListNode* reverseList(ListNode* head) {

        ListNode* cur = head;
        ListNode* prev = nullptr, * next = nullptr;

        while (cur) {
            next = cur->next;
            cur->next = prev;
            prev = cur;
            cur = next;
        }

        return prev;
    }

    pair<ListNode*, ListNode*> reverseListRange(ListNode* head, ListNode* tail) {
        ListNode* prev = tail->next;
        ListNode* cur = head;
        ListNode* nxt = nullptr;
        while (prev != tail) {
            nxt = cur->next;
            cur->next = prev;
            prev = cur;
            cur = nxt;
        }
        return { tail,head };
    }

    ListNode* reverseKGroup(ListNode* head, int k) {
        // 12ms
        ListNode* imagine = new ListNode(0, head);
        ListNode* prev = imagine;
        ListNode* tail = nullptr, *nxt=nullptr;
        while (head) {
            tail = prev;
            for (int i = 0; i < k; ++i) {
                tail = tail->next;
                if (!tail)
                    return imagine->next;
            }

            nxt = tail->next;
            tie(head, tail) = reverseListRange(head, tail);
            prev->next = head;
            tail->next = nxt;
            prev = tail;
            head = tail->next;
        }
        return imagine->next;
    }

    ListNode* reverseKGroup2(ListNode* head, int k) {
        // 12ms
        ListNode* cur;
        ListNode* prev = nullptr, * next = nullptr;
        ListNode* newHead = head;
        ListNode* next_k_node = head, *last, *tail=nullptr, *prev_tail;
        bool updateHead = true;

        if (k == 1)
            return head;
        
        while (1) {
            int i = 0;
            last = next_k_node;
            prev_tail = tail;
            while (next_k_node && i < k) {
                next_k_node = next_k_node->next;
                i++;
            }

            if (i == k) {
                cur = last;
                tail = cur;
                prev = next_k_node;
                while (i>0) {
                    next = cur->next;
                    cur->next = prev;
                    prev = cur;
                    cur = next;
                    i--;
                }
                if (updateHead) {
                    newHead = prev;
                    updateHead = false;
                }
                else
                    prev_tail->next = prev;
            }
            else {
                return newHead;
            }
        }
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> v = sol.split(argv[1], ',');
    int         k = atoi(argv[2]);
    vector<int> vo = sol.split(argv[3], ',');
    ListNode* l = sol.createList(v);
    ListNode* expect = sol.createList(vo);

    int start_time = getMilliCount();
    ListNode* output = sol.reverseKGroup(l,k);
    //ListNode* output = sol.reverseList(l);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string out_str = sol.printList(output);
    string expected = sol.printList(expect);

    cout << "input = '" << argv[1] << "'" << endl;
    cout << "k = '" << argv[2] << "'" << endl;
    cout << "output = " << out_str << endl;
    cout << "expected = " << expected << endl;

    if (out_str.compare(argv[3]) == 0) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }



}