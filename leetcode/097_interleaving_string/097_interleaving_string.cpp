// 097_interleaving_string.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given strings s1, s2, and s3, find whether s3 is formed by an interleaving of s1and s2.
//
//An interleaving of two strings sand t is a configuration where they are divided into non - empty substrings such that :
//
//s = s1 + s2 + ... + sn
//t = t1 + t2 + ... + tm
//| n - m| <= 1
//The interleaving is s1 + t1 + s2 + t2 + s3 + t3 + ... or t1 + s1 + t2 + s2 + t3 + s3 + ...
//Note : a + b is the concatenation of strings a and b.
//
//
//
//Example 1 :
//
//
//    Input : s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
//    Output : true
//    Example 2 :
//
//    Input : s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc"
//    Output : false
//    Example 3 :
//
//    Input : s1 = "", s2 = "", s3 = ""
//    Output : true
//
//
//    Constraints :
//
//    0 <= s1.length, s2.length <= 100
//    0 <= s3.length <= 200
//    s1, s2, and s3 consist of lowercase English letters.
//
//
//    Follow up : Could you solve it using only O(s2.length) additional memory space ?

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode* root) { return printTree(root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        int n = nodes.size();
        //if (index == 0) index_offset = 0;
        if (index < n && nodes[index].compare("null") != 0) {
            int val = atoi(nodes[index].c_str());
            int left_index = 2 * index + 1 - index_offset;
            int right_index = left_index + 1;
            //cout << "node: " << val << ", ind: " << index << ", l_ind:" << left_index << ", r_ind:" << right_index << "\n" << std::flush;
            left = createTree(nodes, left_index, index_offset);
            if (left == nullptr)
                index_offset = (index_offset + 1) << 1;
            right = createTree(nodes, right_index, index_offset);
            root = new TreeNode(val, left, right);
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                if (root->right != nullptr || root->left != nullptr) {
                    leaf_nodes.push(root->left);
                    leaf_nodes.push(root->right);
                }
            }
            else {
                s += "null,";
            }
        }
        cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        s_ind = s.length() - 1;
        if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
            s.erase(s_ind);

        s += "]";

        return s;

    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    bool isInterleave2(string s1, string s2, string s3) {
        // 4ms, 74.53%
        // 6.6MB, 69.34%

        int s1_len = s1.length(), s2_len = s2.length(), s3_len = s3.length();
        if (s1_len + s2_len != s3_len)
            return false;
        
        vector<vector<bool>> dp(s1_len + 1, vector<bool>(s2_len + 1, false));
        // f(i,j) = f(i-1,j)(s1[i-1]==s3[i+j-1]) + f(i,j-1)(s2[j-1]==s3[i+j+1])
        dp[0][0] = true;
        for (int i = 1; i <= s1_len; i++) {
            dp[i][0] = dp[i - 1][0] && (s1[i - 1] == s3[i - 1]);
        }
        for (int j = 1; j <= s2_len; j++) {
            dp[0][j] = dp[0][j - 1] && (s2[j - 1] == s3[j - 1]);
        }
        for (int i = 1; i <= s1_len; i++) {
            for (int j = 1; j <= s2_len; j++) {
                dp[i][j] = (dp[i - 1][j] && s1[i - 1] == s3[i + j - 1]) ||
                    (dp[i][j - 1] && s2[j - 1] == s3[i + j - 1]);
            }
        }
        return dp[s1_len][s2_len];
    }

    bool isInterleave(string s1, string s2, string s3) {
        // 4ms, 74.53%
        // 6.3MB, 86.79%

        int s1_len = s1.length(), s2_len = s2.length(), s3_len = s3.length();
        if (s1_len + s2_len != s3_len)
            return false;

        vector<bool> dp(s2_len + 1, false);
        // f(i,j) = f(i-1,j)(s1[i-1]==s3[i+j-1]) + f(i,j-1)(s2[j-1]==s3[i+j+1])
        dp[0] = true;

        for (int j = 1; j <= s2_len; j++) {
            dp[j] = dp[j - 1] && (s2[j - 1] == s3[j - 1]);
        }
        for (int i = 1; i <= s1_len; i++) {
            dp[0] = dp[0] && (s1[i - 1] == s3[i - 1]);
            for (int j = 1; j <= s2_len; j++) {
                dp[j] = (dp[j] && s1[i - 1] == s3[i + j - 1]) ||
                    (dp[j - 1] && s2[j - 1] == s3[i + j - 1]);
            }
        }
        return dp[s2_len];
    }

    bool isInterleave(string s1, string s2, string s3) {
        // 0ms, 100%
        // 6.1MB, 94.87%

        int s1_len = s1.length(), s2_len = s2.length(), s3_len = s3.length();
        if (s1_len + s2_len != s3_len)
            return false;

        bool dp[102];
        // f(i,j) = f(i-1,j)(s1[i-1]==s3[i+j-1]) + f(i,j-1)(s2[j-1]==s3[i+j+1])
        dp[0] = true;

        for (int j = 1; j <= s2_len; j++) {
            dp[j] = dp[j - 1] && (s2[j - 1] == s3[j - 1]);
        }
        for (int i = 1; i <= s1_len; i++) {
            dp[0] = dp[0] && (s1[i - 1] == s3[i - 1]);
            for (int j = 1; j <= s2_len; j++) {
                dp[j] = (dp[j] && s1[i - 1] == s3[i + j - 1]) ||
                    (dp[j - 1] && s2[j - 1] == s3[i + j - 1]);
            }
        }
        return dp[s2_len];
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s1 = string(argv[1]);
    string s2 = string(argv[2]);
    string s3 = string(argv[3]);
    bool expected = (atoi(argv[4])==1);

    int start_time = getMilliCount();
    bool output = sol.isInterleave(s1, s2, s3);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "s1   = '" << argv[1] << "'" << endl;
    cout << "s2   = '" << argv[2] << "'" << endl;
    cout << "s3   = '" << argv[3] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << argv[4] << "'" << endl;

    if (sol.check<bool>(expected, output) == true)
        return 0;
    else
        return 1;
}

