echo off

echo %1

set status = 0

"%1" "2"      "II"
set /a status= %status% + %errorlevel%
"%1" "22"     "XXII"
set /a status= %status% + %errorlevel%
"%1" "207"    "CCVII"
set /a status= %status% + %errorlevel%
"%1" "1066"   "MLXVI"
set /a status= %status% + %errorlevel%
"%1" "3001"   "MMMI"
set /a status= %status% + %errorlevel%
"%1" "1954"   "MCMLIV"
set /a status= %status% + %errorlevel%
"%1" "1990"   "MCMXC"
set /a status= %status% + %errorlevel%
"%1" "2014"   "MMXIV"
set /a status= %status% + %errorlevel%
"%1" "4"      "IV"
set /a status= %status% + %errorlevel%
"%1" "9"      "IX"
set /a status= %status% + %errorlevel%
"%1" "40"     "XL"
set /a status= %status% + %errorlevel%
"%1" "90"     "XC"
set /a status= %status% + %errorlevel%
"%1" "400"    "CD"
set /a status= %status% + %errorlevel%
"%1" "900"    "CM"
set /a status= %status% + %errorlevel%


if %status% NEQ 0 (echo "FAIL") else (echo "PASS")
