// 012_integer_to_roman.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Roman numerals are represented by seven different symbols : I, V, X, L, C, Dand M.
//
//Symbol       Value
//I             1
//V             5
//X             10
//L             50
//C             100
//D             500
//M             1000
//For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
//
//Roman numerals are usually written largest to smallest from left to right.However, the numeral for four is not IIII.Instead, the number four is written as IV.Because the one is before the five we subtract it making four.The same principle applies to the number nine, which is written as IX.There are six instances where subtraction is used :
//
//I can be placed before V(5) and X(10) to make 4 and 9.
//X can be placed before L(50) and C(100) to make 40 and 90.
//C can be placed before D(500) and M(1000) to make 400 and 900.
//Given an integer, convert it to a roman numeral.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

#if SOL1
    string intToRoman(int num) {
        // I - 1
        // IV - 4
        // V - 5
        // IX - 9
        // X - 10
        // XL - 40
        // L - 50
        // C - 100
        // CD - 400
        // D - 500
        // CM - 900
        // M - 1000

        string roman;

        vector<pair<int, string>> map = { {1000, "M"},{900, "CM"},{500, "D"}, 
            {400, "CD"},{100, "C"}, {90, "XC"},  {50, "L"}, {40, "XL"}, {10, "X"},
            {9, "IX"},   {5, "V"}, {4, "IV"}, {1, "I"}};

        int n = num, i = 0;
        while (i != map.size() && n != 0) {
            
            int d = n / map[i].first;
            if (d) {
                for (int j = 0; j < d; j++)
                    roman += map[i].second;
                n = n % (map[i].first);
            }

            i++;
        }

        return roman;
    }
#endif

#define SOL2 1
#if SOL2
    string intToRoman(int num) {
        string romanSymbol[] = { "M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I" };
        int values[] = { 1000,900,500,400,100,90,50,40,10,9,5,4,1 };
        string ans = "";
        for (int i = 0; i < 13; i++) {
            while (num >= values[i]) {
                ans += romanSymbol[i];
                num -= values[i];
            }
        }
        return ans;
    }
#endif
};


int main(int argc, char* argv[]) {

    //int input = 2147483647;
    int input = atoi(argv[1]);
    string expected(argv[2]);


    Solution sol;

    string output = sol.intToRoman(input);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output.compare(expected) == 0) {
        printf("match\n\n");
        return 0;
    }
    else {
        printf("mismatch\n\n");
        return 1;
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
