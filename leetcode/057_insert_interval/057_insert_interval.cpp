// 057_insert_interval.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//You are given an array of non - overlapping intervals intervals where intervals[i] = [starti, endi] represent the start and the end of the ith interval and intervals is sorted in ascending order by starti.You are also given an interval newInterval = [start, end] that represents the start and end of another interval.
//
//Insert newInterval into intervals such that intervals is still sorted in ascending order by starti and intervals still does not have any overlapping intervals(merge overlapping intervals if necessary).
//
//Return intervals after the insertion.
//
//
//
//Example 1:
//
//Input: intervals = [[1, 3], [6, 9]], newInterval = [2, 5]
//Output : [[1, 5], [6, 9]]
//Example 2 :
//
//    Input : intervals = [[1, 2], [3, 5], [6, 7], [8, 10], [12, 16]], newInterval = [4, 8]
//    Output : [[1, 2], [3, 10], [12, 16]]
//    Explanation : Because the new interval[4, 8] overlaps with[3, 5], [6, 7], [8, 10].
//    Example 3 :
//
//    Input : intervals = [], newInterval = [5, 7]
//    Output : [[5, 7]]
//    Example 4 :
//
//    Input : intervals = [[1, 5]], newInterval = [2, 3]
//    Output : [[1, 5]]
//    Example 5 :
//
//    Input : intervals = [[1, 5]], newInterval = [2, 7]
//    Output : [[1, 7]]
//
//
//    Constraints :
//
//    0 <= intervals.length <= 104
//    intervals[i].length == 2
//    0 <= starti <= endi <= 105
//    intervals is sorted by starti in ascending order.
//    newInterval.length == 2
//    0 <= start <= end <= 105

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    template<typename T> string printItem(T& item) {  }
    template<> string printItem(int& item) { return to_string(item); }
    template<> string printItem(double& item) { return to_string(item); }
    template<> string printItem(char& item) { return to_string(item); }
    template<> string printItem(string& item) { return item; }

    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(printItem(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newinterval) {
        // O(n) - 8ms, 98.76%
        // 16.9MB, 99.61%

        vector<vector<int >>res;

        for (int i = 0; i < intervals.size(); i++) {

            if (intervals[i][1] < newinterval[0]) {
                res.push_back(intervals[i]);
            }

            else if (intervals[i][0] > newinterval[1]) {
                res.push_back(newinterval);
                newinterval = intervals[i];
            }
            else if (intervals[i][1] >= newinterval[0] || intervals[i][0] <= newinterval[1]) {
                newinterval[0] = min(intervals[i][0], newinterval[0]);
                newinterval[1] = max(intervals[i][1], newinterval[1]);
            }

        }
        res.push_back(newinterval);
        return res;
    }

    vector<vector<int>> insert2(vector<vector<int>>& intervals, vector<int>& newInterval) {
        // O(n) - 12ms, 91.10%
        // 17.1MB, 75.57%

        int l = 0, r = intervals.size()-1;

        while (l < r) {
            int p = (l + r) / 2;
            if (intervals[p][0] <= newInterval[0]) {
                l = p + 1;
            }
            else {
                r = p;
            }
        }

        intervals.insert(intervals.begin() + l, newInterval);

        vector<vector<int>> merged;

        sort(intervals.begin(), intervals.end());

        for (int i = 0; i < intervals.size(); i++) {
            int l = intervals[i][0], r = intervals[i][1];
            if (!merged.size() || l > merged.back()[1]) {
                merged.push_back({ l,r });
            }
            else {
                merged.back()[1] = max(r, merged.back()[1]);
            }
        }

        return merged;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<vector<int>> intervals = sol.splitDoubleArray<int>(argv[1], ',');
    vector<int> newInterval = sol.split<int>(argv[2], ',');
    vector<vector<int>> expected = sol.splitDoubleArray<int>(argv[3], ',');

    int start_time = getMilliCount();
    vector<vector<int>> output = sol.insert(intervals, newInterval);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "intervals   = '" << argv[1] << "'" << endl;
    cout << "newInterval   = '" << argv[2] << "'" << endl;
    cout << "output   = '" << sol.printDoubleArray(output) << "'" << endl;
    cout << "expected = '" << argv[3] << "'" << endl;

    if (sol.checkDoubleArray<int>(output, expected) == true)
        return 0;
    else
        return 1;
}
