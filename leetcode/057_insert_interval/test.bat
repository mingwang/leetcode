echo off

echo %1
set status = 0

set arr[0]="%1" "[[1,3],[6,9]]"								"[2,5]"				"[[1,5],[6,9]]"	
set arr[1]="%1" "[[1,2],[3,5],[6,7],[8,10],[12,16]]"		"[4,8]"				"[[1,2],[3,10],[12,16]]"	
set arr[2]="%1" "[]"										"[5,7]"				"[[5,7]]"	
set arr[3]="%1" "[[1,5]]"									"[2,3]"				"[[1,5]]"	
set arr[4]="%1" "[[1,5]]"									"[2,7]"				"[[1,7]]"	

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")