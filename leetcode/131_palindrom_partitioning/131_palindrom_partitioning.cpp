// 131_palindrom_partitioning.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a string s, partition s such that every substring of the partition is a palindrome.Return all possible palindrome partitioning of s.
//
//A palindrome string is a string that reads the same backward as forward.
//
//
//
//Example 1:
//
//Input: s = "aab"
//Output : [["a", "a", "b"], ["aa", "b"]]
//Example 2 :
//
//    Input : s = "a"
//    Output : [["a"]]
//
//
//    Constraints :
//
//    1 <= s.length <= 16
//    s contains only lowercase English letters.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>
#include <set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return string(1, item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

#define SOL 4

#if SOL == 1
    // Runtime 142ms, 70.23%
    // Memory: 53.4MB, 72.24%
    bool isPalindrome(string s) {
        if (s.length() <= 0) {
            return false;
        } 
        else if (s.length() == 1) {
            return true;
        }
        else {
            int mid = s.length() / 2;
            int l = mid - 1, r = (s.length()%2 == 0) ? mid : mid + 1;
            while (l >= 0 && r < s.length()) {
                if (s[l] != s[r])
                    return false;
                l--;
                r++;
            }
            return true;
        }
    }

    void find_partition(string s, int ind, vector<vector<string>>& res, vector<string>& cur) {
        if (ind == s.length()) {
            res.push_back(cur);
            return;
        }
        else {
            for (int step = 1; ind+step <= s.length(); step++) {
                cout << "ind = " << ind << ", step = "<<step<<endl;
                if (isPalindrome(s.substr(ind, step))) {
                    cur.push_back(s.substr(ind, step));
                    find_partition(s, ind + step, res, cur);
                    cur.pop_back();
                }
            }
        }

    }

    vector<vector<string>> partition(string s) {
        vector<vector<string>> res;
        vector<string> cur;

        find_partition(s, 0, res, cur);

        return res;
    }
#endif
#if SOL == 2
    //Runtime : 180 ms, faster than 50.42 % of C++ online submissions for Palindrome Partitioning.
    //Memory Usage : 75.8 MB, less than 37.35 % of C++ online submissions for Palindrome Partitioning.

    vector<vector<int>> dp;
    vector<vector<string>> res;
    vector<string> cur;

    void dfs(int i, string& s) {
        if (i == s.length()) {
            res.push_back(cur);
        }
        else {
            for (int j = i; j < s.length(); j++) {
                if (dp[i][j]) {
                    cur.push_back(s.substr(i, j - i + 1));
                    dfs(j + 1, s);
                    cur.pop_back();
                }
            }
        }
    }

    vector<vector<string>> partition(string s) {
        dp = vector<vector<int>>(s.length(), vector<int>(s.length(), 1));
        int l = s.length();
        cout << "l = " << l << endl;
        for (int i = l - 2; i >= 0; i--) {
            for (int j = i + 1; j < l; j++) {
                //cout << "i: "<<i<<" j: "<<j<<"\n";
                dp[i][j] = dp[i + 1][j - 1] && (s[i] == s[j]);
            }
        }

        //for (int i = 0; i < l; i++) {
        //    for (int j = 0; j < l; j++) {
        //        cout << " " << dp[i][j] << " ";
        //    }
        //    cout << "\n";
        //}

        dfs(0, s);

        return res;
    }
#endif
#if SOL == 3
    //Runtime : 119 ms, faster than 82.88 % of C++ online submissions for Palindrome Partitioning.
    //Memory Usage : 75.7 MB, less than 43.32 % of C++ online submissions for Palindrome Partitioning.

    int dp[16][16];
    vector<vector<string>> res;
    vector<string> cur;

    void dfs(int i, string& s) {
        if (i == s.length()) {
            res.push_back(cur);
        }
        else {
            for (int j = i; j < s.length(); j++) {
                if (dp[i][j]) {
                    cur.push_back(s.substr(i, j - i + 1));
                    dfs(j + 1, s);
                    cur.pop_back();
                }
            }
        }
    }

    vector<vector<string>> partition(string s) {
        memset(dp, 1, sizeof(dp));
        int l = s.length();
        cout << "l = " << l << endl;
        for (int i = l - 2; i >= 0; i--) {
            for (int j = i + 1; j < l; j++) {
                //cout << "i: "<<i<<" j: "<<j<<"\n";
                dp[i][j] = dp[i + 1][j - 1] && (s[i] == s[j]);
            }
        }

        //for (int i = 0; i < l; i++) {
        //    for (int j = 0; j < l; j++) {
        //        cout << " " << dp[i][j] << " ";
        //    }
        //    cout << "\n";
        //}

        dfs(0, s);

        return res;
    }
#endif

#if SOL == 4
    // Runtime: 80ms. 85.56%
    // Mem: 49.62MB. 88.98%

    void dfs(vector<vector<string>>& res, vector<vector<bool>>& dp, vector<string>& cur, string& s, int start_i) {
        int l = s.length();
        if (start_i >= l) {
            res.push_back(cur);
        }
        else {
            for (int i = start_i; i < l; i++) {
                if (dp[start_i][i] == true) {
                    cur.push_back(s.substr(start_i, i - start_i + 1));
                    dfs(res, dp, cur, s, i + 1);
                    cur.pop_back();
                }
            }
        }
    }

    vector<vector<string>> partition(string s) {
        int l = s.length();
        vector<vector<bool>> dp = vector<vector<bool>>(l, vector<bool>(l, true));
        vector<vector<string>> res;
        vector<string> cur;

        for (int i = l - 2; i >= 0; --i) {
            for (int j = l - 1; j > i; --j) {
                dp[i][j] = dp[i + 1][j - 1] && (s[i] == s[j]);
            }
        }

        dfs(res, dp, cur, s, 0);

        return res;
    }
#endif

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s = string(argv[1]);
    vector<vector<string>> expected = sol.splitDoubleArray<string>(argv[2]);

    int start_time = getMilliCount();
    vector<vector<string>> res = sol.partition(s);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "root   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << sol.printDoubleArray<string>(res) << "'" << endl;
    cout << "expected = '" << sol.printDoubleArray<string>(expected) << "'" << endl;

    if (sol.checkDoubleArray<string>(res, expected) == true)
        return 0;
    else
        return 1;
}



