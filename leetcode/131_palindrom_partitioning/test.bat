echo off

echo %1
set status = 0

set arr[0]="%1" "aab" "[[a,a,b],[aa,b]]"
set arr[1]="%1" "a" "[[a]]"
set arr[2]="%1" "efe" "[[e,f,e],[efe]]"
set arr[3]="%1" "abbab" "[[a,b,b,a,b],[a,b,bab],[a,bb,a,b],[abba,b]]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 