echo off

echo %1
set status = 0

set arr[0]="%1"  "[[-2,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2],[0]]" "[[-2,-1,1,2],[-2,0,0,2],[-2,0,1,1],[-1,-1,0,2],[-1,-1,1,1],[-1,0,0,1],[0,0,0,0]]"
set arr[1]="%1"  "[[1,0,-1,0,-2,2],[0]]" "[[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]"
set arr[1]="%1"  "[[1000000000, 1000000000, 1000000000, 1000000000],[0]]" ""

set "x=0"
:loop
if defined arr[%x%] (
	call echo Running test %x%: %%arr[%x%]%%
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")