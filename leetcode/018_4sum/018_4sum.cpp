// 018_4sum.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an array nums of n integers, return an array of all the unique quadruplets[nums[a], nums[b], nums[c], nums[d]] such that :
//
//0 <= a, b, c, d < n
//    a, b, c, and d are distinct.
//    nums[a] + nums[b] + nums[c] + nums[d] == target
//    You may return the answer in any order.
//
//
//
//    Example 1:
//
//Input: nums = [1, 0, -1, 0, -2, 2], target = 0
//Output : [[-2, -1, 1, 2], [-2, 0, 0, 2], [-1, 0, 0, 1]]
//Example 2 :
//
//    Input : nums = [2, 2, 2, 2, 2], target = 8
//    Output : [[2, 2, 2, 2]]
//
//
//    Constraints :
//
//    1 <= nums.length <= 200
//    - 109 <= nums[i] <= 109
//    - 109 <= target <= 109

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    void splitDoubleArray(string input, char c, vector< vector<int> >& out) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector<int> out_ele;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    vector<string> letterCombinations(string digits) {
        vector<string> result;

        unordered_map<char, string> map = { {'0', ""},{'1',""},{'2',"abc"},{'3',"def"},
            {'4',"ghi"},{'5',"jkl"},{'6',"mno"},
            {'7',"pqrs"},{'8',"tuv"},{'9',"wxyz"} };


        for (int i = 0; i < digits.size(); i++) {
            int prev_length = result.size();

            if (!prev_length) {
                for (int j = 0; j < map[digits[i]].size(); j++) {
                    string s;
                    result.push_back(s + map[digits[i]][j]);
                }
            }
            else {
                for (int k = 0; k < prev_length; k++) {
                    for (int j = 0; j < map[digits[i]].size(); j++) {
                        result.push_back(result[k] + map[digits[i]][j]);
                    }
                }
            }

            result = { result.begin() + prev_length, result.end() };
        }

        return result;
    }

    vector<vector<int> > fourSum(vector<int>& nums, int target) {
        vector<vector <int> > result;

        sort(nums.begin(), nums.end());

        int n = nums.size();
        int n_3 = n - 3;
        int n_2 = n - 2;
        int n_1 = n - 1;

        for (int i = 0; i < n_3; i++) {
            if (i > 0 && nums[i-1] == nums[i]) continue;
            if ( nums[i] + nums[n_1] < target - nums[n_2] - nums[n_3]) continue;
            if (nums[i] + nums[i + 1]  > target - nums[i + 2] - nums[i + 3]) continue;

            for (int j = i + 1; j < n - 2; j++) {
                int l = j + 1;
                int r = n_1;
                int csum = target - nums[i] - nums[j];
                if (j > i + 1 && nums[j - 1] == nums[j]) continue;
                while (l < r) {
                    int sum = nums[l] + nums[r];
                    int left = nums[l];
                    int right = nums[r];
                    if (sum>csum) r--;
                    else if (sum<csum) l++;
                    else {
                        result.push_back(vector<int>{nums[i], nums[j], nums[l], nums[r]});

                        while (l < r && nums[l] == left) l++;
                        while (l < r && nums[r] == right) r--;
                    }
                }

            }
        }


        return result;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    string expected(argv[2]);

    vector<vector<int> > in;
    Solution sol;

    sol.splitDoubleArray(input, ',', in);

    vector<vector<int> > expect_array;
    sol.splitDoubleArray(expected, ',', expect_array);
    string expect_str = sol.printDoubleArray(expect_array);

    int start_time = getMilliCount();
    vector<vector<int> > output = sol.fourSum(in[0], in[1][0]);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    string out_str = sol.printDoubleArray(output);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << out_str << endl;
    cout << "expected = " << expect_str << endl;

    if (out_str.compare(expect_str) == 0) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }



}
