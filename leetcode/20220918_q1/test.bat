echo off

echo %1
set status = 0

set arr[0]="%1" "[20,13,8,9]"			"50"			
set arr[1]="%1" "[30,15,5,9]"			"30"
set arr[2]="%1" "[2,9,10,3,7]"			"21"
set arr[3]="%1" "[10,9,10,3,7]"			"29"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 