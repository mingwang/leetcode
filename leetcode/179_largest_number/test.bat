echo off

echo %1
set "status=0"

set arr[0]="%1" "[10,2]" "210"
set arr[1]="%1" "[3,30,34,5,9]" "9534330"
set arr[2]="%1" "[0,0]" "0"
set arr[3]="%1" "[1,0,0]" "100"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 