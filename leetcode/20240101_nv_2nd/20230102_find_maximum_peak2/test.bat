echo off

echo %1
set status = 0

set arr[0]=%1 "[2]" "0"
set arr[1]=%1 "[1,2,3]" "0"
set arr[2]=%1 "[3,2,1]" "0"
set arr[3]=%1 "[1,2,3,2,3]" "4"
set arr[4]=%1 "[1,2,3,2,3,4,5,3,2,5]" "6"
set arr[5]=%1 "[3,2,1,2,3,2,3,4,5,3,2]" "6"
set arr[6]=%1 "[1,2,3,2]" "4"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 