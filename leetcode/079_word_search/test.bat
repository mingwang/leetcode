echo off

echo %1
set status = 0

set arr[0]="%1" "[[A,B,C,E],[S,F,C,S],[A,D,E,E]]"				"ABCCED" "1"
set arr[1]="%1" "[[A,B,C,E],[S,F,C,S],[A,D,E,E]]"			    "SEE"	 "1"
set arr[2]="%1" "[[A,B,C,E],[S,F,C,S],[A,D,E,E]]"			    "ABCB"	 "0"
set arr[3]="%1" "[[a,a]]"										"aaa"	 "0"
set arr[4]="%1" "[[a,a]]"										"aa"	 "1"
set arr[5]="%1" "[[A,A,A,A,A,A],[A,A,A,A,A,A],[A,A,A,A,A,A],[A,A,A,A,A,A],[A,A,A,A,A,B],[A,A,A,A,B,A]]" "AAAAAAAAAAAAABB" "0"


set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 