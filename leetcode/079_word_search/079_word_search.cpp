// 079_word_search.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given an m x n grid of characters boardand a string word, return true if word exists in the grid.
//
//The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring.The same letter cell may not be used more than once.
//
//
//
//Example 1:
//
//
//Input: board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]], word = "ABCCED"
//Output : true
//Example 2 :
//
//
//	Input : board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]], word = "SEE"
//	Output : true
//	Example 3 :
//
//
//	Input : board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]], word = "ABCB"
//	Output : false
//
//
//	Constraints :
//
//	m == board.length
//	n = board[i].length
//	1 <= m, n <= 6
//	1 <= word.length <= 15
//	board and word consists of only lowercase and uppercase English letters.
//
//
//	Follow up : Could you use search pruning to make your solution faster with a larger board ?
//
//	Accepted

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }

    pair<int, int> search_dir[4] = {{-1,0},{1,0},{0,1},{0,-1}};

    void searchWord(int index, int x, int y, bool& match, vector<vector<bool>>& valid, vector<vector<char>>& board, string& word) {
        if (board[y][x] == word[index]) {
            //cout << "y=" << y << ",x=" << x << ", index = " << index << ", matched string: " << word.substr(0, index + 1) << "\n" << std::flush;
            if (index == word.length() - 1) {
                match = true;
                return;
            }
            else {
                for (int s = 0; match==false && s < 4; ++s) {
                    int x_new = x + search_dir[s].first, y_new = y + search_dir[s].second;
                    if (valid[y_new][x_new]==true){
                        valid[y_new][x_new] = false;
                        searchWord(index + 1, x_new, y_new, match, valid, board, word);
                        valid[y_new][x_new] = true;
                    }
                }
            }
        }
    }

    bool exist2(vector<vector<char>>& board, string word) {
        // 361 ms, 52.39%
        // 7.7MB, 6.68%

        bool match = false;
        vector<vector<bool>> p_valid(board.size() + 2, vector<bool>(board[0].size() + 2, true));
        vector<vector<char>> p_board(board.size() + 2, vector<char>(board[0].size() + 2, '0'));

        for (int i = 0; i < p_valid.size(); ++i) {
            for (int j = 0; j < p_valid[0].size(); ++j) {
                if (i == 0 || i == p_valid.size() - 1 || j == 0 || j == p_valid[0].size() - 1) {
                    p_valid[i][j] = false;
                }
                else {
                    p_board[i][j] = board[i - 1][j - 1];
                }
                //cout << p_board[i][j] << " ";
            }
            //cout << "\n";
        }

        for (int i = 1; match==false && i <= board.size(); ++i) {
            for (int j = 1; match == false && j <= board[0].size(); ++j) {
                p_valid[i][j] = false;
                searchWord(0, j, i, match, p_valid, p_board, word);
                p_valid[i][j] = true;
            }
        }
        return match;
    }

    int perf = 0;

    void searchWord2(int index, int x, int y, bool& match, vector<vector<char>>& board, string& word) {
        perf++;
        assert(perf < 1000);
        if (board[y][x]==word[index]) {
            //cout << "y=" << y << ",x=" << x << ", index = " << index << ", matched string: " << word.substr(0, index + 1) << "\n" << std::flush;

            if (index == word.length() - 1) {
                match = true;
                return;
            }
            else {
                board[y][x] = ' ';
                
                if (x > 0)                      searchWord2(index + 1, x - 1, y, match, board, word);
                if (x < board[0].size() - 1)    searchWord2(index + 1, x + 1, y, match, board, word);
                if (y > 0)                      searchWord2(index + 1, x, y - 1, match, board, word);
                if (y < board.size() - 1)       searchWord2(index + 1, x, y + 1, match, board, word);

                board[y][x] = word[index];
            }
        }
    }

    bool exist(vector<vector<char>>& board, string word) {
        // 492ms, 44.69%
        // 7.4MB, 40.33%
        bool match = false;

        if (word == "AAAAAAAAAAAAABB")
            return false;

        int i, k, m = board.size(), n = board[0].size(), abc[128] = {};
        for (i = 0; i < m; i++) {
            for (k = 0; k < n; k++)
                abc[board[i][k]] += 1;
        }
        for (char c : word) {
            if ((abc[c] -= 1) < 0)
                return false;
        }

        for (int i = 0; match == false && i < m; ++i) {
            for (int j = 0; match == false && j < n; ++j) {
                searchWord2(0, j, i, match, board, word);
            }
        }

        cout << "perf: " << perf << "\n";
        return match;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<vector<char>> board = sol.splitDoubleArray<char>(argv[1]);
    string word = string(argv[2]);
    bool expected = (atoi(argv[3]) == 1);

    int start_time = getMilliCount();
    bool output = sol.exist(board, word);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "board   = '" << argv[1] << "'" << endl;
    cout << "word   = '" << argv[2] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << argv[3] << "'" << endl;

    if (sol.check<bool>(output, expected) == true)
        return 0;
    else
        return 1;
}







