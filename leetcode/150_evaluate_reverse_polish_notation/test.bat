echo off

echo %1
set "status=0"

set arr[0]="%1" "[2,1,+,3,*]" "9"
set arr[1]="%1" "[4,13,5,/,+]" "6"
set arr[2]="%1" "[10,6,9,3,+,-11,*,/,*,17,+,5,+]" "22"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 