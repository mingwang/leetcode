// 115_distinct_sequences.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given two strings sand t, return the number of distinct subsequences of s which equals t.
//
//A string's subsequence is a new string formed from the original string by deleting some (can be none) of the characters without disturbing the remaining characters' relative positions. (i.e., "ACE" is a subsequence of "ABCDE" while "AEC" is not).
//
//It is guaranteed the answer fits on a 32 - bit signed integer.
//
//
//
//Example 1:
//
//Input: s = "rabbbit", t = "rabbit"
//Output : 3
//Explanation :
//    As shown below, there are 3 ways you can generate "rabbit" from S.
//    rabbbit
//    rabbbit
//    rabbbit
//    Example 2 :
//
//    Input : s = "babgbag", t = "bag"
//    Output : 5
//    Explanation :
//    As shown below, there are 5 ways you can generate "bag" from S.
//    babgbag
//    babgbag
//    babgbag
//    babgbag
//    babgbag
//
//
//    Constraints :
//
//1 <= s.length, t.length <= 1000
//s and t consist of English letters.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

    int numDistinct2(string s, string t) {
        int dp[1001];
        memset(dp, 0, sizeof(dp));

        int s_len = s.length(), t_len = t.length();
        int m = 0, res  = 0;

        while (dp[0] < s_len) {
            while (m < t_len && dp[m] < s_len) {
                //cout << "m:"<<m<<", dp[m]="<<dp[m]<<"\n"<<std::flush;
                if (s[dp[m]] == t[m]) {
                    m++;
                    dp[m] = dp[m - 1] + 1;
                }
                else {
                    dp[m]++;
                }

                if (m == t_len) {
                    ++res;
                    --m;
                    ++dp[m];
                }
            }
            int i = m;
            while (i >= 0 && dp[i] > s_len) { --i; }

            if (i >= 0) {
                m = i;
                dp[m]++;
            }
        }

        return res;
    }

    int numDistinct(string s, string t) {
        // 36ms, 34.45%
        // 30.2MB, 11.18%
        int m = s.length(), n = t.length();
        if (m < n)
            return 0;

        vector<vector<unsigned long long>> dp(m + 1, vector<unsigned long long>(n + 1, 0));
        for (int i = 0; i <= m; i++) {
            dp[i][n] = 1;
        }
        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                if (s[i] == t[j]) {
                    dp[i][j] = dp[i + 1][j + 1] + dp[i + 1][j];
                }
                else {
                    dp[i][j] = dp[i + 1][j];
                }
            }
        }
        return dp[0][0];
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s = string(argv[1]);
    string t = string(argv[2]);
    int expected = atoi(argv[3]);

    int start_time = getMilliCount();
    int output = sol.numDistinct(s,t);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "s   = '" << s << "'" << endl;
    cout << "t   = '" << t << "'" << endl;
    cout << "output = '" << output << "'" << endl;
    cout << "expected = '" << expected << "'" << endl;

    if (sol.check<int>(output, expected) == true)
        return 0;
    else
        return 1;
}







