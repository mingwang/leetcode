echo off

echo %1
set "status=0"

set arr[0]="%1" "[[-2,-3,3],[-5,-10,1],[10,30,-5]]" "7"
set arr[1]="%1" "[[0]]" "1"
set arr[2]="%1" "[[0,0,0],[1,1,-1]]" "1"
set arr[3]="%1" "[[1,-3,3],[0,-2,0],[-3,-3,-3]]" "3"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	if %ERRORLEVEL% NEQ 0 (
		set /a "status+=1""
	)
	set /a "x+=1"
	echo status is %status%
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 