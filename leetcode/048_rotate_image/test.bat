echo off

echo %1
set status = 0

set arr[0]="%1" "[[1,2,3],[4,5,6],[7,8,9]]"									"[[7,4,1],[8,5,2],[9,6,3]]"		
set arr[1]="%1" "[[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]"			"[[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]"
set arr[2]="%1" "[[1]]"							"[[1]]]"
set arr[3]="%1" "[[1,2],[3,4]]"					"[[3,1],[4,2]]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")