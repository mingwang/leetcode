echo off

echo %1
set status = 0

set arr[0]="%1" "[3,2,2,3]"			"3"		"2"		"[2,2]"
set arr[1]="%1" "[0,1,2,2,4,0,3,2]"	"2"		"5"		"[0,1,4,0,3]"
set arr[2]="%1" "[0]"				"0"		"0"		"[]"
set arr[2]="%1" "[0]"				"1"		"1"		"[0]"


set "x=0"
:loop
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")