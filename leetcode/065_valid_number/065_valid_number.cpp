﻿// 065_valid_number.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//A valid number can be split up into these components(in order) :
//
//    A decimal number or an integer.
//    (Optional)An 'e' or 'E', followed by an integer.
//    A decimal number can be split up into these components(in order) :
//
//    (Optional)A sign character(either '+' or '-').
//    One of the following formats :
//One or more digits, followed by a dot '.'.
//One or more digits, followed by a dot '.', followed by one or more digits.
//A dot '.', followed by one or more digits.
//An integer can be split up into these components(in order) :
//
//    (Optional)A sign character(either '+' or '-').
//    One or more digits.
//    For example, all the following are valid numbers : ["2", "0089", "-0.1", "+3.14", "4.", "-.9", "2e10", "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789"] , while the following are not valid numbers : ["abc", "1a", "1e", "e3", "99e2.5", "--6", "-+3", "95a54e53"] .
//
//    Given a string s, return true if s is a valid number.
//
//
//
//    Example 1:
//
//Input: s = "0"
//Output : true
//Example 2 :
//
//    Input : s = "e"
//    Output : false
//    Example 3 :
//
//    Input : s = "."
//    Output : false
//    Example 4 :
//
//    Input : s = ".1"
//    Output : true
//
//
//    Constraints :
//
//    1 <= s.length <= 20
//    s consists of only English letters(both uppercase and lowercase), digits(0 - 9), plus '+', minus '-', or dot '.'.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    template<typename T> string printItem(T& item) {  }
    template<> string printItem(int& item) { return to_string(item); }
    template<> string printItem(double& item) { return to_string(item); }
    template<> string printItem(char& item) { return to_string(item); }
    template<> string printItem(string& item) { return item; }

    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(printItem(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }

    int findNotNumber(string& s, int si, bool& sign, bool& exp, bool& find_exp, bool& exp_num) {
        int l = s.length();
        if (sign && l>0 && (s[si] == '+' || s[si] == '-')) {
            ++si;
        }
        sign = false;
        int i=si;
        while (i < l && (s[i] >= '0' && s[i] <= '9'))
            ++i;

        if (i > si) {
            sign = false;
            if (!find_exp) {
                exp = true;
                find_exp = true;
            }
            if (exp_num) {
                exp_num = false;
            }
        }

        return i;
    }

    bool isNumber2(string s) {
        // 15ms, 5.20%
        // 5.9MB, 73.62%
        bool optSign = true, decDot=true, exp=false, find_exp=false,exp_num=false;
        int l = s.length(), i=0;
        while (i < l) {
            int n = findNotNumber(s, i, optSign, exp, find_exp, exp_num);

            if (n >= l) {
                return !exp_num;
            } else if (decDot && s[n] == '.') {
                decDot = false;
                if (n == 0 || s[n-1]<'0' || s[n-1]>'9')
                    exp_num = true;
            }
            else if (exp && (s[n] == 'e' || s[n] == 'E')) {
                exp = false;
                optSign = true;
                decDot = false;
                exp_num = true;
            }
            else  {
                return false;
            }
            i = n + 1;
        }
        return !exp_num;
    }

    enum State {
        STATE_INITIAL,
        STATE_INT_SIGN,
        STATE_INTEGER,
        STATE_POINT,
        STATE_POINT_WITHOUT_INT,
        STATE_FRACTION,
        STATE_EXP,
        STATE_EXP_SIGN,
        STATE_EXP_NUMBER,
        STATE_END
    };

    enum CharType {
        CHAR_NUMBER,
        CHAR_EXP,
        CHAR_POINT,
        CHAR_SIGN,
        CHAR_ILLEGAL
    };

    CharType toCharType(char ch) {
        if (ch >= '0' && ch <= '9')
            return CHAR_NUMBER;
        else if (ch == 'e' || ch == 'E')
            return CHAR_EXP;
        else if (ch == '.')
            return CHAR_POINT;
        else if (ch == '+' || ch == '-')
            return CHAR_SIGN;
        else
            return CHAR_ILLEGAL;
    }

    bool isNumber(string s) {
        // 84ms, 5.12%
        // 13.6MB, 5.12%
        unordered_map<State, unordered_map<CharType, State>> next_state{
            {STATE_INITIAL, {
                {CHAR_NUMBER, STATE_INTEGER},
                {CHAR_POINT, STATE_POINT_WITHOUT_INT},
                {CHAR_SIGN, STATE_INT_SIGN}}},
            {STATE_INT_SIGN, {
                {CHAR_NUMBER, STATE_INTEGER},
                {CHAR_POINT, STATE_POINT_WITHOUT_INT}}},
            {STATE_INTEGER, {
                {CHAR_NUMBER, STATE_INTEGER},
                {CHAR_EXP, STATE_EXP},
                {CHAR_POINT, STATE_POINT}}},
            {STATE_POINT, {
                {CHAR_NUMBER, STATE_FRACTION},
                {CHAR_EXP, STATE_EXP}}},
            {STATE_POINT_WITHOUT_INT, {
                {CHAR_NUMBER, STATE_FRACTION}}},
            {STATE_FRACTION, {
                {CHAR_NUMBER, STATE_FRACTION},
                {CHAR_EXP, STATE_EXP}}},
            {STATE_EXP, {
                {CHAR_NUMBER, STATE_EXP_NUMBER},
                {CHAR_SIGN, STATE_EXP_SIGN}}},
            {STATE_EXP_SIGN, {
                {CHAR_NUMBER, STATE_EXP_NUMBER}}},
            {STATE_EXP_NUMBER, {
                {CHAR_NUMBER, STATE_EXP_NUMBER}}}
        };

        int len = s.length();
        State curr = STATE_INITIAL;

        for (int i = 0; i < len; i++) {
            CharType c = toCharType(s[i]);
            if (next_state[curr].find(c) == next_state[curr].end()) {
                return false;
            }
            else {
                curr = next_state[curr][c];
            }
        }

        return curr == STATE_INTEGER ||
            curr == STATE_POINT ||
            curr == STATE_FRACTION ||
            curr == STATE_EXP_NUMBER ||
            curr == STATE_END;
    }

};  

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s = string(argv[1]);
    bool expected = (atoi(argv[2])==1);

    int start_time = getMilliCount();
    bool output = sol.isNumber(s);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "s   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << output << "'" << endl;
    cout << "expected = '" << expected << "'" << endl;

    if (sol.check<bool>(output, expected) == true)
        return 0;
    else
        return 1;
}



