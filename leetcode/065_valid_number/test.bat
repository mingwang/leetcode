echo off

echo %1
set status = 0

set arr[0]="%1" "0"		"1"									
set arr[1]="%1" "e"		"0"						
set arr[2]="%1" "."		"0"	
set arr[3]="%1" ".1"	"1"	

set arr[4]="%1" "2"		"1"
set arr[5]="%1" "0089"		"1"
set arr[6]="%1" "-0.1"		"1"
set arr[7]="%1" "+3.14"		"1"
set arr[8]="%1" "4."		"1"
set arr[9]="%1" "-.9"		"1"
set arr[10]="%1" "2e10"		"1"
set arr[11]="%1" "-90E3"		"1"
set arr[12]="%1" "3e+7"		"1"
set arr[13]="%1" "+6e-1"		"1"
set arr[14]="%1" "53.5e93"		"1"
set arr[15]="%1" "-123.456e789"		"1"

set arr[16]="%1" "abc"		"0"	
set arr[17]="%1" "1a"		"0"	
set arr[18]="%1" "1e"		"0"	
set arr[19]="%1" "e3"		"0"	
set arr[20]="%1" "99e2.5"		"0"	
set arr[21]="%1" "--6"		"0"	
set arr[22]="%1" "-+3"		"0"	
set arr[23]="%1" "95a54e53"		"0"	

set arr[24]="%1" "-1."		"1"
set arr[25]="%1" "4e+"		"0"
set arr[26]="%1" ".-4"		"0"
set arr[27]="%1" "92e1740e91"		"0"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 