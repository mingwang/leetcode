echo off

echo %1
set status = 0

set arr[0]="%1" "[1,2,5,3,4,null,6]" "[1,null,2,null,3,null,4,null,5,null,6]"
set arr[1]="%1" "[]" "[]"
set arr[2]="%1" "[0]" "[0]"	


set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 