// 005_longest_palindromic_substring.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a string s, find the longest palindromic substring in s.You may assume that the maximum length of s is 1000.
//
//Example:
//
//Input: "babad"
//
//Output : "bab"
//
//Note : "aba" is also a valid answer.
//Example :
//
//    Input : "cbbd"
//
//    Output : "bb"



#include <stdio.h>
#include <String.h>
#include <iostream>

using namespace std;

class Solution {
public:
    string longestPalindrome(string s) {
        if (s.size() <= 1)
            return s;

        int left = 0, right = 0, start = 0, max_left = 0, max_len = 1;
        int len = s.size();

        while (start<len && len - start>max_len / 2) {
            left = right = start;
            while (right < len - 1 && s[right + 1] == s[right])
                ++right;
            start = right + 1;
            while (left > 0 && right < len - 1 && s[right + 1] == s[left - 1]) {
                ++right;
                left--;
            }
            if (right - left + 1 > max_len) {
                max_left = left;
                max_len = right - left + 1;
            }
        }
        return s.substr(max_left, max_len);
    }
};

int main() {

    //string input("abaaaaaxz");
    string input("aaaaaaaab");

    Solution sol;

    string substring = sol.longestPalindrome(input);

    printf("substring = %s\n", substring.c_str());


    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
