// 051_n_queen.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    template<typename T> string printItem(T& item) {  }
    template<> string printItem(int& item) { return to_string(item); }
    template<> string printItem(double& item) { return to_string(item); }
    template<> string printItem(char& item) { return to_string(item); }
    template<> string printItem(string& item) { return item; }

    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(printItem(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n\n");
            return 1;
        }

        printf("match\n\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return 1;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return 1;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return 1;
                }
            }
        }

        printf("match\n\n");
        return 0;
    }

    void dfs(int n, int index, int s_i, int s_j,
        vector<vector<bool>>& board, vector<vector<string>>& res, 
        vector<bool> rowValid, vector<bool> colValid, 
        vector<bool> backslashValid, vector<bool> forwardslashValid) {

        if (index == n) {
            vector<string> sol;
             for (int i = 0; i < n; i++) {
                string pack;
                for (int j = 0; j < n; j++) 
                    pack.push_back(board[i][j] ? 'Q' : '.');
                sol.push_back(pack);
            }
             res.push_back(sol);
             return;
        }

        for (int i = s_i; i < n; i++) {
            for (int j = s_j; j < n; j++) {
                if (rowValid[i] && colValid[j] && forwardslashValid[i + j] && backslashValid[n - i - 1 + j]) {
                    board[i][j] = true;
                    rowValid[i] = colValid[j] = forwardslashValid[i + j] = backslashValid[n - i - 1 + j] = false;
                    dfs(n, index + 1, i + 1, 0, board, res, rowValid, colValid, backslashValid, forwardslashValid);
                    board[i][j] = false;
                    rowValid[i] = colValid[j] = forwardslashValid[i + j] = backslashValid[n - i - 1 + j] = true;
                }
            }
        }


    }

    vector<vector<string>> solveNQueens2(int n) {
        vector<vector<string>> res;
        vector<vector<bool>> board(n, vector<bool>(n, false));
        vector<bool> rowValid(n, true);
        vector<bool> colValid(n, true);
        vector<bool> backslashValid(2 * n - 1, true);
        vector<bool> forwardslashValid(2 * n - 1, true);

        dfs(n, 0, 0, 0, board, res, rowValid, colValid, backslashValid, forwardslashValid);

        return res;
    }

    void backtrack(vector<vector<string>>& res, vector<int>& queens, int n, int row, 
        unordered_set<int>& columns, unordered_set<int>& diag1, unordered_set<int>& diag2) {
        if (row == n) {
            res.push_back(generateBoard(queens, n));
        }
        else {
            for (int i = 0; i < n; i++) {
                if (columns.find(i) != columns.end())
                    continue;
                int ind_diag1 = row - i;
                if (diag1.find(ind_diag1) != diag1.end())
                    continue;
                int ind_diag2 = row + i;
                if (diag2.find(ind_diag2) != diag2.end())
                    continue;
                queens[row] = i;
                columns.insert(i);
                diag1.insert(ind_diag1);
                diag2.insert(ind_diag2);
                backtrack(res, queens, n, row + 1, columns, diag1, diag2);
                queens[row] = -1;
                columns.erase(i);
                diag1.erase(ind_diag1);
                diag2.erase(ind_diag2);
            }


        }

    }

    vector<string> generateBoard(vector<int>& queens, int n) {
        vector<string> board;
        for (int i = 0; i < n; i++) {
            string row = string(n, '.');
            row[queens[i]] = 'Q';
            board.push_back(row);
        }
        return board;
    }

    vector<vector<string>> solveNQueens3(int n) {
        // 16ms, 9.3MB
        vector<vector<string>> res;
        vector<int> queens(n, -1);
        unordered_set<int> columns;
        unordered_set<int> diag1;
        unordered_set<int> diag2;

        backtrack(res, queens, n, 0, columns, diag1, diag2);

        return res;
    }

    vector<vector<string>> solveNQueens(int n) {
        // 4ms, 94.65%
        // 7.2MB, 87.50%
        vector<vector<string>>res;
        vector<string>nQueens(n, string(n, '.'));
        vector<int> flag_col(n, 1), flag_45(2 * n - 1, 1), flag_135(2 * n - 1, 1);
        dfs(res, nQueens, flag_col, flag_45, flag_135, 0, n);
        return res;
    }

    void dfs(vector<vector<string>>& res, vector<string>& nQueens, vector<int>& flag_col, vector<int>& flag_45, vector<int>& flag_135
        , int row, int n) {
        if (row == n) {
            res.push_back(nQueens);
            return;
        }

        for (int col = 0; col < n; col++) {
            if (flag_col[col] && flag_45[row + col] && flag_135[n - 1 - row + col]) {
                flag_col[col] = flag_45[row + col] = flag_135[n - 1 - row + col] = 0;
                nQueens[row][col] = 'Q';
                dfs(res, nQueens, flag_col, flag_45, flag_135, row + 1, n);
                nQueens[row][col] = '.';
                flag_col[col] = flag_45[row + col] = flag_135[n - 1 - row + col] = 1;
            }
        }

    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    int n = atoi(argv[1]);
    vector<vector<string>> expected = sol.splitDoubleArray<string>(argv[2], ',');

    int start_time = getMilliCount();
    vector<vector<string>> output = sol.solveNQueens(n);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string out_str = sol.printDoubleArray<string>(output);

    cout << "n   = '" << n << "'" << endl;
    cout << "output   = '" << out_str << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    return sol.checkDoubleArray<string>(output, expected);
}

