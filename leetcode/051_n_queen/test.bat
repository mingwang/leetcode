echo off

echo %1
set status = 0

set arr[0]="%1" "1"		"[[Q]]"	
set arr[1]="%1" "4"		"[[.Q..,...Q,Q...,..Q.],[..Q.,Q...,...Q,.Q..]]"	

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")