// 002_add_two_numbers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <stdlib.h>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode {
    int val;
    struct ListNode* next;
};

struct ListNode* createListNode(int head_value) {
    struct ListNode* head = (struct ListNode*)malloc(sizeof(struct ListNode));
    head->val = head_value;
    head->next = NULL;

    return head;
}

void appendListNode(struct ListNode* ptr, int append_value) {
    while (ptr->next != NULL) {
        ptr = ptr->next;
    }
    ptr->next = createListNode(append_value);
}

void freeListNode(struct ListNode* head) {
    while (head != NULL) {
        struct ListNode* deleteNode = head;
        head = head->next;
        free(deleteNode);
    }

}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {

    struct ListNode* result = NULL;
    int carry = 0;

    while (l1 != NULL && l2 != NULL) {
        int value = (l1->val + l2->val + carry) % 10;
        if (result == NULL) {
            result = createListNode(value);
        }
        else {
            appendListNode(result, value);
        }
        carry = (l1->val + l2->val + carry) / 10;

        if (l1->next == NULL && l2->next != NULL) {
            appendListNode(l1, 0);
        }
        if (l2->next == NULL && l1->next != NULL) {
            appendListNode(l2, 0);
        }
        l1 = l1->next;
        l2 = l2->next;
    }

    if (carry != 0) {
        appendListNode(result, carry);
    }

    return result;

}

void printListNode(struct ListNode* head, const char* comment) {

    printf("%s\n", comment);

    if (head == NULL) {
        return;
    }

    for (struct ListNode* ptr = head; ptr != NULL; ptr = ptr->next) {
        printf("%d", ptr->val);
        if (ptr->next != NULL) {
            printf(" -> ");
        }
    }
    printf("\n");
}

int main() {

    struct ListNode* l1 = createListNode(9);
    appendListNode(l1, 9);
    appendListNode(l1->next, 9);
    appendListNode(l1->next->next, 9);
    appendListNode(l1->next->next->next, 9);
    appendListNode(l1->next->next->next->next, 9);

    struct ListNode* l2 = createListNode(9);
    appendListNode(l2, 9);
    appendListNode(l2->next, 9);
    appendListNode(l2->next->next, 9);

    printListNode(l1, "l1:");
    printListNode(l2, "l2:");

    struct ListNode* result = addTwoNumbers(l1, l2);


    printListNode(result, "OUTPUT:");

    freeListNode(l1);
    freeListNode(l2);
    freeListNode(result);

    return 0;
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
