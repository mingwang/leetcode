// 075_sort_colors.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given an array nums with n objects colored red, white, or blue, sort them in - place so that objects of the same color are adjacent, with the colors in the order red, white, and blue.
//
//We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.
//
//You must solve this problem without using the library's sort function.
//
//
//
//Example 1:
//
//Input: nums = [2, 0, 2, 1, 1, 0]
//Output : [0, 0, 1, 1, 2, 2]
//Example 2 :
//
//    Input : nums = [2, 0, 1]
//    Output : [0, 1, 2]
//    Example 3 :
//
//    Input : nums = [0]
//    Output : [0]
//    Example 4 :
//
//    Input : nums = [1]
//    Output : [1]
//
//
//    Constraints :
//
//    n == nums.length
//    1 <= n <= 300
//    nums[i] is 0, 1, or 2.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }

#define SOL 4

#if SOL == 1
    void sortColors(vector<int>& nums) {
        // 0ms, 100%
        // 8.4MB, 23.05%
        int beg = 0, mid = 0, high = nums.size() - 1;
        while (mid <= high)
        {
            if (nums[mid] == 2)
            {
                // cout<<nums[mid]<<" "<<nums[high];
                swap(nums[mid], nums[high]);
                // cout<<nums[mid]<<" "<<nums[high];
                high--;
            }
            else if (nums[mid] == 0)
            {
                swap(nums[mid], nums[beg]);
                mid++;
                beg++;
            }
            else
            {
                mid++;
            }
        }
    }
#endif

#if SOL == 2
    void sortColors2(vector<int>& nums) {
        // 6ms, 5.16%
        // 8.3MB, 68.26%
        vector<int> tilly(3, 0);
        for (int i = 0; i < nums.size(); ++i)
            tilly[nums[i]]++;
        
        int si = 0, ei;
        for (int i = 0; i < 3; ++i) {
            ei = si + tilly[i];
            if (tilly[i]>0) 
            {
                //cout << "i: " << i << ", count: " << tilly[i] << ", si: "<<si<<", ei="<<ei << "\n" << std::flush;
                fill(nums.begin() + si, nums.begin() + ei, i);
            }
                
            si += tilly[i];
        }
    }
#endif

#if SOL == 3
    // DP
    // 0ms, 100%
    // 9.9MB. 8.56%
    void sortColors(vector<int>& nums) {
        vector<int> cnt(3, 0);
        int n = nums.size();
        for (int i = 0; i < n; ++i) {
            cnt[nums[i]]++;
        }
        int start = 0;
        for (int i = 0; i < 3; ++i) {
            if (cnt[i] > 0) {
                fill(nums.begin() + start, nums.begin() + start + cnt[i], i);
                start += cnt[i];
            }
        }
    }
#endif

#if SOL == 4
    // Two pointers.
    // O(n): 2ms (45.14%)
    // O(1): 9.86MB (6.19%)
    void sortColors(vector<int>& nums) {
        int n = nums.size();
        int left = 0, right = n - 1, i = 0;

        while (i <= right) {
            if (nums[i] == 0) {
                swap(nums[i++], nums[left++]);
            }
            else if (nums[i] == 2) {
                swap(nums[i], nums[right--]);
            }
            else {
                ++i;
            }
        }
    }
#endif

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> nums = sol.split<int>(argv[1]);
    vector<int> expected = sol.split<int>(argv[2]);


    int start_time = getMilliCount();
    sol.sortColors(nums);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));
    vector<int> output = nums;


    cout << "nums   = '" << argv[1] << "'" << endl;
    cout << "output   = '" << sol.print<int>(output) << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;

    if (sol.checkArray<int>(output, expected) == true)
        return 0;
    else
        return 1;
}




