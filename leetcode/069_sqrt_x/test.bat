echo off

echo %1
set status = 0

set arr[0]="%1" "4"		"2"					
set arr[1]="%1" "8"		"2"					 
set arr[2]="%1" "36"	"6"		
set arr[3]="%1" "9"		"3"		
set arr[4]="%1" "2147483647"		"46340"		

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 