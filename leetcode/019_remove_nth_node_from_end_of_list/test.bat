echo off

echo %1
set status = 0

set arr[0]="%1" "[1,2,3,4,5]"	"2"		"[1,2,3,5]"
set arr[1]="%1" "[1]"			"1"		"[]"
set arr[2]="%1" "[1,2]"			"1"		"[1]"
set arr[3]="%1" "[1,2,3]"			"3"		"[2,3]"
set arr[4]="%1" "[1,2]"			"2"		"[2]"


set "x=0"
:loop
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")
