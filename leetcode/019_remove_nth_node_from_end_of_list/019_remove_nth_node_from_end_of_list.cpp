// 019_remove_nth_node_from_end_of_list.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given the head of a linked list, remove the nth node from the end of the listand return its head.
//
//
//
//Example 1:
//
//
//Input: head = [1, 2, 3, 4, 5], n = 2
//Output : [1, 2, 3, 5]
//Example 2 :
//
//	Input : head = [1], n = 1
//	Output : []
//	Example 3 :
//
//	Input : head = [1, 2], n = 1
//	Output : [1]
#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    string print(vector<int>& in) {
        string out;
        out += '[';
        for (int i = 0; i < in.size(); i++) {
            out += in[i];
            if (i != in.size() - 1)
                out += ',';
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    void splitDoubleArray(string input, char c, vector< vector<int> >& out) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector<int> out_ele;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode *l=head, *r = head;
        int i = 0;

        while (r != nullptr) {
            if (i > n)
                l = l->next;
            r = r->next;
            i++;
        }

        if (i==n) {
            head = head->next;
        }
        else {
            l->next = l->next->next;
        }

        return head;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    string index(argv[2]);
    string expected(argv[3]);

    vector<int> v_in;
    Solution sol;

    sol.split(input, ',', v_in);
    int n = atoi(index.c_str());
    vector<int> expect_array;
    sol.split(expected, ',', expect_array);

    ListNode* list_in = sol.createList(v_in);

    int start_time = getMilliCount();
    ListNode* output = sol.removeNthFromEnd(list_in, n);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    string out_str = sol.printList(output);

    cout << "input = '" << input << "'" << endl;
    cout << "n = " << n << endl;
    cout << "output = " << out_str << endl;
    cout << "expected = " << expected << endl;

    if (out_str.compare(expected) == 0) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }



}