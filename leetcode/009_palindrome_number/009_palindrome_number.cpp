// 009_palindrome_number.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an integer x, return true if x is palindrome integer.
//
//An integer is a palindrome when it reads the same backward as forward.For example, 121 is palindrome while 123 is not.
//
//
//
//Example 1:
//
//Input: x = 121
//Output : true
//Example 2 :
//
//    Input : x = -121
//    Output : false
//    Explanation : From left to right, it reads - 121. From right to left, it becomes 121 - .Therefore it is not a palindrome.
//    Example 3 :
//
//    Input : x = 10
//    Output : false
//    Explanation : Reads 01 from right to left.Therefore it is not a palindrome.
//    Example 4 :
//
//    Input : x = -101
//    Output : false
//
//
//    Constraints :
//
//    -231 <= x <= 231 - 1

#include <iostream>

using namespace std;

class Solution {
public:
    bool isPalindrome2(int x) {
        if (x < 0)
            return false;
        long rev=0;
        int temp = x;
        while (temp != 0) {
            rev = rev * 10 + temp % 10;
            temp /= 10;
        }

        return rev == x;
    }
    bool isPalindrome(int x) {
        string s;
        if (x < 0) 
            s.push_back('-');
        while (x != 0) {
            s.push_back(x % 10);
            x /= 10;
        }
        for (int i = 0; i < s.length() / 2; i++) {
            if (s[i] != s[s.length() - i - 1])
                return false;
        }
        return true;
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    int input = atoi(argv[1]);
    bool expected = atoi(argv[2]);

    Solution sol;

    bool output = sol.isPalindrome2(input);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output == expected) {
        printf("match\n\n");
        return 0;
    }
    else {
        printf("mismatch\n\n");
        return 1;
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
