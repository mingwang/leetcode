
echo %1
echo off
set status = 0

%1 "121" "1"
set /a status= %status% + %errorlevel%
%1 "1221" "1"
set /a status= %status% + %errorlevel%
%1 "12321" "1"
set /a status= %status% + %errorlevel%
%1 "12331" "0"
set /a status= %status% + %errorlevel%
%1 " -121" "0"
set /a status= %status% + %errorlevel%
%1 "10" "0"
set /a status= %status% + %errorlevel%
%1 "-101" "0"
set /a status= %status% + %errorlevel%

%1 "0" "1"
set /a status= %status% + %errorlevel%

:: test MAX_INT case
%1 "2147483648" "0"
set /a status= %status% + %errorlevel%

:: test MIN_INT case
%1 "-2147483649" "0"
set /a status= %status% + %errorlevel%

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")