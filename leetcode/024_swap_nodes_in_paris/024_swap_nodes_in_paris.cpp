// 024_swap_nodes_in_paris.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given a linked list, swap every two adjacent nodesand return its head.You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)
//
//
//
//Example 1:
//
//
//Input: head = [1, 2, 3, 4]
//Output : [2, 1, 4, 3]
//Example 2 :
//
//    Input : head = []
//    Output : []
//    Example 3 :
//
//    Input : head = [1]
//    Output : [1]
//
//
//    Constraints :
//
//    The number of nodes in the list is in the range[0, 100].
//    0 <= Node.val <= 100

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out += '[';
        for (int i = 0; i < in.size(); i++) {
            out += in[i];
            if (i != in.size() - 1)
                out += ',';
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    ListNode* swapPairs2(ListNode* head) {
        // 3 ms
        if (head == NULL) {
            return NULL;
        }
        else if (head->next == NULL) {
            return head;
        }
        else {
            ListNode* newHead = head->next;
            head->next = newHead->next;
            newHead->next = head;
            head->next = swapPairs(head->next);
            return newHead;
        }
    }

    ListNode* swapPairs(ListNode* head) {
        // 0 ms
        ListNode* cur = head;
        ListNode* nxt = (head) ? head->next : nullptr;

        if (nxt) {
            head = nxt;
            cur->next = nxt->next;
            nxt->next = cur;
            nxt = cur->next;
        }

        while (nxt && nxt->next) {
            cur->next = nxt->next;
            nxt->next = cur->next->next;
            cur->next->next = nxt;
            cur = nxt;      
            nxt = cur->next;
        }

        return head;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> v = sol.split(argv[1], ',');
    vector<int> vo = sol.split(argv[2], ',');
    ListNode* l = sol.createList(v);
    ListNode* expect = sol.createList(vo);

    int start_time = getMilliCount();
    ListNode* output = sol.swapPairs(l);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string out_str = sol.printList(output);
    string expected = sol.printList(expect);

    cout << "input = '" << argv[1] << "'" << endl;
    cout << "output = " << out_str << endl;
    cout << "expected = " << expected << endl;

    if (out_str.compare(argv[2]) == 0) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }



}