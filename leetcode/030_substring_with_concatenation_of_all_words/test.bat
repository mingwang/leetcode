echo off

echo %1
set status = 0

set arr[0]="%1" "barfoothefoobarman"				"[foo,bar]"					"[0,9]"
set arr[1]="%1" "wordgoodgoodgoodbestword"			"[word,good,best,word]"		"[]"		
set arr[2]="%1" "barfoofoobarthefoobarman"			"[bar,foo,the]"				"[6,9,12]"
set arr[3]="%1" "lingmindraboofooowingdingbarrwingmonkeypoundcake"			"[fooo,barr,wing,ding,wing]"				"[13]"


set "x=0"
:loop
if defined arr[%x%] (
	call %%arr[%x%]%%
	set /a "x+=1"
	set /a status= %status% + %errorlevel%
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")