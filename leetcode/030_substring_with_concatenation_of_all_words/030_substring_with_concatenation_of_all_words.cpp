// 030_substring_with_concatenation_of_all_words.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//You are given a string sand an array of strings words of the same length.Return all starting indices of substring(s) in s that is a concatenation of each word in words exactly once, in any order, and without any intervening characters.
//
//You can return the answer in any order.
//
//
//
//Example 1:
//
//Input: s = "barfoothefoobarman", words = ["foo", "bar"]
//Output : [0, 9]
//Explanation : Substrings starting at index 0 and 9 are "barfoo" and "foobar" respectively.
//The output order does not matter, returning[9, 0] is fine too.
//Example 2 :
//
//    Input : s = "wordgoodgoodgoodbestword", words = ["word", "good", "best", "word"]
//    Output : []
//    Example 3 :
//
//    Input : s = "barfoofoobarthefoobarman", words = ["bar", "foo", "the"]
//    Output : [6, 9, 12]
//
//
//    Constraints :
//
//    1 <= s.length <= 104
//    s consists of lower - case English letters.
//    1 <= words.length <= 5000
//    1 <= words[i].length <= 30
//    words[i] consists of lower - case English letters.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
    vector<int> split(string input, char c) {

        vector<int> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }

        return out;
    }

    string print(vector<int>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(to_string(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    vector<string> splitStr(string input, char c) {
        vector<string> out;
        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
        return out;
    }


    vector<vector<int>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<int> > out;
        vector<int> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    bool checkIntArray(vector<int>& out, vector<int>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n\n");
            return 1;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                printf("i-%d mismatch out=%d, exp=%d\n\n", i, out[i], exp[i]);
                return 1;
            }
        }

        printf("match\n\n");
        return 0;
    }

    vector<int> findSubstring2(string s, vector<string>& words) {
        // 684 ms, 286MB
        vector<int> res;
        vector<string> pattern;
        unordered_map <string, int> map;
        int w_len = words[0].length();
        int p_len = w_len * words.size();
        int s_len = s.length()-p_len;

        for (auto pat : words) {
            if (map.find(pat)==map.end())
                map[pat] = 1;
            else
                map[pat]++;
        }
        
        for (int i = 0; i <= s_len; i += 1) {
            unordered_map <string, int> copy = map;
            for (int j = 0; j < p_len; j += w_len) {
                string cur = s.substr(j + i, w_len);
                if (copy.find(cur) != copy.end()) {
                    if (copy[cur] == 1)
                        copy.erase(cur);
                    else
                        copy[cur]--;
                }
                else
                    break;
            }
            if (!copy.size())
                res.push_back(i);
        }

        return res;
    }

    vector<int> findSubstring(string s, vector<string>& words) {
        // 32ms, 16.5MB 
        vector<int> res;
        vector<string> pattern;
        unordered_map <string, int> map;
        int w_len = words[0].length();
        int p_len = w_len * words.size();
        int s_len = s.length() - p_len;

        for (auto pat : words) {
                map[pat]++;
        }

        for (int i = 0; i < w_len; i++) {
            int left = i, right = i, cnt = 0;
            unordered_map<string, int> cur;
            while (right + w_len <= s.length()) {
                string w = s.substr(right, w_len);
                right += w_len;
                if (map.find(w) != map.end()) {
                    ++cur[w];
                    ++cnt;
                    while (cur[w] > map[w]) {
                        string ls = s.substr(left, w_len);
                        left += w_len;
                        --cnt;
                        --cur[ls];
                    }
                    if (cnt == words.size())
                        res.push_back(left);
                }
                else {
                    left = right;
                    cnt = 0;
                    cur.clear();
                }
            }
        }

        return res;
    }
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    string s = string(argv[1]);
    vector<string> words = sol.splitStr(argv[2],',');
    vector<int> expected = sol.split(argv[3], ',');

    int start_time = getMilliCount();
    vector<int> output = sol.findSubstring(s, words);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));
    string output_str = sol.print(output);

    cout << "s = '" << argv[1] << "'" << endl;
    cout << "words = '" << argv[2] << "'" << endl;
    cout << "output  = " << output_str << endl;
    cout << "expected = " << argv[3] << endl;

    return sol.checkIntArray(output, expected);

}