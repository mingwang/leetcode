// 022_generate_parentheses.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given n pairs of parentheses, write a function to generate all combinations of well - formed parentheses.
//
//
//
//Example 1:
//
//Input: n = 3
//Output : ["((()))", "(()())", "(())()", "()(())", "()()()"]
//Example 2 :
//
//    Input : n = 1
//    Output : ["()"]
//
//
//    Constraints :
//
//    1 <= n <= 8

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    void splitDoubleArray(string input, char c, vector< vector<int> >& out) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector<int> out_ele;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }


    void addParenthesis(vector <string>& v, string str, int lp, int rp) {
        if (lp == 0 && rp == 0) {
            v.push_back(str);
        }
        if (lp > 0) addParenthesis(v, str + '(', lp - 1, rp + 1);
        if (rp > 0) addParenthesis(v, str + ')', lp, rp - 1);
    }

    vector<string> generateParenthesis(int n) {
        vector <string> result;
        if (n != 0)
            addParenthesis(result, "", n, 0);
        return result;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    int input = atoi(argv[1]);
    string expect(argv[2]);

    vector<string > expect_out;
    Solution sol;

    sol.splitStr(expect, ',', expect_out);

    int start_time = getMilliCount();
    vector<string> output = sol.generateParenthesis(input);
    printf("Runtime = %u ms\n", getMilliSpan(start_time));

    cout << "input = '" << input << "'" << endl;
    //cout << "output = " << output << endl;
    cout << "expected = " << expect << endl;

    bool mismatch = false;



    if (output.size() != expect_out.size()) {
        printf("size: output=%lu, expect=%lu\n", output.size(), expect_out.size());
        mismatch = true;
    }

    for (int i = 0; !mismatch && i < output.size(); i++) {
        //printf("output[%d]=%s, expect_out[%d]=%s\n", i, output[i].c_str(), i, expect_out[i].c_str());

        if (output[i].compare(expect_out[i])) {
            printf("output[%d]=%s, expect_out[%d]=%s\n", i, output[i].c_str(), i, expect_out[i].c_str());
            mismatch = true;
            break;
        }
    }

    if (mismatch == false) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }
}