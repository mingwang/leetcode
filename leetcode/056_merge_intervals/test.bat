echo off

echo %1
set status = 0

set arr[0]="%1" "[[1,3],[2,6],[8,10],[15,18]]"		"[[1,6],[8,10],[15,18]]"	
set arr[1]="%1" "[[1,4],[4,5]]"						"[[1,5]]"	
set arr[2]="%1" "[[1,4],[2,3]]"						"[[1,4]]"	

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")