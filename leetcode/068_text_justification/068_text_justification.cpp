// 068_text_justification.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given an array of strings wordsand a width maxWidth, format the text such that each line has exactly maxWidth charactersand is fully(leftand right) justified.
//
//You should pack your words in a greedy approach; that is, pack as many words as you can in each line.Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.
//
//Extra spaces between words should be distributed as evenly as possible.If the number of spaces on a line does not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.
//
//For the last line of text, it should be left - justified and no extra space is inserted between words.
//
//Note:
//
//A word is defined as a character sequence consisting of non - space characters only.
//Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
//The input array words contains at least one word.
//
//
//Example 1:
//
//Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
//Output :
//    [
//        "This    is    an",
//        "example  of text",
//        "justification.  "
//    ]
//Example 2:
//
//Input: words = ["What", "must", "be", "acknowledgment", "shall", "be"], maxWidth = 16
//Output :
//    [
//        "What   must   be",
//        "acknowledgment  ",
//        "shall be        "
//    ]
//Explanation : Note that the last line is "shall be    " instead of "shall     be", because the last line must be left - justified instead of fully - justified.
//Note that the second line is also left - justified becase it contains only one word.
//Example 3 :
//
//    Input : words = ["Science", "is", "what", "we", "understand", "well", "enough", "to", "explain", "to", "a", "computer.", "Art", "is", "everything", "else", "we", "do"], maxWidth = 20
//    Output :
//    [
//        "Science  is  what we",
//        "understand      well",
//        "enough to explain to",
//        "a  computer.  Art is",
//        "everything  else  we",
//        "do                  "

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c) {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return to_string(item); };
    template<> string covert2string(string& item) { return item; };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }


    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        for (int i = 0; i < exp.size(); i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << " mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";
        return true;
    }

    string fillWords(vector<string>& words, int bg, int ed, int maxWidth, bool lastLine = false) {
        int wordCount = ed - bg + 1;
        int spaceCount = maxWidth + 1 - wordCount;
        for (int i = bg; i <= ed; i++) {
            spaceCount -= words[i].size();
        }

        int spaceSuffix = 1;
        int spaceAvg = (wordCount == 1) ? 1 : spaceCount / (wordCount - 1);
        int spaceExtra = (wordCount == 1) ? 0 : spaceCount % (wordCount - 1);
        string ans;
        for (int i = bg; i < ed; ++i) {
            ans += words[i];
            if (lastLine) {
                fill_n(back_inserter(ans), 1, ' ');
                continue;
            }
            fill_n(back_inserter(ans), spaceSuffix + spaceAvg + ((i - bg) < spaceExtra), ' ');
        }
        ans += words[ed];
        fill_n(back_inserter(ans), maxWidth - ans.size(), ' ');
        return ans;
    }

    vector<string> fullJustify(vector<string>& words, int maxLength) {
        // 0ms, 100%
        // 7.2MB, 99.89%
        vector<string> ans;
        int cnt = 0;
        int bg = 0;
        for (int i = 0; i < words.size(); ++i) {
            cnt += words[i].size() + 1;
            if (i + 1 == words.size() || cnt + words[i + 1].size() > maxLength) {
                ans.push_back(fillWords(words, bg, i, maxLength, i + 1 == words.size()));
                bg = i + 1;
                cnt = 0;

            }
        }
        return ans;
    }

    vector<string> fullJustify4(vector<string>& words, int maxLength) {
        // 0ms, 100%
        // 7.5MB, 20.66%
        vector<string> output;

        reverse(words.begin(), words.end());
        vector<string>curWords;
        int curLen = 0;

        while (words.size() >= 1) {
            string word = words[words.size() - 1];
            words.pop_back();

            if ((int)(word.size()) > maxLength - curLen - (int)(curWords.size())) {
                int spaces = maxLength - curLen;
                int evenSpaces = (int)(curWords.size()) == 1 ? 0 : spaces / ((int)(curWords.size()) - 1);
                int extraSpaces = spaces - evenSpaces * ((int)(curWords.size()) - 1);
                string newLine = "";

                newLine += curWords[0];

                for (int i = 1; i < (int)(curWords.size()); i++) {
                    for (int j = 0; j < evenSpaces + (extraSpaces > 0 ? 1 : 0); j++)
                        newLine += " ";
                    extraSpaces -= (extraSpaces > 0 ? 1 : 0);
                    newLine += curWords[i];
                }

                for (int j = 0; j < extraSpaces; j++)
                    newLine += " ";

                output.push_back(newLine);
                curWords.clear();
                curLen = 0;
            }

            curWords.push_back(word);
            curLen += (int)(word.size());

            if ((int)(words.size()) == 0) {
                int spaces = maxLength - curLen;
                string newLine = "";

                newLine += curWords[0];

                for (int i = 1; i < (int)(curWords.size()); i++) {
                    newLine += " ";
                    newLine += curWords[i];
                }

                for (int j = 0; j < spaces - ((int)(curWords.size()) - 1); j++)
                    newLine += " ";
                output.push_back(newLine);
            }
        }

        return output;
    }

    vector<string> fullJustify3(vector<string>& words, int maxWidth) {
        // 4ms 49.3%
        // 7.5MB, 53.77%
        int currLeftWidth = maxWidth, i = 0, si=0,count=0,minSpaces=-1;
        vector<string> justified;

        while (i < words.size()) {
            if (currLeftWidth - minSpaces > words[i].length()) {
                currLeftWidth -= words[i].length();
                ++count;
                minSpaces = count - 1;
                ++i;
            }
            else {
                string line = words[si];
                if (count > 1) {
                    int avgSpace = currLeftWidth / (count - 1);
                    int extraSpaceInd = currLeftWidth % (count - 1);
                    for (int j = 1; j < count; ++j) {
                        string space((j <= extraSpaceInd ? avgSpace + 1 : avgSpace), ' ');
                        line.append(space).append(words[j+si]);
                    }
                }
                else {
                    line.append(string(currLeftWidth, ' '));
                }

                justified.push_back(line);
                count = 0;
                si = i;
                currLeftWidth = maxWidth;
                minSpaces = -1;
            }
        }

        if (count) {
            string line = words[si];
            for (int j = 1; j < count; ++j) {
                line.append(" ").append(words[si+j]);
            }
            if (minSpaces == -1) minSpaces = 0;
            line.append(string(currLeftWidth - minSpaces, ' '));
            justified.push_back(line);
        }

        return justified;
    
    }

    vector<string> fullJustify2(vector<string>& words, int maxWidth) {
        // 4ms, 49.30%
        // 7.5MB, 20.66%
        int currLeftWidth = maxWidth, i=0, minSpaces=-1;
        vector<string> stack;
        vector<string> justified;

        while (i < words.size()) {
            if (currLeftWidth - minSpaces > words[i].length()) {
                currLeftWidth -= words[i].length();
                stack.push_back(words[i]);
                minSpaces = stack.size() - 1;
                ++i;
            }
            else {
                string line = stack[0];
                if (stack.size() > 1) {
                    int avgSpace = currLeftWidth / (stack.size() - 1);
                    int extraSpaceInd = currLeftWidth % (stack.size() - 1);
                    for (int j = 1; j < stack.size(); ++j) {
                        string space((j <= extraSpaceInd ? avgSpace + 1 : avgSpace), ' ');
                        line.append(space).append(stack[j]);
                    }
                }
                else {
                    line.append(string(currLeftWidth, ' '));
                }

                justified.push_back(line);
                stack = {};
                currLeftWidth = maxWidth;
                minSpaces = -1;
            }
        }
        if (stack.size()) {
            string line = stack[0];
            for (int j = 1; j < stack.size(); ++j) {
                line.append(" ").append(stack[j]); 
            }
            if (minSpaces == -1) minSpaces = 0;
            line.append(string(currLeftWidth - minSpaces, ' '));
            stack = {};
            justified.push_back(line);
        }

        return justified;
    }

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<string> words = sol.split<string>(argv[1], ',');
    int maxWidth = atoi(argv[2]);
    vector<string> expected = sol.split<string>(argv[3], ',');

    int start_time = getMilliCount();
    vector<string> output = sol.fullJustify(words, maxWidth);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));


    cout << "words   = '" << argv[1] << "'" << endl;
    cout << "maxWidth   = '" << argv[2] << "'" << endl;
    cout << "output   = '" << sol.print<string>(output) << "'" << endl;
    cout << "expected = '" << argv[3] << "'" << endl;

    if (sol.checkArray<string>(output, expected) == true)
        return 0;
    else
        return 1;
}






