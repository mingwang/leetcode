echo off

echo %1
set status = 0

set arr[0]="%1" "[This,is,an,example,of,text,justification.]"		"16"					 "[This    is    an,example  of text,justification.  ]"
set arr[1]="%1" "[What,must,be,acknowledgment,shall,be]"		"16"					 "[What   must   be,acknowledgment  ,shall be        ]"				
set arr[2]="%1" "[Science,is,what,we,understand,well,enough,to,explain,to,a,computer.,Art,is,everything,else,we,do]"		"20"				 "[Science  is  what we,understand      well,enough to explain to,a  computer.  Art is,everything  else  we,do                  ]"		

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo   "FAIL") else (echo "PASS") 