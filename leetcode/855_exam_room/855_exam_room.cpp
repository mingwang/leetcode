// 855_exam_room.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//There is an exam room with n seats in a single row labeled from 0 to n - 1.
//
//When a student enters the room, they must sit in the seat that maximizes the distance to the closest person.If there are multiple such seats, they sit in the seat with the lowest number.If no one is in the room, then the student sits at seat number 0.
//
//Design a class that simulates the mentioned exam room.
//
//Implement the ExamRoom class :
//
//    ExamRoom(int n) Initializes the object of the exam room with the number of the seats n.
//    int seat() Returns the label of the seat at which the next student will set.
//    void leave(int p) Indicates that the student sitting at seat p will leave the room.It is guaranteed that there will be a student sitting at seat p.
//
//
//    Example 1:
//
//Input
//["ExamRoom", "seat", "seat", "seat", "seat", "leave", "seat"]
//[[10], [], [], [], [], [4], []]
//Output
//[null, 0, 9, 4, 2, null, 5]
//
//Explanation
//ExamRoom examRoom = new ExamRoom(10);
//examRoom.seat(); // return 0, no one is in the room, then the student sits at seat number 0.
//examRoom.seat(); // return 9, the student sits at the last seat number 9.
//examRoom.seat(); // return 4, the student sits at the last seat number 4.
//examRoom.seat(); // return 2, the student sits at the last seat number 2.
//examRoom.leave(4);
//examRoom.seat(); // return 5, the student sits at the last seat number 5.
//
//
//
//Constraints:
//
//1 <= n <= 109
//It is guaranteed that there is a student sitting at seat p.
//At most 104 calls will be made to seat and leave.

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>
#include <set>
#include <map>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return string(1, item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }

    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return 1;
        }

        printf("match\n");
        return 0;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
cout << "r reach end at " << i << "\n\n";
return false;
            }
            else if (l == nullptr && r != nullptr) {
            cout << "l reach end at " << i << "\n\n";
            return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

#define SOL 2
#if SOL == 1

    // O(n^2) : 260ms (72.89%)
    // O(n) : 25.08MB (14.67%)

    class ExamRoom {
    private:
        int n;
        set<int> taken;

    public:
        ExamRoom(int n) :n(n) {
        }

        int seat() {
            if (taken.empty() == true) {
                taken.insert(0);
                return 0;
            }

            int d = 0, prev = *taken.begin(), max_d = 0, seat = 0;
            if (taken.size() >= 2) {
                for (auto i = next(taken.begin()); i != taken.end(); i++) {
                    d = (*i - prev) / 2;
                    //printf("i=%d, prev=%d, d=%d\n", *i, prev, d);
                    if (d > max_d) {
                        seat = prev + d;
                        max_d = d;
                    }
                    prev = *i;

                }
            }

            int left = *taken.begin(), right = n - 1 - *taken.rbegin();

            //printf("max_d=%d, prev_seat=%d, left=%d, right=%d, ", max_d, seat, left, right);

            if (max_d >= right && max_d > left) {
                taken.insert(seat);
            }
            else if (right > left) {
                taken.insert(n - 1);
                seat = n - 1;
            }
            else {
                taken.insert(0);
                seat = 0;
            }

            //printf("final_seat = %d\n", seat);
            //for (auto& i : taken) {
            //    cout << i << ' ';
            //}
            //printf("\n");
            return seat;
        }

        void leave(int p) {
            //printf("removing %d ", p);
            int cnt = taken.erase(p);

            if (cnt > 0) {
                //printf("done\n");
            }
            else {
                //printf("failed\n");
            }

            cout << std::flush;
        }
    };
#endif

#if SOL == 2
    // Seats: O(logm). 28ms. 98.22% : m is # of times to call seats
    // Leave: O(logm)
    // Mem: O(m). 25.86MB. 10.67%

    struct Comp {
        bool operator()(const pair<int, int> p1, const pair<int, int> p2) {
            int d1 = p1.second - p1.first, d2 = p2.second - p2.first;
            return (d1 / 2 < d2 / 2) || (d1 / 2 == d2 / 2 && p1.first > p2.first);
        }
    };

    class ExamRoom {
    private:
        int n;
        set<int> taken;
        priority_queue<pair<int, int>, vector<pair<int, int>>, Comp> pq;
        
    public:
        ExamRoom(int n) :n(n) {
        }

        int seat() {
            if (taken.empty() == true) {
                taken.insert(0);
                return 0;
            }

            int left = *taken.begin(), right = n - 1 - *taken.rbegin();

            while (taken.size() >= 2) {
                auto p = pq.top();
                if (taken.count(p.first) > 0 && taken.count(p.second) > 0 &&
                    *next(taken.find(p.first)) == p.second) { // not in delayed removal region
                    int d = p.second - p.first;
                    if (d / 2 < right || d / 2 <= left) {
                        break;
                    }
                    pq.pop();
                    int seat = p.first + d / 2;
                    pq.push({ p.first, seat });
                    pq.push({ seat, p.second });
                    taken.insert(seat);
                    return seat;
                }
                pq.pop();
            }

            if (right > left) {
                pq.push({ *taken.rbegin(), n - 1 });
                taken.insert(n - 1);
                return n - 1;
            }
            else {
                pq.push({ 0,*taken.begin() });
                taken.insert(0);
                return 0;
            }
        }

        void leave(int p) {
            if (p != *taken.begin() && p != *taken.rbegin()) {
                auto it = taken.find(p);
                pq.push({ *prev(it),*next(it) });
            }
            taken.erase(p);
        }
    };
#endif

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> nums = sol.split<int>(argv[1]);
    int expected = atoi(argv[2]);

    int start_time = getMilliCount();
    int res = sol.maxSubarraySumCircular(nums);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "nums   = '" << argv[1] << "'" << endl;
    cout << "expected = '" << expected << "'" << endl;
    cout << "res = '" << res << "'" << endl;

    if (sol.check<int>(res, expected) == true)
        return 0;
    else
        return 1;
}

