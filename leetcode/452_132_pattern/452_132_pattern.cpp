// 452_132_pattern.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//Given an array of n integers nums, a 132 pattern is a subsequence of three integers nums[i], nums[j] and nums[k] such that i < j < kand nums[i] < nums[k] < nums[j].
//
//    Return true if there is a 132 pattern in nums, otherwise, return false.
//
//
//
//    Example 1:
//
//Input: nums = [1, 2, 3, 4]
//Output : false
//Explanation : There is no 132 pattern in the sequence.
//Example 2 :
//
//    Input : nums = [3, 1, 4, 2]
//    Output : true
//    Explanation : There is a 132 pattern in the sequence : [1, 4, 2] .
//    Example 3 :
//
//    Input : nums = [-1, 3, 2, 0]
//    Output : true
//    Explanation : There are three 132 patterns in the sequence : [-1, 3, 2] , [-1, 3, 0] and [-1, 2, 0].
//
//
//    Constraints :
//
//    n == nums.length
//    1 <= n <= 2 * 105
//    - 109 <= nums[i] <= 109

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <sys/timeb.h>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <cassert>
#include <unordered_set>
#include <queue>
#include <set>

using namespace std;


int getMilliCount() {
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart) {
    int nSpan = getMilliCount() - nTimeStart;
    if (nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

//Definition for a binary tree node.
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

#define PASS 0
#define FAIL 1

class Solution {
public:

    template<typename T> T getItem(string& str) {  }
    template<> int getItem(string& str) { return atoi(str.c_str()); }
    template<> char getItem(string& str) { return str[0]; }
    template<> double getItem(string& str) { return atof(str.c_str()); }
    template<> string getItem(string& str) { return str; }

    template<typename T>
    vector<T> split(string input, char c = ',') {

        vector<T> out;

        if (input.compare("[]") == 0)
            return out;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(getItem<T>(s));
            i = new_i + 1;
        }

        return out;
    }

    template<typename T> string covert2string(T& item) { }
    template<> string covert2string(int& item) { return to_string(item); };
    template<> string covert2string(double& item) { return to_string(item); };
    template<> string covert2string(char& item) { return string(1, item); };
    template<> string covert2string(string& item) { return item; };
    template<> string covert2string(TreeNode& root) { return printTree(&root); };

    template<typename T>
    string print(vector<T>& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append(covert2string<T>(in[i]));
            if (i != in.size() - 1)
                out.append(",");
        }
        out += "]";
        return out;
    }

    string printList(ListNode* head) {
        string out;
        out.append("[");
        for (ListNode* tmp = head; tmp != nullptr; tmp = tmp->next) {
            out.append(to_string(tmp->val));
            if (tmp->next != nullptr) {
                out.append(",");
            }
        }
        out.append("]");
        return out;
    }

    template<typename T>
    vector<vector<T>> splitDoubleArray(string input, char c = ',') {
        //"[[-1,-1,2],[-1,0,1]]"
        vector< vector<T> > out;
        vector<T> out_ele;

        if (input.compare("[]") == 0)
            return out;

        if (input.compare("[[]]") == 0) {
            out.push_back(out_ele);
            return out;
        }

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                if (s != "")
                    out_ele.push_back(getItem<T>(s));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(getItem<T>(s));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
        return out;
    }



    template<typename T>
    string printDoubleArray(vector<vector<T> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(covert2string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]");
        return out;
    }

    ListNode* createList(vector<int>& in) {
        ListNode* head = nullptr;
        ListNode* tmp = head;
        for (int i = 0; i < in.size(); i++) {
            if (i == 0) {
                head = new ListNode(in[i]);
                tmp = head;
            }
            else {
                tmp->next = new ListNode(in[i]);
                tmp = tmp->next;
            }
        }

        return head;
    }

    vector<ListNode*> createList(vector<vector<int>>& in) {
        vector<ListNode*> out;
        for (int i = 0; i < in.size(); i++) {
            out.push_back(createList(in[i]));
        }
        return out;
    }

    TreeNode* createTree(vector<string>& nodes, int index = 0, int index_offset = 0) {
        TreeNode* root = nullptr, * left = nullptr, * right = nullptr;
        deque<TreeNode*> queue;
        int n = nodes.size();
        if (n > 0) {
            root = new TreeNode(atoi(nodes[0].c_str()));
            queue.push_back(root);
        }

        int i = 1;

        while (i < n) {
            int curr_size = queue.size();
            for (int j = 0; j < curr_size && i < n; ++j) {
                TreeNode* node = queue.front();
                //cout << "i:" << i << ", val:" << node->val << "\n" << std::flush;
                queue.pop_front();
                node->left = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                node->right = (i >= n || nodes[i].compare("null") == 0) ? nullptr : new TreeNode(atoi(nodes[i].c_str()));
                i++;
                if (node->left != nullptr)
                    queue.push_back(node->left);
                if (node->right != nullptr)
                    queue.push_back(node->right);
            }
        }

        return root;
    }

    string printTree(TreeNode* root) {
        queue<TreeNode*> leaf_nodes;
        leaf_nodes.push(root);
        string s = "[";

        while (leaf_nodes.size() > 0) {
            root = leaf_nodes.front();
            leaf_nodes.pop();
            if (root != nullptr) {
                s += to_string(root->val) += ",";
                leaf_nodes.push(root->left);
                leaf_nodes.push(root->right);
            }
            else {
                s += "null,";
            }
        }
        //cout << "s:" << s << "\n" << std::flush;
        int s_ind = s.length() - 5;
        while (s_ind >= 0 && s.substr(s_ind, 5).compare("null,") == 0) {
            s.erase(s_ind);
            s_ind = s.length() - 5;
        }

        //s_ind = s.length() - 1;
        //if (s_ind && s.substr(s_ind, 1).compare(",") == 0)
        //    s.erase(s_ind);

        if (s.length() > 1)
            s[s.length() - 1] = ']';
        else
            s += ']';

        return s;

    }

    template  <class T>
    bool checkArray(vector<T>& out, vector<T>& exp, int len = -1) {
        if (out.size() != exp.size()) {
            printf("length mismatch\n");
            return false;
        }

        if (len == -1)
            len = exp.size();

        for (int i = 0; i < len; i++) {
            if (out[i] != exp[i]) {
                cout << "i-" << i << "checkArray mismatch out=" << out[i] << ", exp=" << exp[i] << "\n";
                return false;
            }
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool check(T& out, T& exp) {
        if (out != exp) {
            printf("check mismatch\n");
            return false;
        }

        printf("match\n");
        return true;
    }

    template  <typename T>
    bool checkDoubleArray(vector<vector<T>>& out, vector<vector<T>>& exp) {
        if (out.size() != exp.size()) {
            printf("height mismatch\n\n");
            return false;
        }
        for (int i = 0; i < exp.size(); i++) {
            if (out[i].size() != exp[i].size()) {
                printf("width mismatch\n\n");
                return false;
            }
        }
        for (int i = 0; i < exp.size(); i++) {
            for (int j = 0; j < exp[i].size(); j++) {
                if (out[i][j] != exp[i][j]) {
                    printf("mismatch @ i,j-%d,%d ", i, j);
                    cout << "out=" << out[i][j] << ",exp=" << exp[i][j] << "\n\n";
                    return false;
                }
            }
        }

        printf("match\n");
        return true;
    }

    bool checkList(ListNode* l, ListNode* r) {
        int i = 0;

        while (l != nullptr || r != nullptr) {

            if (l != nullptr && r == nullptr) {
                cout << "r reach end at " << i << "\n\n";
                return false;
            }
            else if (l == nullptr && r != nullptr) {
                cout << "l reach end at " << i << "\n\n";
                return false;
            }

            if (l->val != r->val) {
                cout << "ind " << i << ": mismatch " << l->val << " != " << r->val << "\n\n";
                return false;
            }
            else {
                ++i;
                l = l->next;
                r = r->next;
            }
        }
        cout << "match\n\n";

        return true;
    }

#define GET_STATE(state) \
    ((state == RISING) ?  "rise" : \
     (state == FALLING) ? "fall" :\
    (state == RESET) ?    "rest" : "")

#define SOL 5

#if SOL == 1
    // time limit exceeded with large input set
    bool find132pattern(vector<int>& nums) {

        for (int i = 0; i < int(nums.size()) - 2; i++) {
            for (int j = i + 1; j < int(nums.size()) - 1; j++) {
                for (int k = j + 1; k < nums.size(); k++) {
                    if (nums[i] < nums[k] && nums[k] < nums[j]) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
#endif

#if SOL == 2
    // Method 1: min + ordered set. Runtime O(nlogn)
    // O(nlogn): init right. 307ms: 5.49%
    // O(n): store right. 107.08MB: 5.56%
    void print_multiset(multiset<int> set) {
        printf("set: ");
        for (int a : set) {
            printf("%d ", a);
        }
        printf("\n");
    }

    bool find132pattern(vector<int>& nums) {
        int n = nums.size();
        if (n < 3)
            return false;

        int left_min = nums[0];
        multiset<int> right;

        for (int k = 2; k < n; k++) {
            right.insert(nums[k]);
        }
        //print_multiset(right);

        for (int j = 1; j < n - 1; j++) {
            if (left_min < nums[j]) {
                auto k = right.upper_bound(left_min);
                if (k != right.end() && *k < nums[j]) {
                    return true;
                }
            }
            left_min = min(left_min, nums[j]);
            right.erase(right.find(nums[j + 1]));
            //print_multiset(right);
        }

        return false;
    }
#endif

#if SOL == 3
    bool find132pattern(vector<int>& arr) {
        ios_base::sync_with_stdio(0), cin.tie(0), cout.tie(0);
        int n = arr.size();
        vector<int> min_val(n), s;
        min_val[0] = arr[0];
        for (int i = 1; i < n; i++) min_val[i] = min(min_val[i - 1], arr[i]);
        for (int i = n - 1; i > 0; i--) {
            while (!s.empty() and arr[s.back()] < arr[i]) {
                if (min_val[i - 1] < arr[s.back()]) return true;
                s.pop_back();
            }
            s.push_back(i);
        }
        return false;
    }
#endif

#if SOL == 4
    // Method 4: monotonic stack. Runtime O(n)
    // Runtime: O(n): 101ms. 83.89%
    // Space: O(n): 66.85MB. 61.56%
    bool find132pattern(vector<int>& nums) {
        int n = nums.size();
        stack<int> stk; // keep track of the maximum element from right side. This stack is monotonic growing. 
        stk.push(nums[n - 1]);
        int max_k = INT_MIN;

        for (int i = n - 2; i >= 0; --i) {
            if (nums[i] < max_k) {
                return true;
            }
            while (stk.empty() == false && nums[i] > stk.top()) {
                max_k = stk.top();
                stk.pop();
            }
            if (nums[i] > max_k) {
                stk.push(nums[i]);
            }
        }
        return false;
    }
#endif

#if SOL == 5
    // SOL1 and SOL4 requires all the data being present
    // SOL5 can work with data stream
    // Runtime: O(nlogn). 111ms, 51.53%
    // Space: O(n). 63.73MB, 95.22%

    void print_array(const string str, vector<int>& nums) {
        printf("%s : ", str.c_str());
        for (auto& i : nums) {
            printf("%d ", i);
        }
        printf("\n");
    }

    bool find132pattern(vector<int>& nums) {
        int n = nums.size();
        vector<int> can_i = { nums[0] }; // Descending Elements (with monotonic shrinking)
        vector<int> can_j = { nums[0] }; // Descending Elements (with monotonic growing)

        for (int k = 1; k < n; k++) {
            print_array("  can_i", can_i);
            print_array("  can_j", can_j);

            // value is nums[k]
            // find first element when value > element
            auto it_i = upper_bound(can_i.begin(), can_i.end(), nums[k], greater<int>()); // find ele that is < nums[k]. (value > ele)
            // find first element when element <= nums[k] or when element > value is false
            auto it_j = lower_bound(can_j.begin(), can_j.end(), nums[k], greater<int>()); // find ele that is <= nums[k]. (value >= ele)
            int ind_i = it_i - can_i.begin();
            int ind_j = it_j - can_j.begin() - 1;
            printf("nums[%d] = %d, ind_i=%d, ind_j=%d\n", k, nums[k], ind_i, ind_j);
            if (ind_i <= ind_j) {
                return true;
            }

            if (nums[k] < can_i.back()) {
                can_i.push_back(nums[k]);
                can_j.push_back(nums[k]);
            }
            else if (nums[k] > can_j.back()) {
                int last_i = can_i.back();
                while (can_j.empty() == false && can_j.back() < nums[k]) {
                    can_i.pop_back();
                    can_j.pop_back();
                }
                can_i.push_back(last_i);
                can_j.push_back(nums[k]);
            }
        }

        return false;

    }
#endif

};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    Solution sol;
    vector<int> list = sol.split<int>(argv[1]);
    bool expected = (atoi(argv[2]) == 1);

    int start_time = getMilliCount();
    bool result = sol.find132pattern(list);
    printf("\nRuntime = %u ms\n", getMilliSpan(start_time));

    cout << "list   = '" << argv[1] << "'" << endl;
    cout << "expected = '" << argv[2] << "'" << endl;
    cout << "res = '" << result << "'" << endl;

    if (sol.check<bool>(result, expected) == true) {
        exit(PASS);
    }
    else {
        exit(FAIL);
    }
}