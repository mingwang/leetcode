// 016_3sum_closet.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//Given an array nums of n integersand an integer target, find three integers in nums such that the sum is closest to target.Return the sum of the three integers.You may assume that each input would have exactly one solution.
//
//
//
//Example 1:
//
//Input: nums = [-1, 2, 1, -4], target = 1
//Output : 2
//Explanation : The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
//
//
//Constraints :
//
//    3 <= nums.length <= 10 ^ 3
//    - 10 ^ 3 <= nums[i] <= 10 ^ 3
//    - 10 ^ 4 <= target <= 10 ^ 4

#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    void split(string input, char c, vector<int>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size();
            string s = input.substr(i, new_i - i + 1);
            int ele = atoi(s.c_str());
            out.push_back(ele);
            i = new_i + 1;
        }
    }

    void splitStr(string input, char c, vector<string>& out) {

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            out.push_back(s);
            i = new_i + 1;
        }
    }


    void splitDoubleArray(string input, char c, vector< vector<int> >& out) {
        //"[[-1,-1,2],[-1,0,1]]"
        vector<int> out_ele;

        for (int i = 1; i < input.size();) {
            int new_i = input.find(c, i);
            if (new_i == string::npos)
                new_i = input.size() - 1;
            string s = input.substr(i, new_i - i);
            if (s[0] == '[')
                s.erase(0, 1);
            if (s[s.length() - 1] == ']') {
                s.erase(s.length() - 1, 1);
                out_ele.push_back(atoi(s.c_str()));
                out.push_back(out_ele);
                out_ele.clear();
            }
            else {
                out_ele.push_back(atoi(s.c_str()));
            }
            //printf("substr = %s\n", s.c_str());
            i = new_i + 1;
        }
    }

    string printDoubleArray(vector<vector<int> >& in) {
        string out;
        out.append("[");
        for (int i = 0; i < in.size(); i++) {
            out.append("[");
            for (int j = 0; j < in[i].size(); j++) {
                out.append(to_string(in[i][j]));
                if (j != in[i].size() - 1)
                    out.append(",");
            }
            out.append("]");
            if (i != in.size() - 1)
                out.append(",");
        }
        out.append("]\n");
        return out;
    }
    
    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int mint = INT_MAX;

        for (int i = 0; i < nums.size(); i++)
        {
            if (i > 0 && nums[i] == nums[i - 1]) continue;
            int j = i + 1;
            int k = nums.size() - 1;
            int s = 0;
            while (j < k)
            {
                s = nums[i] + nums[j] + nums[k];

                if (abs(mint) > abs(target - s)) mint = target - s;

                if (s > target) k--;
                else if (s < target) j++;
                else  if (target - s == 0) return target;
            }
        }
        return target - mint;
    }

    int threeSumClosest2(vector<int>& nums, int target) {
        int result = 0, min_delta = INT_MAX;
        
        sort(nums.begin(), nums.end());

        for (int i = 0; i < nums.size();) {
            int si = i + 1;
            int ti = nums.size() - 1;
            int first = nums[i];
            while (si < ti) {   
                int second = nums[si], third = nums[ti];
                int delta = first + second + third - target;
                if (abs(delta) < min_delta) {
                    min_delta = abs(delta);
                    result = first + second + third;

                }

                if (delta == 0) return result;
                while (si < ti && second == nums[si] && delta < 0) si++;
                while (si < ti && third == nums[ti] && delta > 0) ti--;

            }
            while (++i < nums.size() && first == nums[i]);
        }

        return result;
    }
    
};

int main(int argc, char* argv[]) {

    //int input = 2147483647;
    string input(argv[1]);
    int expect = atoi(argv[2]);

    vector<vector<int> > in;
    Solution sol;

    sol.splitDoubleArray(input, ',', in);

    for (int i = 0; i < in[0].size(); i++) {
        printf("input = %d\n", in[0][i]);
    }
    printf("target=%d\n", in[1][0]);

    int output = sol.threeSumClosest(in[0], in[1][0]);

    cout << "input = '" << input << "'" << endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expect << endl;

    if (expect == output) {
        printf(" match\n\n");
        return 0;
    }
    else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;
    }
}