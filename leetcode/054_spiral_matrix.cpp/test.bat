echo off

echo %1
set status = 0

set arr[0]="%1" "[[1,2,3,4],[5,6,7,8],[9,10,11,12]]"		"[1,2,3,4,8,12,11,10,9,5,6,7]"	
set arr[1]="%1" "[[1,2,3],[4,5,6],[7,8,9]]"					"[1,2,3,6,9,8,7,4,5]"	
set arr[2]="%1" "[[3],[2]]"									"[3,2]"

set "x=0"
:loop 
if defined arr[%x%] (
	call %%arr[%x%]%%
	echo errorlevel is %errorlevel%
	set /a status= %status% + %errorlevel%
	set /a "x+=1"
	GOTO :loop
)

if %status% NEQ 0 (echo "FAIL") else (echo "PASS")