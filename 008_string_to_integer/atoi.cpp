#include <iostream>
#include <math.h>
#include <string>

using namespace std;


class Solution {
public:
    int myAtoi(string str) {
       int i=0;
       //skip white space
       while (str[i]==' ')
           i++;
       
       bool positive = false;

       if (str[i] == '+') {
           positive = true;
           i++;
       } else if (str[i]>='0' && str[i] <='9' ) {
           positive = true;     
       } else if (str[i] == '-') {
           i++;
       } else {
            // not start with +, -, or 0-9
            return 0;    
       }

       long int result = 0;
       while (str[i]>='0' && str[i]<='9') {
            if (positive) {
                result = result * 10 + str[i] - '0';
                if (result>INT_MAX) {
                    return INT_MAX;
                }
            } else {
                result = result * 10 - str[i] + '0';
                if (result<INT_MIN) {
                    return INT_MIN;
                }
            }
            i++;
       }

        return (int)result;
    }
};

int main(int argc, char *argv[]) {
    
    //int input = 2147483647;
    string input(argv[1]);
    int expected = atoi(argv[2]) ;

    Solution sol;

    int output = sol.myAtoi(input);

    cout << "input = '" << input <<"'" <<endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output==expected) {
        printf("match\n\n");
        return 0;
    } else {
        printf("mismatch\n\n");
        return 1;   
    }
}
