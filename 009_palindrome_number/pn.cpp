#include <iostream>
#include <math.h>


class Solution {
public:
    bool isPalindrome(int x) {
        if (x<0)
            return false;

        int size = (x==0)?1:log(x)/log(10)+1;

        printf("size = %d\n", size);

        for(int li=0,ui=size-1;li<ui;li++,ui--) {
            int ud =(int) (x/pow(10,ui))%10;
            int ld =(int) (x/pow(10,li))%10;
            printf("li=%d, ui=%d, ld=%d, ud=%d\n", li,ui,ld,ud);    

            if (ud!=ld)
                return false;
        }

        return true;
    }
};

int main() {
    
    //int input = 2147483647;
    int input = -2147447412;
    bool expected = true;

    Solution sol;

    bool output = sol.isPalindrome(input);

    printf("input    = %d\n", input);
    printf("output   = %d\n", output);
    printf("expected = %d\n", expected);

    if (output==expected)
        printf("match\n");
    else
        printf("mismatch\n");
    
    return 0;
}
