#include <stdio.h> 
#include <stdlib.h> 
 
/** 
 * Note: The returned array must be malloced, assume caller calls free(). 
 */ 
int* twoSum(int* nums, int numsSize, int target) { 
    int* index_array = (int*)malloc(2*sizeof(int)); 
    for (int i = 0; i < numsSize; i ++) { 
        for (int j = i+1; j < numsSize; j++) { 
            //printf("nums[%d]+nums[%d]=%d\n", i, j, nums[i]+nums[j]); 
            if (nums[i]+nums[j]==target) { 
                index_array[0] = i; 
                index_array[1] = j; 
                return index_array; 
            } 
        } 
    } 
    return NULL; 
} 
 
int main() { 
    int size = 3; 
    int* input = (int*)malloc(size*sizeof(int)); 
    input[0] = 3; 
    input[1] = 2; 
    input[2] = 4; 
    int target = 6; 
 
    int* result = twoSum(input, size, target); 
 
    printf ("Answer = [%d,%d]\n", result[0], result[1]); 
 
    free(input); 
    free(result); 
 
} 
