#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        
       vector<int> result; 
       unordered_map<int,int> map;

       for (int i=0;i<nums.size();i++) {
            cout<<"i:"<<i<<endl;
            auto search = map.find(nums[i]);
            if (search!=map.end()) {
                result.push_back(search->second);
                result.push_back(i);
            } else {
                if (target-nums[i] > 0)
                    map[target-nums[i]] = i;    
            }    
       }

       return result;
    }
};


int main() {
    
    Solution sol;
    vector<int> input = {3,2,4};
    int target = 7;

    vector<int> v = sol.twoSum(input, target);

    printf("input[%d] + input[%d]\n", v[0], v[1]);    
    printf("= %d + %d\n", input[v[0]], input[v[1]]);
    printf("= %d\n", target);

    return 0;

}
