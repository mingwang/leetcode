class Solution(object): 
    def twoSum(self, nums, target): 
        """ 
        :type nums: List[int] 
        :type target: int 
        :rtype: List[int] 
        """ 
 
        result = [] 
        map = {} 
 
        #for i in range(0,len(nums)-1): 
        #    for j in range(i+1,len(nums)): 
        #        if (nums[i]+nums[j] == target): 
        #            result.append(i) 
        #            result.append(j) 
        #            return result 
 
        for i in range(0,len(nums)): 
            if (nums[i] not in map): 
                map[target-nums[i]] = i 
            else: 
                return [map[nums[i]],i] 
             
 
        return [-1,-1] 
 
 
input = [11,15,2,7] 
target = 9 
 
 
solution = Solution() 
 
result = solution.twoSum(input, target) 
 
print "result["+str(result[0])+"] + result ["+str(result[1])+"]" 
print "= " + str(input[result[0]]) + " + " + str(input[result[1]]) 
print "= " + str(target) 
