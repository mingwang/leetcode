#include <iostream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
    void split (string input, char c, vector<int>& out) {

        for (int i = 1; i<input.size();) {
           int new_i = input.find (c, i);
           if (new_i==string::npos)
               new_i = input.size();
           string s = input.substr(i,new_i-i+1);
           int ele = atoi(s.c_str());
           out.push_back(ele);
           i=new_i+1;
        }
    }

    void splitStr (string input, char c, vector<string>& out) {

        for (int i = 1; i<input.size();) {
           int new_i = input.find (c, i);
           if (new_i==string::npos)
               new_i = input.size()-1;
           string s = input.substr(i,new_i-i);
           out.push_back(s);
           i=new_i+1;
        }
    }

    string intToRoman(int num) {
        // I - 1
        // V - 5
        // X - 10
        // L - 50
        // C - 100
        // D - 500
        // M - 1000

        num = num % 4000;

        string roman;

        int digit_0001 = num % 10;
        int digit_0010 = (num/10) % 10;
        int digit_0100 = (num/100) % 10;
        int digit_1000 = (num/1000) % 10;

        while(digit_1000>0) {
            roman.append("M"); 
            digit_1000 --;
        }

        if (digit_0100 == 9) {
            roman.append("CM");
        } else if (digit_0100 >= 5) {
            roman.append("D");
            while (digit_0100 > 5) {
                roman.append("C");    
                digit_0100--;
            }
        } else if (digit_0100 == 4) {
            roman.append("CD");    
        } else {
            while (digit_0100>0) {
                roman.append("C");    
                digit_0100--;
            }    
        }

        if (digit_0010 == 9) {
            roman.append("XC");
        } else if (digit_0010 >= 5) {
            roman.append("L");
            while (digit_0010 > 5) {
                roman.append("X");    
                digit_0010--;
            }
        } else if (digit_0010 == 4) {
            roman.append("XL");    
        } else {
            while (digit_0010>0) {
                roman.append("X");    
                digit_0010--;
            }    
        }

        if (digit_0001 == 9) {
            roman.append("IX");
        } else if (digit_0001 >= 5) {
            roman.append("V");
            while (digit_0001 > 5) {
                roman.append("I");    
                digit_0001--;
            }
        } else if (digit_0001 == 4) {
            roman.append("IV");    
        } else {
            while (digit_0001>0) {
                roman.append("I");    
                digit_0001--;
            }    
        }

        return roman;
    }

    int romanToInt(string s) {
        // I - 1
        // V - 5
        // X - 10
        // L - 50
        // C - 100
        // D - 500
        // M - 1000

        int result = 0;

        for (int i=0; i<s.length();i++) {
            int ni = (i+1>=s.length())?-1:i+1;
            switch (s[i]) {
            
                case 'M': result += 1000; break;

                case 'C': if (ni>0 && (s[ni] == 'M' || s[ni] == 'D')) result -= 100; 
                          else result += 100;
                          break;

                case 'D': result += 500; break;

                case 'L': result += 50; break;

                case 'X': if (ni>0 && (s[ni] == 'C' || s[ni] == 'L')) result -= 10;
                          else result += 10;
                          break;

                case 'V': result += 5; break;

                case 'I': if (ni>0 && (s[ni] == 'X' || s[ni] == 'V')) result -= 1;
                          else result += 1;
                          break;
            }
        }
        return result;
    }

    string longestCommonPrefix(vector<string>& strs) {

        if (strs.size()==0)
            return "";
    
        int i = 0;

        for (i=0; i<strs[0].length(); i++) {
            for (int s=1; s<strs.size(); s++) {
                if (strs[0][i]!=strs[s][i]) {
                    return strs[0].substr(0,i);
                }
            }
        }
        return strs[0].substr(0,i);
    }

};

int main(int argc, char *argv[]) {
    
    //int input = 2147483647;
    string input(argv[1]);
    string expected(argv[2]);

    vector<string> in;
    Solution sol;

    sol.splitStr(input,',',in);

    for (int i=0; i<in.size();i++) {
        printf("string%d = '%s'\n", i, in[i].c_str());    
    }


    string output = sol.longestCommonPrefix(in);

    cout << "input = '" << input <<"'" <<endl;
    cout << "output = " << output << endl;
    cout << "expected = " << expected << endl;

    if (output.compare(expected)==0) {
        printf(" match\n\n");
        return 0;
    } else {
        printf("\033[91m mismatch\n\n\033[0m");
        return 1;   
    }
}
