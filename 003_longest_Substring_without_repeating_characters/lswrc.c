#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void printSubStr(char*s, int s_index, int e_index) {
    if (s_index<0 || e_index<0 || s_index>strlen(s)-1 || e_index>strlen(s)-1) {
        printf("input outbound\n");
        return;
    }

    printf("[%d,%d] => ", s_index, e_index);

    for(int i=s_index; i<=e_index; i++) {
        printf ("%c", *(s+i));    
    }
    printf("\n");
}

int lengthOfLongestSubstring(char* s) {

    int len = strlen(s);
    int s_index = 0;
    int e_index = 0;
    int map[128];
    int max_len = 0;

    memset(map, -1, 128*sizeof(int));

    for(int i=0;i<128;i++) {
        if(map[i]!=-1)
            printf("map[%d] = %d\n", i, map[i]);
    }

    for (int i=0;i<len;i++) {
       printf("%d char %c %d\n",i, s[i],s[i]);
       printf("map[u] = %d\n", map['u']);
       if (map[s[i]]!=-1) {
            if (s_index<=map[s[i]])
                e_index = i-1;
            else
                e_index = i;
            if(e_index-s_index+1>max_len) {
                max_len = e_index-s_index+1; 
            }
            if (map[s[i]]+1>s_index)
                s_index = map[s[i]]+1;
            map[s[i]]=i;
       } else {
            e_index = i;
            if(e_index-s_index+1>max_len) {
                max_len = e_index-s_index+1;
            }
            map[s[i]] = i;    
       }
       printSubStr(s,s_index,e_index);
    }
    
    return max_len;
}


int main()
{

    //char* input = "abcabcdbxyzjklc";
    //char* input = "tmmzuxt";
    //char* input = "ggububgvfk";
    //char* input = "xbcabcd";
    //char* input = "abcadef";
    char* input = "aabbccabc";
    
    //printSubStr(input,0,0);
    //printSubStr(input,0,1);
    //printSubStr(input,0,7);
    //printSubStr(input,6,6);

    int longest_length = lengthOfLongestSubstring(input);

    printf ("longest length = %d\n", longest_length);
        
    return 0;
}
