#include <stdio.h>
#include <String.h>
#include <iostream>

using namespace std;

class Solution {
public:
    string longestPalindrome(string s) {
        if (s.size()<=1)
            return s;

        int left=0,right=0,start=0,max_left=0,max_len=1;
        int len = s.size();
        int midpoint = (len+1)/2;

        while(start<len && len-start>max_len/2) {
            left=right=start;
            while(right<len-1 && s[right+1]==s[right])
                ++right;
            start=right+1;
            while(left>0&&right<len-1&&s[right+1]==s[left-1]) {
                ++right;
                left--;
            }
            if(right-left+1>max_len){
                max_left=left;
                max_len=right-left+1;
            }
        }
        return s.substr(max_left,max_len);
    }
};

int main() {
    
    string input("abaaaaaxz");

    Solution sol;

    string substring = sol.longestPalindrome(input);

    printf ("substring = %s\n", substring.c_str());
    

    return 0;
}
